# -*- coding: utf-8 -*-

import os
import re
import logging

from cm_api.api_client import ApiResource, ApiException

MODULE_PATH = os.path.realpath(__file__).rsplit("/", 1)[0] + "/"
FLUME_PATH = MODULE_PATH + "../../user/flume/"

RAW_SRC = "raw_source"
RAW_SINK = "raw_sink"
AVRO_SINK = "avro_sink"
RSLT_SRC = "result_source"
FILE_CHANNEL = "file_channel"
EMPTY_LINE = "\n\n"
INADDR_ANY = "0.0.0.0"
CHN_CNT_OFFSET = 2

class AgentConf(object):
    """
    A configuration struct for creating FlumeAgents.
    
    @param name: The name of the Flume agent.
    @type name: str
    @param conf: The name of the configuration used by the agent, that is, the
                 configuration file name without file suffix. Configuration files are 
                 located under user/flume. 
    @type conf: str
    @param host: The address of the host where the Flume agent should be started by the
                 Cloudera Manager.
    @type host: str    
    """

    def __init__(self, name, conf, host):
        self.name = name
        self.conf = conf
        self.host = host

class AgentSourceConf(object):
    """
    A configuration struct for creating Flume sources for FlumeAgents.
    
    @param name: The name of the Flume agent source.
    @type name: str
    @param conf: The address to which the agent source should bind.
    @type conf: str
    @param port: The port to which the agent source should listen.
    @type port: int
    """

    def __init__(self, name, bind, port):
        self.name = name
        self.bind = bind
        self.port = port

class HDFSSinkConf(object):
    """
    A configuration struct for creating HDFS sinks for FlumeAgents.

    @param storage: Configuration of the HDFS storage to which the sink should write its 
                    data.
    @type storage: StorageConf
    @param batch_size: The size of a single batch of data (in samples) that the sink 
                       should use when reading data from its channel.
    @type batch_size: int
    @param roll_size: The size of the data (in bytes) the sink should write to a single
                      file before rolling it and creating a new one. Should be set to zero
                      if not used.
    @type roll_size: int
    @param roll_count: The size of the data (in samples) the sink should write to a single
                       file before rolling it and creating a new one. Should be set to zero
                      if not used.
    @type roll_size: int
    @param roll_ival: Number of seconds the sink should wait befor rolling a file and
                      creating a new one. Should be set to zero if not used.
    @type roll_ival: int
    @param idle_timeout: Timeout after the sink should roll an inactive file and create a
                         new file. Should be set to zero if not used.
    @type idle_timeout: int
    """

    def __init__(self, storage, batch_size, roll_size, roll_count, roll_ival, idle_timeout):
        self.storage = storage
        self.batch_size = batch_size
        self.roll_size = roll_size
        self.roll_count = roll_count
        self.roll_ival = roll_ival
        self.idle_timeout = idle_timeout

class AvroSinkConf(object):
    """
    A configuration struct for creating Avro sinks for FlumeAgents.
    
    @param name: The name of the sink.
    @type name: str
    @param host: The address of the node to which the sink should write its data.
    @type host: str
    @param port: The port of the node to which the sink should write its data.
    @type port: int
    """

    def __init__(self, name, host, port):
        self.name = name
        self.host = host
        self.port = port

class FlumeAgent(object):
    """
    Defines a single Flume agent whose configuration file is created by the user or is
    to be created by using one of the templates provided by the class. Additionally, 
    implements methods for startng, updating and stopping the agent.
    
    @param ag_conf: The configuration of the agent.
    @type ag_conf: AgentConf
    """

    flume = None
    """
    @cvar flume: The Cloudera Manager Flume service used by all the FlumeAgents in the system.
    @type flume: cm_api.endpoints.services.ApiService
    """
    
    def __init__(self, ag_conf):
        self.name = ag_conf.name
        self._conf = re.sub("\.conf$", "", ag_conf.conf.rstrip("/"))
        self._conf_f = FLUME_PATH + self._conf + ".conf"
        self._host = ag_conf.host
        self._role_created = False

    @classmethod
    def _check_flume_service(cls):
        """
        Determines if the Flume service has been initialized for the FlumeAgents.
        
        @raise FlumeException: Raised if the Flume service has not been initialized.
        """

        if cls.flume is None:
            msg = "Cloudera Manager Flume service has not been initialized for Flume agents!"
            raise FlumeException(msg)
        
    @classmethod
    def _get_all_roles(cls):
        """
        Returns all the Cloudera Manager roles from the Flume service used by the FlumeAgents

        @return: All the roles of the Flume service
        @rtype: sequence of cm_api.endpoints.roles.ApiRoles
        
        @raise FlumeException: Raised if the Flume service has not been initialized or an
                               error occurs while retrieving the roles from it.
        """

        cls._check_flume_service()

        try:
            return cls.flume.get_all_roles()
        except ApiException:
            msg = "Failed to retrieve existing Flume roles!"
            raise FlumeException(msg)

    def _check_role(self):
        """
        Determines if a Cloudera Manager role has been created for the agent.
        
        @raise AgentException: Raised if no role has been created.
        """

        if not self._role_created:
            msg = "Agent %s has no role!" % self.name
            raise AgentException(msg)

    def _get_role(self):
        """
        Returns the Cloudera Manager role of the agent.

        @return: The Cloudera Manager role of the agent.
        @rtype: cm_api.endpoints.roles.ApiRole

        @raise FlumeException: Raised if the Flume service has not been initialized or an
                               error occurs while retrieving the role from it.
        """

        self._check_flume_service()

        try:
            return self.flume.get_role(self.name)
        except ApiException as e:
            msg = "Failed to fetch role of agent %s! Reason: %s" % (self.name, e)
            raise FlumeException(msg) 

    def _get_role_state(self):
        """
        Returns the current state of the Cloudera Manager role of the agent.
        
        @return: The current state of the Cloudera Manager role of the agent.
        @rtype: str
        
        @raise FlumeException: Raised if the Flume service has not been initialized or an
                               error occurs while retrieving the role state from it.
        """
        
        return self._get_role().roleState

    def _read_conf(self, readlines=False):
        """
        Reads and returns the contents of the configuration file of the agent if the file has 
        been created.
        
        @param readlines: Determines if the configuration should be read using readlines or
                          read method.
        @type readlines: bool
        
        @return: The contents of the configuration file of the agent.
        @rtype: str or list of strings
        
        @raise AgentException: Raised if the configuration file does not exist or an error
                               occurs while reading it.
        """

        if not self.conf_exists():
            msg = "Conf file of agent %s does not exist!" % self.name
            raise AgentException(msg)
        try:
            with open(self._conf_f) as f:
                if readlines:
                    return f.readlines()
                return f.read()
        except IOError:
            msg = "Conf file %s of agent %s could not be read!" % (self._conf_f, self.name)
            raise AgentException(msg)

    def _create_component_conf(self, channels, srcs, sinks):
        """
        """

        if type(channels) != str:
            chn_str = " ".join(channels)
        else:
            chn_str = channels
        if type(srcs) != str:
            src_str = " ".join(srcs)
        else:
            src_str = srcs
        if type(sinks) != str:
            sink_str = " ".join(sinks)
        else:
            sink_str = sinks
        
        d = dict(ag=self.name, channels=chn_str, srcs=src_str, sinks=sink_str)
        return Template(
            "$ag.channels=$channels\n"
            "$ag.sources=$srcs\n"
            "$ag.sinks=$sinks"
            ).substitute(d)
    
    def _create_src_conf(self, src, channels):
        if type(channels) != str:
            chn_str = " ".join(channels)
        else:
            chn_str = channels
        d = dict(ag=self.name, 
                 src=src.name, 
                 bind=src.bind, 
                 port=src.port,
                 channels=chn_str)
        return Template(
            "$ag.sources.$src.type=netcat\n"
            "$ag.sources.$src.bind=$bind\n"
            "$ag.sources.$src.port=$port\n"
            "$ag.sources.$src.ack_every_event=false\n"
            "$ag.sources.$src.channels=$channels"
            ).substitute(d)

    def _create_avro_sink_conf(self, sink, channel):
        d = dict(ag=self.name,
                 sink=sink.name, 
                 host=sink.host, 
                 port=sink.port,
                 channel=channel)
        return Template(
            "$ag.sinks.$sink.type=avro\n"
            "$ag.sinks.$sink.hostname=$host\n"
            "$ag.sinks.$sink.port=$port\n"
            "$ag.sinks.$sink.channel=$channel"
            ).substitute(d)

    def _create_hdfs_sink_conf(self, sink, channel):
        path = HDFS_INCOMING_PATH + sink.storage.name + HOUR_DIR
        d = dict(ag=self.name,
                 sink=sink.name,
                 path=path, 
                 bsize=sink.batch_size, 
                 rcount=sink.roll_count, 
                 rinterval=sink.roll_ival, 
                 rsize=sink.roll_size,
                 timeout=sink.idle_timeout,
                 channel=channel)
        return Template(
            "$ag.sinks.$sink.type=hdfs\n"
            "$ag.sinks.$sink.hdfs.useLocalTimeStamp=true\n"
            "$ag.sinks.$sink.hdfs.path=$path\n"
            "$ag.sinks.$sink.hdfs.batchSize=$bsize\n"
            "$ag.sinks.$sink.hdfs.rollCount=$rcount\n"
            "$ag.sinks.$sink.hdfs.rollInterval=$rinterval\n"
            "$ag.sinks.$sink.hdfs.rollSize=$rsize\n"
            "$ag.sinks.$sink.hdfs.idleTimeout=$timeout\n"
            "$ag.sinks.$sink.hdfs.writeFormat=Text\n"
            "$ag.sinks.$sink.hdfs.fileType=DataStream\n"
            "$ag.sinks.$sink.hdfs.inUsePrefix=_\n"
            "$ag.sinks.$sink.channel=$channel"
            ).substitute(d)

    def _create_channel_conf(self, name):
        d = dict(ag=self.name, channel=name)
        s = Template(
            "$ag.channels.$channel.type=file\n"
            "$ag.channels.$channel.checkpointDir=./$channel/checkpoint\n"
            "$ag.channels.$channel.dataDirs=./$channel/data"
            ).substitute(d)
        return s

    def _write_conf(self, content):
        try:
            with open(self._conf_f, 'w') as f:
                f.write(content)
        except IOError as e:
            msg = "Failed to write conf file %s for agent %s!" % (self._conf_f, self.name)
            raise AgentException(msg)

    def conf_exists(self):
        return os.path.isfile(self._conf_f)

    def get_conf_sink_paths(self):
        paths = []
        content = self._read_conf(readlines=True)
        regex = "[^\s]*\.sinks\.[^\s]*\.hdfs\.path"
        for line in content:
            if re.match(regex, line) is not None:
                paths.append(line.partition("=")[-1].strip())
        return paths

    def create_simple_conf(self, src, sink):
        if self.conf_exists():
            msg = "Conf file %s for agent %s already exists!" % (self._conf_f, self.name)
            raise AgentException(msg)
        
        src_name = src.name
        sink_name = sink.name 
        content = (
            self._create_component_conf(FILE_CHANNEL, src_name, sink_name),
            self._create_channel_conf(FILE_CHANNEL),
            self._create_src_conf(src, FILE_CHANNEL),
            self._create_hdfs_sink_conf(sink, FILE_CHANNEL)
            )
        content = EMPTY_LINE.join(content)
        self._write_conf(content)

    def create_complex_conf(self, raw_src, avro_sink, rslt_ports, hdfs_sinks, 
                            raw_hdfs_sink=None):
        if self.conf_exists():
            msg = "Conf %s for agent %s already exists!" % (self._conf, self.name)
            raise AgentException(msg)
        if len(rslt_ports) != len(hdfs_sinks):
            msg = ("There must be an equal amount of ports listening analysis results "
                   "as there are sinks writing them to HDFS!")
            raise AgentException(msg)
        if len(rslt_ports) == 0:
            msg = "There must be at least one port listening analysis results!"
            raise AgentException(msg)

        # Defining channel names
        avro_chn_name = FILE_CHANNEL + "1"
        hdfs_chn_names = [FILE_CHANNEL + str(i + CHN_CNT_OFFSET) 
                          for i in range(len(hdfs_sinks))]
        chn_names = [avro_chn_name] + hdfs_chn_names
                          
        # Defining result sources
        rslt_srcs = [AgentSourceConf(RSLT_SRC + str(i + 1), INADDR_ANY, port)
                     for i, port in enumerate(rslt_ports)]

        # Defining source names
        src_names = [raw_src.name] + [src.name for name in rslt_srcs]

        # Defining sink names
        sink_names = [avro_sink.name] + [sink.name for sink in hdfs_sinks]

        # Ensure that there are no duplicate names
        names = chn_names + src_names + sink_names
        if len(names) > len(set(names)):
            msg = "Failed to create a conf file! Reason: duplicate names."
            raise AgentException(msg)
        
        # Create channel confs
        avro_chn_conf = self._create_channel_conf(avro_chn_name)
        rslt_chn_confs = [self._create_channel_conf(chn) for chn in rslt_chn_names]
        chn_confs = avro_chn_conf + EMPTY_LINE + EMPTY_LINE.join(rslt_chn_confs)
        
        # Create result source confs
        rslt_src_confs = [self._create_src_conf(src, rslt_chn_names[i])
                          for i, src in enumerate(rslt_srcs)]

        # Create sink confs
        avro_sink_conf = self._create_avro_sink_conf(avro_sink, avro_chn_name)
        hdfs_sink_confs = [self._create_hdfs_sink_conf(sink, rslt_chn_names[i]) 
                           for i, sink in enumerate(hdfs_sinks)]
        sink_confs = avro_sink_conf + EMPTY_LINE + EMPTY_LINE.join(hdfs_sink_confs)
        
        # If raw sink was given, create a conf for it and edit previous confs
        if raw_hdfs_sink is not None:
            raw_chn_name = FILE_CHANNEL + str(len(hdfs_chn_names) + CHN_CNT_OFFSET)
            chn_names.append(raw_chn_name)
            sink_names.append(raw_sink.name)
            raw_chn_conf = self._create_channel_conf(raw_chn_name)
            raw_src_conf = self._create_src_conf(raw_src, (avro_chn_name, raw_chn_name))
            raw_sink_conf = self._create_hdfs_sink_conf(raw_hdfs_sink, raw_chn_name)
            chn_confs += EMPTY_LINE + raw_chn_conf
            sink_confs += EMPTY_LINE + raw_sink_conf
        else:
            raw_src_conf = self._create_src_conf(raw_src, avro_chn_name)

        #Create source confs
        src_confs = raw_src_conf + EMPTY_LINE + EMPTY_LINE.join(rslt_src_confs)
        
        content = (
            self._create_component_conf(chn_names, src_names, sink_names),
            chn_confs,
            src_confs,
            sink_confs
            )
        content = EMPTY_LINE.join(content)
        self._write_conf(content)
         
    def is_initialized(self):
        if self._role_created:
            conf = self.get_role_config()
            if conf["agent_name"] == self.name and conf["agent_config_file"] == self.conf:
                return True
        return False

    def create_role(self):
        for role in self._get_all_roles():
            if role.name == self.name:
                msg = "Agent %s role already exists, no need to create again..." % self.name
                logging.info(msg)
                self._role_created = True
                return
        
        logging.info("Creating role for agent %s..." % self.name)
        try:
            self.flume.create_role(role_name=self.name, role_type="AGENT", host_id=self._host)
        except ApiException as e:
            msg = "Flume agent %s role creation failed! Reason: %s" % (self.name, e)
            raise FlumeException(msg)
        self._role_created = True
    
    def update_role_conf(self):
        content = self._read_conf()
        
        logging.info("Updating role configuration for agent %s..." % self.name)
        role = self._get_role()
        try:
            role.update_config({"agent_config_file" : content})
            role.update_config({"agent_name" : self.name})
        except ApiException as e:
            msg = "Failed to set conf for Flume agent %s! Reason: %s" % (self.name, e)
            raise FlumeException(msg)

    def get_role_config(self):
        self._check_role()
        role = self._get_role()

        try:
            return role.get_config()       
        except ApiException as e:
            msg = "Failed to read role config Flume agent %s! Reason: %s" % (self.name, e)
            raise FlumeException(msg)

    def delete_role(self):
        self._check_flume_service()
        self._check_role()
        logging.info("Deleting role of agent %s..." % self.name)
        
        try:
            self.flume.delete_role(self.name)
        except ApiException as e:
            msg = "Agent %s role deletion failed! Reason: %s" % (self.name, e)
            raise FlumeException(msg)
        self.role = False

    def start_role(self):
        self._check_flume_service()
        self._check_role()
        logging.info("Starting role of agent %s..." % self.name)

        role = self._get_role()
        try:
            if role.roleState == "STARTED":
                self.flume.restart_roles(self.name)
            else:
                self.flume.start_roles(self.name)
        except ApiException as e:
            msg = "Role of Flume agent %s ignored start command! Reason: %s" % (self.name, e)
            raise FlumeException(msg)
    
    def stop_role(self):
        self._check_flume_service()
        self._check_role()
        logging.info("Stopping role of agent %s..." % self.name)

        try:
            self.flume.stop_roles(self.name)
        except ApiException as e:
            msg = "Role of Flume agent %s ignored stop command! Reason: %s" % (self.name, e)
            raise FlumeException(msg)

    def is_role_started(self):
        if not self._role_created:
            return False
        return self._get_role_state() == "STARTED"

    def is_role_starting(self):
        if not self._role_created:
            return False
        return self._get_role_state() == "STARTING"

    def is_role_stopped(self):
        if not self._role_created:
            return True
        return self._get_role_state() == "STOPPED"

class FlumeException(Exception):
    pass

class AgentException(Exception):
    pass
