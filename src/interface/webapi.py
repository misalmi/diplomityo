# -*- coding: utf-8 -*-

import os
import sys
import hmac
import time
import logging
import subprocess
import multiprocessing
import socket
import importlib
import argparse
import Queue
from threading import Lock

import inspect
import Pyro4
import flask
from Crypto.Hash import SHA256
from flask.ext.restful import Api, Resource, abort
from werkzeug.exceptions import ClientDisconnected

sys.path.append("..")
sys.path.append("../core")
import table_admin
from shared import conf_admin, db_connector, tables, utils
from shared.constants import *
import interface

sys.path.append(IFACES)

IFACE_CONF_F = "interfaces.json"
IFACE_SERVER_NAME = "interface.server"
PARSE_METHOD = "parse_args"
QUERY_METHOD = "query"
RELOAD_CMD = "reload"

ifaces = {}
queue = multiprocessing.Queue()
imp_host = "127.0.0.1"
imp_port = 21050
lock = Lock()

class InterfaceServer(conf_admin.ConfAdmin):
    def __init__(self, queue):
        super(InterfaceServer, self).__init__(IFACE_CONF_F)
        self._queue = queue
        self._mod_names = set()
        self._connector = db_connector.SimpleConnector(imp_host, imp_port)
        
        for conf in self._read_from_conf(IFACES):
            try:
                name = conf[NAME]
                cls = conf[CLASS]
                data_tbls = conf[DATA_TABLES]
                rslt_tbls = conf[RESULT_TABLES]
            except KeyError as e:
                msg = IFACE_PARSE_FAILED % (conf, e)
                logging.exception(msg)
                continue
            try:
                iface = self._create_interface(name, cls, data_tbls, rslt_tbls)
            except utils.InterfaceInitException as e:
                logging.exception(str(e))
                continue
                
            self._queue.put((UPDATE_CMD, (name, iface)))

    def _parse_data_tables(self, confs):
        logging.info("Parsing data table pairs...")
        tbl_pairs = set()
        
        for conf in confs:
            try:
                name, cols, part_lvl = table_admin.TableAdmin.parse_table_conf(conf)
            except KeyError as e:
                msg = table_admin.DTBL_PARSE_FAILED % (conf, e)
                raise utils.InterfaceInitException(msg)
            except conf_admin.ConfException:
                msg = "Failed to parse data table pair from %s" % conf
                raise utils.InterfaceInitException(msg)
            
            try:
                table_admin.TableAdmin.check_database_table_pair(name, cols, part_lvl,
                                                                 self._connector)
            except table_admin.DatabaseTableException:
                msg = " Corresponding table pair %s not found in database!" % name
                raise utils.InterfaceInitException(msg)
            
            try:
                tbl = tables.DataTablePair(name, cols, part_lvl, self._connector)
            except (tables.SQLCharException, tables.PartitionLevelException) as e:
                raise utils.InterfaceInitException(msg)
            
            tbl_pairs.add(tbl)
            logging.info("Data table pair %s parsed!" % name)
        return tbl_pairs

    def _parse_result_tables(self, confs):
        logging.info("Parsing result tables...")
        rslt_tbls = set()

        for conf in confs:
            try:
                name, cols, part_lvl = table_admin.TableAdmin.parse_table_conf(conf)
            except KeyError as e:
                msg = table_admin.RTBL_PARSE_FAILED % (conf, e)
                raise utils.InterfaceInitException(msg)
            except conf_admin.ConfException:
                msg = "Failed to parse result table from %s" % conf
                raise utils.InterfaceInitException(msg)

            try:
                table_admin.TableAdmin.check_database_result_table(name, cols, part_lvl,
                                                                   self._connector)
            except DatabaseTableException:
                msg = "Corresponding result table %s not found in database!" % name
                raise utils.InterfaceInitException(msg)

            try:
            logging.exception(msg)
                break
            try:
                Pyro4.Daemon.serveSimple(
                    {server: IFACE_SERVER_NAME},
                    host=host,
                    port=daem_port,
                    ns=True)
            except Pyro4.errors.CommunicationError:
                pass
    except KeyboardInterrupt:
        msg = "Stopping Pyro daemon..."
        logging.info(msg)
    except socket.error:
        msg = "Could not connect to name server!"
        logging.exception(msg)
    p2.terminate()

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, 
                        format="%(levelname)s:%(filename)s: %(message)s")

    prog_desc = ("RESTful web API for distributed database. Acts also as a Pyro4 nameserver," 
                 "so the server can be updated on the fly.")
    parser = argparse.ArgumentParser(description=prog_desc)
    parser.add_argument("-k", "--key", help="Secret key for Pyro4", required=True, type=str)
    parser.add_argument("-sp", "--sport", help="Pyro4 server port", default=11111, type=int)
    parser.add_argument("-dp", "--dport", help="Pyro4 daemon port", default=11112, type=int)
    parser.add_argument("-ih", "--imhost", help="Impala host", default="127.0.0.1", type=str)
    parser.add_argument("-ip", "--import", help="Impala host", default=21050, type=int)
    args = vars(parser.parse_args())
    key = args["key"]
    serv_port = args["sport"]
    daem_port = args["dport"]
    imp_host = args["imhost"]
    imp_port = args["import"]

    if not os.path.isdir(IFACES):
        os.mkdir(IFACES)
    
    p = multiprocessing.Process(target=start_server, 
                                args=(queue, key, serv_port, daem_port))  
    p.start()
    time.sleep(5)
    if not p.is_alive():
        print "Failed to start server!"
        sys.exit()
    
    app = flask.Flask(__name__)
    api = Api(app)
    api.add_resource(DataList, "/data")
    api.add_resource(Data, "/data/<string:data_id>")
    try:
        app.run(host="127.0.0.1", debug=False)
    except socket.error:
        msg = " Could not run REST app!"
        logging.exception(msg)
    except KeyboardInterrupt:
        msg = " Stopping REST app..."
        logging.info(msg)
    p.terminate()
