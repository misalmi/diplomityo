# -*- coding: utf-8 -*-

import logging

# manager settings
manager_host = "172.31.0.101"
manager_user = "admin"
manager_passwd = "$:*.tiR3Nnw"
cluster_name = "cluster"
flume_name = "flume"

# impala setting
impala_host = "127.0.0.1"
impala_port = 21050

# miscellaneous settings
hdfs_path = "hdfs://172.31.0.101:8020/user/database/"
logging_level = logging.INFO
module_limit = 1000
iface_server = "interface.server"

# program path settings
spark_home = "/opt/cloudera/parcels/CDH-5.1.0-1.cdh5.1.0.p0.53/lib/spark/"
hadoop_home = "/opt/cloudera/parcels/CDH-5.1.0-1.cdh5.1.0.p0.53/lib/hadoop/"
hdfs_home = "/opt/cloudera/parcels/CDH-5.1.0-1.cdh5.1.0.p0.53/lib/hadoop-hdfs/"
mapreduce_home = "/opt/cloudera/parcels/CDH-5.1.0-1.cdh5.1.0.p0.53/lib/hadoop-mapreduce/"
yarn_home = "/opt/cloudera/parcels/CDH-5.1.0-1.cdh5.1.0.p0.53/lib/hadoop-yarn/"
flume_home = "/opt/cloudera/parcels/CDH-5.1.0-1.cdh5.1.0.p0.53/lib/flume-ng/"
hadoop_conf = "/etc/hadoop/conf/"

# spark streaming jars
avro_lib = "/opt/cloudera/parcels/CDH-5.1.0-1.cdh5.1.0.p0.53/lib/flume-ng/lib/avro.jar"
avro_ipc_lib = "/opt/cloudera/parcels/CDH-5.1.0-1.cdh5.1.0.p0.53/lib/flume-ng/lib/avro-ipc.jar"
flume_sdk_lib = "/opt/cloudera/parcels/CDH-5.1.0-1.cdh5.1.0.p0.53/lib/flume-ng/lib/flume-ng-sdk-1.5.0-cdh5.1.0.jar"
streaming_flume_lib = "/home/ec2-user/diplomityo/src/streaming/spark-streaming-flume_2.10-1.0.2.jar"

# spark launch settings
spark_driver_mem = "385m"
spark_executor_mem = "385m"
spark_num_executors = 3
spark_executor_cores = 3

def create():
    pass

create()
