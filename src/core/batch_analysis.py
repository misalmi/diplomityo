# -*- coding: utf-8 -*-

import sys
sys.path.append("..")

from shared import utils
from shared.constants import *

# TODO: Complete the methods for this base class to easily fetch the 
# relevant data files based on script properties.

class AnalysisScript(object):
    def __init__(self, data_tables, result_table, hdfs_util, spark):
        self.data_tables = data_tables
        self.result_table = result_table
        self.hdfs_util = hdfs_util
        self.spark = spark
        
    def run_analysis(self, dtime):
        print dtime
        result = self.launch_task(dtime)
        self.write_result(result, dtime)

    def launch_task(self, dtime):
        raise NotImplementedError("Subclasses should implement this method!")

    def write_result(self, result, dtime):
        print result
        if result:
            try:
                result = self.result_table.convert_result(result)
                print result
            except TypeError as e:
                msg = ("Failed to convert all analysis result rows to compatible" 
                       " format with result table! Reason: %s" % e)
                raise AnalysisResultException(msg)
            self.result_table.insert_into(result, dtime)

    def get_hour_data_path(self, data_id, dtime):
        return self.hdfs_util.get_archive_path_by_hour(data_id, dtime)

    def get_day_data_path(self, data_id, dtime):
        return self.hdfs_util.get_archive_path_by_day(data_id, dtime)

    def get_month_data_path(self, data_id, dtime):
        return self.hdfs_util.get_archive_path_by_month(data_id, dtime)

    def get_cache_data_path(self, data_id, dtime):
        pass

class AnalysisResultException(Exception):
    pass
