# -*- coding: utf-8 -*-

import os
import sys
import logging
import subprocess
from datetime import timedelta

import hadoopy

sys.path.append("..")
from shared import utils
from shared.constants import *

MONTH_DIR = "/month"
DAY_DIR = "/day"
HOUR_DIR = "/hour"

# TODO: The merged data files should be merged as SequenceFiles?
# Year directory also?
class HDFSUtility:
    def __init__(self, cache_path, archive_path):
        if not cache_path.endswith("/"):
            cache_path += "/"
        if not archive_path.endswith("/"):
            archive_path += "/"
        self.cache_path = cache_path
        self.archive_path = archive_path

    @staticmethod
    def merge_hdfs_dir(path, dest, filename):
        # TODO: Hadoop file crusher seems like a valid option here unless the data sizes are
        # too small for MapReduce. Should be used, when the system is moved to a cluster
        # hadoop jar filecrush-2.0-SNAPSHOT.jar crush.Crush -Ddfs.block.size=67108864 \
        # --input-format=text --output-format=text --compress=none path dest timestamp
        
        local = TMP_PATH + filename
        try:
            subprocess.check_call(["hadoop", "fs", "-getmerge", path, local])
        except subprocess.CalledProcessError:
            msg = "Failed to merge files in path %s" % path
            raise HDFSUtilityException(msg)
        try:
            hadoopy.put(local, dest)
        except IOError:
            msg = "Failed to write to hadoop directory %s" % dest
            raise HDFSUtilityException(msg)
        try:
            os.remove(local)
        except OSError:
            msg = "Failed to remove local file %s" % local
            raise HDFSUtilityException(msg)

    @staticmethod
    def remove_recursively(path):
        try:
            hadoopy.rmr(path)
        except IOError:
            msg = "Failed to delete directory %s" % path
            raise HDFSUtilityException(msg)

    def create_cache(self, data_id):
        try:
            hadoopy.mkdir(self.cache_path + data_id)
        except IOError:
            msg = "Failed to create cache directory %s" % data_id
            raise HDFSUtilityException(msg)
        logging.info("Created cache directory %s!" % data_id)

    def cache_exists(self, data_id):
        try:
            return hadoopy.isdir(self.cache_path + data_id)
        except IOError:
            msg = "Failed to check cache %s" % data_id
            raise HDFSUtilityException(msg)
    
    def archive_exists(self, data_id): 
        try:
            return hadoopy.isdir(self.archive_path + data_id)
        except IOError:
            msg = "Failed to check archive %s" % data_id
            raise HDFSUtilityException(msg)

    def remove_data_cache(self, data_id):
        HDFSUtility.remove_recursively(self.cache_path + data_id)
        logging.info("Removed data cache %s!" % data_id)

    def create_data_archive(self, data_id):
        path = self.archive_path + data_id
        try:
            hadoopy.mkdir(path)
            hadoopy.mkdir(path + MONTH_DIR)
            hadoopy.mkdir(path + DAY_DIR)
            hadoopy.mkdir(path + HOUR_DIR)
        except IOError:
            msg = "Failed to create data archive %s" % path
            raise HDFSUtilityException(msg)
        else:
            logging.info("Created data archive %s!" % data_id)

    def remove_data_archive(self, data_id):
        HDFSUtility.remove_recursively(self.archive_path + data_id)
        logging.info("Removed data archive %s!" % data_id)
    
    def get_archive_path_by_hour(self, data_id, dtime):
        path = self.archive_path + data_id + HOUR_DIR + "/"
        filename = data_id + "-day=%02d-hour=%02d" % (dtime.day, dtime.hour) 
        return path + filename

    def get_archive_path_by_day(self, data_id, dtime):
        path = self.archive_path + data_id + DAY_DIR + "/"
        filename = data_id + "-month=%02d-day=%02d" % (dtime.month, dtime.day) 
        return path + filename
    
    def get_archive_path_by_month(self, data_id, dtime):
        path = self.archive_path + data_id + MONTH_DIR + "/"
        filename = data_id + "-year=%02d-month=%02d" % (dtime.year, dtime.month) 
        return path + filename

    def perform_hourly_merge(self, data_id, dtime):
        path = self.cache_path + data_id + "/hour=%02d" % dtime.hour
        dest = self.archive_path + data_id + HOUR_DIR
        filename = data_id + "-day=%02d-hour=%02d" % (dtime.day, dtime.hour)
        try:
            HDFSUtility.merge_hdfs_dir(path, dest, filename)
        except HDFSUtilityException as e:
            logging.exception(str(e))

    def perform_daily_merge(self, data_id, dtime):
        self.perform_hourly_merge(data_id, dtime)
        try:
            self._merge_data_of_day(data_id, dtime)
        except HDFSUtilityException as e:
            logging.exception(str(e))    

    def perform_monthly_merge(self, data_id, dtime):
        self.perform_daily_merge(data_id, dtime)
        try:
            self._merge_data_of_month(data_id, dtime)
        except HDFSUtilityException as e:
            logging.exception(str(e))

    def perform_hourly_removal(self, data_id, dtime):
        path = self.cache_path + data_id + "/"
        filename = "hour=%02d" % dtime.hour
        HDFSUtility.remove_recursively(path + filename)

    def perform_daily_removal(self, data_id, dtime):
        self.perform_hourly_removal(data_id, dtime)
        self._remove_data_of_prev_day(data_id, dtime)

    def perform_monthly_removal(self, data_id, dtime):
        self.perform_daily_removal(data_id, dtime)
        self._remove_data_of_prev_month(data_id, dtime)

    def _merge_data_of_day(self, data_id, dtime):
        path = self.archive_path + data_id + HOUR_DIR
        dest = self.archive_path + data_id + DAY_DIR
        filename = data_id + "-month=%02d-day=%02d" % (dtime.month, dtime.day)
        HDFSUtility.merge_hdfs_dir(path, dest, filename)

    def _merge_data_of_month(self, data_id, dtime):
        path = self.archive_path + data_id + DAY_DIR
        dest = self.archive_path + data_id + MONTH_DIR
        filename = data_id + "-year=%d-month=%02d" % (dtime.year, dtime.month)
        HDFSUtility.merge_hdfs_dir(path, dest, filename)

    def _remove_data_of_prev_day(self, data_id, dtime):
        path = self.archive_path + data_id + HOUR_DIR + "/"
        filename = data_id + "-day=%02d*" % (dtime - timedelta(hours=1)).day
        HDFSUtility.remove_recursively(path + filename)

    def _remove_data_of_prev_month(self, data_id, dtime):
        path = self.archive_path + data_id + DAY_DIR + "/"
        filename = data_id + "-month=%02d*" % (dtime - timedelta(days=1)).month
        HDFSUtility.remove_recursively(path + filename)

class HDFSUtilityException(Exception):
    pass
