# -*- coding: utf-8 -*-

import logging
import socket
import urllib2
import time
from lxml import etree
from datetime import datetime
from multiprocessing import Process

def parse_fields(xml):
    fields = []
    for field in xml:
        if field.tag == "Coordinates":
            fields.append(field.attrib["X"] + "," + field.attrib["Y"])
        elif field.text is None:
            fields.append("")
        elif field.tag == "Time":
            fields.append(field.text.replace("T", " ").replace("+00:00", ""))
        else:
            fields.append(field.text)
    return fields

def parse_xml(xml):
    events = [(incident.attrib["ID"], observation.attrib["ID"]) + 
              tuple(parse_fields(observation.getchildren()))
              for incident in etree.fromstring(xml).getchildren()
              for observation in incident.getchildren()]
    return [",".join(event) for event in events]


def forward_to_socket(contents, sock):
    [sock.send(event + "\n") for event in parse_xml(contents)]

def compose_filename(t):
    return datetime.strftime(t, "%G-%m-%dT%H:%M:%S")

def get_and_forward(url, t, sock):
    try:
        contents = urllib2.urlopen(url + compose_filename(t)).read()
    except urllib2.HTTPError:
        pass
    else:
        forward_to_socket(contents, sock)

def poll_and_forward(url, t_poll, sock):
    t_now = datetime.now()
    t_start = t_now
    try:
        while True:
            while (datetime.now() - t_now).seconds < 1:
                time.sleep(0.5)
            t_now = datetime.now()
            t = t_poll + (t_now - t_start)
            Process(target=get_and_forward, args=(url, t, sock)).start()
    except (KeyboardInterrupt, socket.error):
        sock.close()
            
def connect(host, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))
    return sock

def start(url, t, host, port):
    try:
        sock = connect(host, port)
    except socket.error:
        logging.exception(" Failed to connect to %s:%s" % (host, port))
    else:
        if not url.endswith("/"):
            url += "/"
        poll_and_forward(url, t, sock)

if __name__ == "__main__":
    start(url="https://cooperativetraffic.blob.core.windows.net/taxi/",
          t=datetime(2012, 10, 18, 10, 1, 25),
          host="127.0.0.1",
          port=11111)
