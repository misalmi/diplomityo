# -*- coding: utf-8 -*-

import subprocess
import os
import re
import time
import logging
from string import Template

from cm_api.api_client import ApiResource, ApiException

import flume_agent
import stream_analysis
from shared.constants import *

MODULE_PATH = os.path.realpath(__file__).rsplit("/", 1)[0] + "/" 

SOURCE_INIT_FAILED = "Cannot initialize source %s! Reason: "

BIND = "bind"
BATCH_SIZE = "batch_size"
ROLL_SIZE = "roll_size"
ROLL_COUNT = "roll_count"
ROLL_INTERVAL = "roll_interval"
IDLE_TIMEOUT = "idle_timeout"
            
class DataSource(object):
    """
    Describes how a data stream or data streams from a single external data source are 
    received and processed, including the Flume agents and Spark Streaming analyses 
    manipulating it, as well as the Hadoop storages to which the data is saved. On 
    initialization, creates agent roles and compiles analyses.

    @param name: The name of the DataSource object.
    @type name: str
    @param storages: Storage names to which the source data is saved.
    @type storages: sequence of strings
    @param agents: Agents receiving and manipulating data from the external source.
    @type agents: sequence of FlumeAgents
    @param analyses: Spark Streaming analyses manipulating data from the external source.
    @type analyses: sequence of StreamAnalyses
    
    @raise SourceException: Raised on object instantation error, such as: 
                            - An empty sequence of storages or agents is received.
                            - Storages with duplicate names are received.
                            - Agents with duplicate names are received.
                            - Analyses with duplicate names are received.
                            - If the storages do not match the sinks defined in agent 
                              configuration files.
    """

    def __init__(self, name, storages, agents, analyses=[]):
        self.name = name
        self.storages = storages

        # Check that one or more storages and agents have been given.
        if len(agents) == 0:
            msg = SOURCE_INIT_FAILED + "no agents were given" % self.name
            raise SourceException(msg)
        elif len(storages) == 0:
            msg = SOURCE_INIT_FAILED + "no output storages were given." % self.name
            raise SourceException(msg)
            
        # Check storage names for duplicates and create HDFS paths corresponding to names.
        stor_paths = set(HDFS_INCOMING_PATH + s + HOUR_DIR for s in storages)
        if len(stor_paths) > len(storages):
            msg = SOURCE_INIT_FAILED + "duplicate storage names detected" % self.name
            raise SourceException(msg)

        # Check agent names for duplicates.
        ag_names = set(ag.name for ag in agents)
        if len(ag_names) > len(agents):
            msg = SOURCE_INIT_FAILED + "duplicate agent names detected" % self.name
            raise SourceException(msg)
            
        # Check analysis names for duplicates
        sa_names = set(sa.name for sa in analyses)
        if len(sa_names) > len(analyses):
            msg = SOURCE_INIT_FAILED + "duplicate agent names detected" % self.name
            raise SourceException(msg)

        # Check that HDFS paths of the agent sinks correspond to the storage paths.
        sink_paths = set()
        for ag in agents:
           sink_paths.update(ag.get_conf_sink_paths())
        if sink_paths != stor_paths:
            msg = (SOURCE_INIT_FAILED % self.name + 
                   "the paths of storages and agent sinks do not correspond")
            raise SourceException(msg)

        # Initialize agents.
        self._agents = {}
        for ag in agents:
            try:
                ag.create_role()
                ag.update_role_conf()
            except (flume_agent.AgentException, flume_agent.FlumeException):
                logging.exception("Failed to set up Flume agent %s!" % ag.name)
            self._agents[ag.name] = ag

        # Initialize analyses.
        self._analyses = {}
        for sa in analyses:
            try:
                sa.compiled = True
                sa.compile()
            except stream_analysis.StreamAnalysisException:
                logging.exception("Failed to compile Spark Streaming analysis %s!" % sa.name)
            self._analyses[sa.name] = sa

    def _get_agent(self, name):
        """
        Returns a Flume agent associated with the DataSource by name.

        @param name: The name of the Flume agent
        @type name: str
        
        @return: The Flume agent
        @rtype: FlumeAgent
        
        @raise SourceException: Raised if a Flume agent with the given name is not found. 
        """

        try:
            return self._agents[name]
        except KeyError:
            msg = "DataSource %s has no Flume agent %s!" % (self.name, name)
            raise SourceException(msg)

    def _get_analysis(self, name):
        """
        Returns a Spark Streaming analysis associated with the DataSource by name.

        @param name: The name of the Spark Streaming analysis.
        @type name: str
        
        @return: The Spark Streaming analysis
        @rtype: StreamAnalysis
        
        @raise SourceException: Raised if a Spark Streaming analysis with the given name is not
                                found.
        """

        try:
            return self._analyses[name]
        except KeyError:
            msg = "DataSource %s has no Spark Streaming analysis named %s!" % (self.name, name)
            raise SourceException(msg)

    def is_initialized(self):
        """
        Determines if the DataSource is initialized. That is, if agent roles have been 
        succesfully created as well as updated according to the agent configuration, and the 
        analyses have been compiled.
        
        @return: True if the DataSource is initialized, otherwise False.
        @rtype: bool

        @raise FlumeException: Raised if fails to connect to Flume service to check agent 
                               status.
        """

        if (all(ag.is_initialized() for ag in self._agents.itervalues()) and 
            all(sa.compiled for sa in self._analyses.itervalues())):
            return True
        return False

    def is_running(self):
        """
        Determines if the DataSource is running. That is, if agent roles have been succesfully
        started and analyses are running.
        
        @return: True if the DataSource is running, otherwise False.
        @rtype: bool

        @raise FlumeException: Raised if an exception occurs when reading the state of an agent 
                               role. 
        """
        
        all_agents_started = all(ag.is_role_started() or ag.is_role_starting() 
                                 for ag in self._agents.itervalues())
        all_analyses_running = all(sa.is_running() for sa in self._analyses.itervalues())
        if (all_agents_started and all_analyses_running):
            return True
        return False

    def is_partly_running(self):
        """
        Determines if the DataSource is partly running. That is, if any agent roles are 
        started or any analyses running.
        
        @return: True if the DataSource is partly running, otherwise False.
        @rtype: bool
        
        @raise FlumeException: Raised if an exception occurs when reading the state of an agent
                               role.
        """

        any_agent_started = any(ag.is_role_started() or ag.is_role_starting() 
                                for ag in self._agents.itervalues())
        any_analysis_running = any(sa.is_running() for sa in self._analyses.itervalues())
        if (any_agent_started or any_analysis_running):
            return True
        return False
    
    def create_agent_role(self, name):
        """
        Creates a Cloudera Manager service role for a specified Flume agent associated with 
        the DataSource.
        
        @param name: The name of the Flume agent.
        @type name: str

        @raise SourceException: Raised if a Flume agent with the given name is not found.
        @raise FlumeException: Raised if an exception occurs when creating the role of the
                                Flume agent.
        """

        agent = self._get_agent(name)
        agent.create_agent_role()

    def update_agent_conf(self, name):
        """
        Updates the Cloudera Manager service role configuration, according to the agent
        configuration, for a specified Flume agent associated with the DataSource.
        
        @param name: The name of the Flume agent.
        @type name: str

        @raise SourceException: Raised if a Flume agent with the given name is not found.
        @raise AgentException: Raised if no role has been yet created for the Flume agent.
        @raise FlumeException: Raised if an exception occurs when updating the role of the
                               Flume agent.
        """
        
        self._get_agent(name).update_role_conf()
     
    def init_agent(self, name):
        """
        Initializes a specified Flume agent associated with the DataSource, by creating a
        Cloudera Manager service role for it and updating the role configuration according
        to the agent configuration.

        @param name: The name of the Flume agent.
        @type name: str

        @raise SourceException: Raised if a Flume agent with the given name is not found.
        @raise FlumeException: Raised if an exception occurs when creating or updating the 
                               role of the Flume agent.
        """

        agent = self._get_agent(name)
        agent.create_role()
        agent.update_role_conf()
        
    def delete_agent_role(self, name):
        """
        Deletes the Cloudera Manager service role of a specified Flume agent associated with 
        the DataSource.
        
        @param name: The name of the Flume agent.
        @type name: str

        @raise SourceException: Raised if a Flume agent with the given name is not found.
        @raise AgentException: Raised if no role has been yet created for the Flume agent.
        @raise FlumeException: Raised if an exception occurs when deleting the role of the
                               Flume agent.
        """

        self._get_agent(name).delete_role()
        
    def start_agent(self, name):
        """
        Starts the Cloudera Manager service role of a specified Flume agent associated with 
        the DataSource.
        
        @param name: The name of the Flume agent.
        @type name: str

        @raise SourceException: Raised if a Flume agent with the given name is not found.
        @raise AgentException: Raised if no role has been yet created for the Flume agent.
        @raise FlumeException: Raised if an exception occurs when starting the role of the
                               Flume agent.
        """

        self._get_agent(name).start_role()
        
    def stop_agent(self, name):
        """
        Stops the Cloudera Manager service role of a specified Flume agent associated with 
        the DataSource.
        
        @param name: The name of the Flume agent.
        @type name: str

        @raise SourceException: Raised if a Flume agent with the given name is not found.
        @raise AgentException: Raised if no role has been yet created for the Flume agent.
        @raise FlumeException: Raised if an exception occurs when stopping the role of the
                               Flume agent.
        """
        
        self._get_agent(name).stop_role()

    def start_agents(self):
        """
        Starts all the Flume agents associated with the DataSource. Exceptions are not raised on
        failures.
        """

        for ag in self._agents.itervalues():
            try:
                ag.start_role()
            except (flume_agent.AgentException, flume_agent.FlumeException) as e:
                logging.exception("Failed to start agent %s! Reason: %s" % (ag.name, e))

    def stop_agents(self):
        """
        Stops all the Flume agents associated with the DataSource. Exceptions are not raised on
        failures.
        """

        for ag in self._agents.itervalues():
            try:
                ag.stop_role()
            except (flume_agent.AgentException, flume_agent.FlumeException) as e:
                logging.exception("Failed to stop agent %s! Reason: %s" % (ag.name, e))

    def compile_analysis(self, name):
        """
        Compiles the specified Spark Streaming analysis associated with the DataSource.
        
        @param name: The name of the streaming analysis.
        @type name: str

        @raise SourceException: Raised if a streaming analysis with the given name is not 
                                found.
        @raise StreamingAnalysisException: Raised if the compilation of the analysis fails. 
        """

        self._get_analysis(name).compile()
        
    def run_analysis(self, name):
        """
        Runs the specified Spark Streaming analysis associated with the DataSource.
        
        @param name: The name of the streaming analysis.
        @type name: str

        @raise SourceException: Raised if a streaming analysis with the given name is not 
                                found.
        @raise StreamingAnalysisException: Raised if the analysis fails to run. 
        """

        self._get_analysis(name).run()

    def stop_analysis(self, name):
        """
        Stops the specified Spark Streaming analysis associated with the DataSource.
        
        @param name: The name of the streaming analysis.
        @type name: str

        @raise SourceException: Raised if a streaming analysis with the given name is not 
                                found.
        @raise StreamingAnalysisException: Raised if the analysis fails to stop. 
        """

        self._get_analysis(name).stop()
        
    def run_analyses(self):
        """
        Runs all the Spark Streaming analyses associated with the DataSource. Exceptions are 
        not raised on failures.
        """

        for sa in self._analyses.itervalues():
            try:
                sa.run()
            except stream_analysis.StreamAnalysisException as e:
                logging.exception("Failed to run streaming analysis %s!" % sa.name)
        
    def stop_analyses(self):
        """
        Stops all the Spark Streaming analyses associated with the DataSource. Exceptions are 
        not raised on failures.
        """

        for sa in self._analyses.itervalues():
            try:
                sa.stop()
            except stream_analysis.StreamAnalysisException as e:
                logging.exception("Failed to stop streaming analysis %s!" % sa.name)

    def run(self):
        """
        Starts all the Flume agents and runs all the Spark Streaming analyses associated with 
        the DataSource. Exceptions are not raised on failures.
        """
        
        self.start_agents()
        self.run_analyses()

    def stop(self):
        """
        Stops all the Flume agents and the Spark Streaming analyses associated with the 
        DataSource. Exceptions are not raised on failures.
        """

        self.stop_agents()
        self.stop_analyses()

    def list_agents(self):
        """
        Returns the names of the Flume agents associated with the DataSource.

        @return: The names of the Flume agents
        @rtype: list of strings
        """

        return [ag.name for ag in self._agents.itervalues()]

    def list_analyses(self):
        """
        Returns the names of the Spark Streaming analyses associated with the DataSource.

        @return: The names of the Spark Streaming analyses
        @rtype: list of strings
        """
        
        return [sa.name for sa in self._analyses.itervalues()]

    def list_initialized_agents(self):
        """
        Returns the names of the initialized Flume agents associated with the DataSource.
        That is, the agents whose role has been succesfully created and updated according
        to the agent configuration.

        @return: The names of the Flume agents
        @rtype: list of strings

        @raise FlumeException: Raised if fails to connect to Flume service to check an agent
                               status.
        """

        return [ag.name for ag in self._agents.itervalues() if ag.is_initialized()]

    def list_compiled_analyses(self):
        """
        Returns the names of the compiled Spark Streaming analyses associated with the 
        DataSource.

        @return: The names of the Spark Streaming analyses
        @rtype: list of strings
        """
        
        return [sa.name for sa in self._analyses.itervalues() if sa.compiled]
            
    def list_started_agents(self):
        """
        Returns the names of the started Flume agents associated with the DataSource.

        @return: The names of the Flume agents
        @rtype: list of strings

        @raise FlumeException: Raised if an exception occurs when reading the state of an agent 
                               role.
        """
        
        return [ag.name for ag in self._agents.itervalues() if ag.is_role_started()]
    
    def list_stopped_agents(self):
        """
        Returns the names of the stopped Flume agents associated with the DataSource.

        @return: The names of the Flume agents
        @rtype: list of strings

        @raise FlumeException: Raised if an exception occurs when reading the state of an agent 
                               role.
        """

        return [ag.name for ag in self._agents.itervalues() if ag.is_role_stopped()]

    def list_running_analyses(self):
        """
        Returns the names of the running Spark Streaming analyses associated with the 
        DataSource.

        @return: The names of the Spark Streaming analyses
        @rtype: list of strings
        """
        
        return [sa.name for sa in self._analyses.itervalues() if sa.is_running()]

    def list_stopped_analyses(self):
        """
        Returns the names of the stopped Spark Streaming analyses associated with the 
        DataSource.

        @return: The names of the Spark Streaming analyses
        @rtype: list of strings
        """
        
        return [sa.name for sa in self._analyses.itervalues() if not sa.is_running()]

class SourceException(Exception):
    """
    An exception which is raised when an error associated with a DataSource object is 
    encountered.
    """
    pass

if __name__ == "__main__":
    # cluster = ApiResource(server_host="192.168.109.100", username="admin", password="$:*.tiR3Nnw").get_cluster("cluster")
    # src_name = "taxidata"
    # flume = cluster.get_service("flume")
    # agents = [{"name" : "hdfs_agent",
    #            "conf" : "flume_agent",
    #            "host" : "datanode1.traffic.pilot"}]
    # streamers = [{"name" : "TaxidataStream",
    #               "client" : "yarn-client",
    #               "host" : "datanode1.traffic.pilot",
    #               "port" : "11112",
    #               "batch_interval" : "10000",
    #               "args" : ["datanode1.traffic.pilot", "11113", "datanode1.traffic.pilot", "11114"]}]
    # outputs = ["taxidata", "trafficfluency"]
    # src = DataSource(src_name, flume, agents, streamers, outputs)
    # src.run_source()
    logging.basicConfig(level=logging.INFO)
    ag = FlumeAgent("test_agent", "test", None, None)
    raw_src = dict(name="raw_src", host="127.0.0.1", port="11111")
    stream_sink = dict(name="stream_sink", host="192.168.109.100", port="11112")
    stream_srcs = [dict(name="output1_src", port="11113"), dict(name="output2_src", port="11114")]
    output_sinks = [dict(name="output1_sink", path="hdfs://namenode.traffic.pilot:8020/user/database/cache/output1/hour=%H"), dict(name="output2_sink", path="hdfs://namenode.traffic.pilot:8020/user/database/cache/output2/hour=%H")]
    raw_sink = dict(name="raw_sink", path="hdfs://namenode.traffic.pilot:8020/user/database/cache/raw/hour=%H")
    #ag.create_complex_conf(raw_src, stream_sink, stream_srcs, output_sinks, raw_sink)
    ag.create_simple_conf(raw_src, raw_sink)
