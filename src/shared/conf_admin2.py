# -*- coding: utf-8 -*-

import os
import logging
import json

CONF_NOT_FOUND = "Conf file %s was not found!"

class ConfAdmin(object):
    def __init__(self, conf_f):
        if os.path.isfile(conf_f):
            self._conf_f = conf_f
        else:
            msg = CONF_NOT_FOUND % conf_f
            raise ConfException(msg)

    @staticmethod
    def _create_missing_key_msg(target, conf, key):
        return "Failed to parse %s conf %s! Missing_key: %s" % (target, conf, key)

    @staticmethod
    def _create_parse_failure_msg(target, err):
        return "Failed to parse %s! Reason: %s" % (target, err) 

    def _read_conf(self):
        try:
            with open(self._conf_f) as f:
                data = json.load(f)
        except IOError:
            msg = CONF_NOT_FOUND % self._conf_f
            raise ConfException(msg)
        except ValueError as e:
            msg = "Corrupted JSON data in conf file %s: %s" % (self._conf_f, e)
            raise ConfException(msg)
        return data

    def _read_from_conf(self, group):
        data = self._read_conf()
        try:
            return dict(data[group])
        except KeyError as e:
            msg = "Conf %s has no group %s!" % (self._conf_f, group)
            raise ConfException(msg)
        except (TypeError, ValueError) as e:
            msg = "Corrupted data in conf file %s in group %s: %s" % (self._conf_f, group, e)

    def _write_conf(self, data):
        try:
            with open(self._conf_f, 'w') as f:
                json.dump(data, f)
        except IOError:
            msg = CONF_NOT_FOUND % self._conf_f
            raise ConfException(msg)

    def _update_conf(self, group, item, conf):
        data = self._read_conf()
        try:
            data[group][item] = conf
        except (KeyError, AttributeError) as e:
            msg = "Failed to update item %s in group %s of conf file %s!" % (item, key, 
                                                                             self._conf_f)
            raise ConfException(msg)
        self._write_conf(data)

    def _remove_from_conf(self, group, item):
        data = self._read_conf()
        try:
            del data[group][item]
        except KeyError:
            msg = "Failed to remove item %s in group %s from conf file %s!" % (group, item, 
                                                                               self._conf_f)
            raise ConfException(msg)
        self._write_conf(data) 

class ConfException(Exception):
    pass
