# -*- coding: utf-8 -*-

import paramiko
import subprocess
import scp
import select
import os
import logging
import socket
import urllib2
from cm_api.api_client import ApiResource, ApiException

SSH_PORT = 22
COMPILER_PATH = "../../sbin/scala_compile.sh"
LAUNCHER_PATH = "../../../sbin/spark_launcher.sh"
PARENT_SRC_PATH = "../streaming/FlumeStreamLauncher.scala"
PACKAGE_NAME = "flumestream"
SOURCE_PATH = "../../user/sources/"
MODULE_PATH = os.path.realpath(__file__).rsplit("/", 1)[0] + "/"
#REMOTE_INGESTION_PATH = "ingestion/"
#REMOTE_BIN_PATH = REMOTE_INGESTION_PATH + "bin/"
#REMOTE_LIB_PATH = REMOTE_INGESTION_PATH + "lib/"
#REMOTE_STREAMING_PATH = REMOTE_INGESTION_PATH + "streaming/"
#REMOTE_LAUNCHER_PATH = "../../sbin/spark_launcher.sh"
#DIR_EXISTS_ERR = "File exists"

def has_agent_dec(f):
    def has_agent(self, ag_name, *args):
        if not any(agent.name != ag_name for agent in self.agents):
            msg = "Source has no agent named %s!" % ag_name
            raise AgentException(msg)
        return f(self, ag_name, *args)
    return has_agent

def has_role_dec(f):
   def has_role(self, ag_name, *args):
        if not any(role.name != ag_name for agent in self._agent_roles):
            msg = "Source has no agent named %s with role!" % ag_name
            raise AgentException(msg)
        return f(self, ag_name, *args)
    return has_role

class DataSource(object):
    def __init__(self, name, flume, agents, streamers, save_raw=True):
        self.name = name
        self.flume = flume
        self.agents = agents
        self.streamers = streamers
        self.save_raw = save_raw
        self._agent_roles = []
        for agent in agents:
            try:
                self.setup_agent_role(agent.name, agent.conf, agent.server)
            except AgentException as e:
                logging.exception(" " + str(e))
        
        self._streamer_procs = []

    @staticmethod
    def compile_script(compile_cmd, script_d, script_f, parent_f, output_f):
        subprocess.check_call([compile_cmd, script_d, parent_f, script_f])
        subprocess.check_call(["jar", "cf", output_f, "-C", script_d, PACKAGE_NAME])
        subprocess.check_call(["rm", "-rf", script_d + PACKAGE_NAME])

    @staticmethod
    def compile_script_and_check(compile_cmd, script_d, script_f, parent_f, output_f):
        try:
            compile_script(compile_cmd, script_d, script_f, parent_f, output_f)
        except subprocess.CalledProcessError:
            msg = " Failed to compile script %s!" % script_f
            logging.exception(msg)
        except OSError:
            msg = " Could not find script compiler %s" % compile_cmd
            logging.exception(msg)
        else:
            return True
        return False

# # http://stackoverflow.com/questions/250283/how-to-scp-in-python
# def create_ssh_client(server, user, passwd):
#     client = paramiko.SSHClient()
#     client.load_system_host_keys()
#     client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
#     client.connect(server, SSH_PORT, user, passwd)
#     return client

# def exec_remote_and_check(ssh, cmd):
#     _, stdout, stderr = ssh.exec_command(cmd)
#     if stdout.channel.recv_exit_status() == 0:
#         return True
#     err_descript = stderr.read().split(": ")[-1].rstrip()
#     if err_descript == DIR_EXISTS_ERR:
#         print "what"
#         return True
#     return False

# def create_remote_dir(ssh, path):
#     if not exec_remote_and_check(ssh, "mkdir " + path):
#         msg = " Failed to create remote dectory %s!" % path
#         logging.error(msg)
#         return False
#     return True

# def create_remote_dir_struct(ssh, target_d):
#     dirs = (REMOTE_INGESTION_PATH, 
#             REMOTE_BIN_PATH, 
#             REMOTE_LIB_PATH, 
#             REMOTE_FLUME_PATH, 
#             REMOTE_STREAMING_PATH)
#     for d in dirs:
#         if not create_remote_dir(target_d + d):
#             return False
#     return True

# def scp_file(ssh, f, remote_d):
#     scp_client = scp.SCPClient(ssh.get_transport())
#     scp_client.put(f, remote_d)

# def scp_file_and_check(ssh, f, remote_d):
#     try:
#         scp_file(ssh, f, remote_d)
#     except (scp.SCPException, OSError):
#         msg = " Remote copy of file %s to remote directory %s failed!" % (f, remote_d)
#         logging.exception(msg)
#     except IOError:
#         msg = " File %s was not found!" % f
#         logging.exception(msg)
#     else:
#         return True
#     return False

# def scp_script_output(ssh, output_f, remote_d):
#     if not create_remote_d(ssh, remote_d):
#         return False
#     if not scp_file_and_check(ssh, output_f, remote_d):
#         return False
#     return True

    # https://github.com/onefoursix/cm-api-flume-example

    def _get_agent_role(self, ag_name):
        try:
            return self.flume.get_role(ag_name)
        except ApiException:
            msg = "Failed to retrieve role of Flume agent %s!" % ag_name
            raise AgentException(msg)

    def _create_agent_role(self, ag_name, server):
        try:
            roles = self.flume.get_all_roles()
        except ApiException:
            msg = "Failed to retrieve existing Flume roles!"
            raise AgentException(msg)

        for role in roles:
            if role.name == ag_name:
                msg = " Agent %s role already exists, no need to create again..." % ag_name
                logging.info(msg)
                return role
        try:
            return self.flume.create_role(role_name=ag_name, role_type="AGENT", host_id=server)
        except ApiException:
            msg = "Flume agent %s role creation failed!" % ag_name
            raise AgentException(msg)
    
    @has_agent_dec
    def create_agent_role(self, ag_name, server):
        self._create_agent_role(ag_name, server)

    def _update_agent_role_conf(self, ag_name, ag_conf, role):
        conf_f = MODULE_PATH + SOURCE_PATH + self.name + "/flume/" + ag_conf + ".conf"
        try:
            with open(conf_f) as f:
                conf = f.read()
        except IOError:
            msg = "Conf file %s of agent %s was not found!" % (conf_f, ag_name)
            raise AgentException(msg)
        try:
            role.update_config({"agent_config_file" : conf})
            role.update_config({"agent_name" : ag_name})
        except ApiException:
            msg = "Failed to set conf for Flume agent %s!" % ag_name
            raise AgentException(msg)

    @has_role_dec
    def update_agent_role_conf(self, ag_name):
        
        update_agent_role_conf(ag_name, role)
    
    def _setup_agent_role(self, ag_name, ag_conf, ag_server):
        role = self._create_agent_role(ag_name, ag_server)
        self._agent_roles.append(role)
        self._update_agent_conf(ag_name, ag_conf, role)

    @has_agent_dec
    def setup_agent_role(self, ag_name, ag_server):
        self._setup_agent_role(ag_name, ag_server)

    @has_role_dec
    def delete_agent_role(self, ag_name):
        try:
            self.flume.delete_role(ag_name)
        except ApiException:
            msg = "Agent %s role deletion failed!" % ag_name
            raise AgentException(msg)
    
    def _start_role(self, role):
        try:
            if role.roleState == "STARTED":
                self.flume.restart_roles(role.name)
            else:
                self.flume.start_roles(role.name)
        except ApiException:
            msg = "Failed to start Flume agent role %s!" % role.name
            raise AgentException(role.name)

    @has_role_dec
    def start_agent_role(self, ag_name):
        role = self._get_agent_role(ag_name)
        self._start_role(role)

    def _stop_agent_role(self, ag_name):
        try:
            self.flume.stop_roles(ag_name)
        except ApiException:
            msg = "Failed to stop Flume agent role %s!" % ag_name
            raise AgentException(ag_name)

    @has_role_dec
    def stop_agent_role(self, ag_name):
        self._stop_agent_role(ag_name)

    def start_agent_roles(self):
        [self._start_role(role) for role in self._agent_roles]

    def stop_agent_roles(self):
        [self._stop_agent_role(role.name) for role in self._agent_roles]
            
def launch_script(script, server, user, passwd, module_d, target_d):
    compile_cmd = module_d + COMPILER_PATH
    launch_cmd = module_d + LAUNCHER_PATH
    script_d = module_d + STREAMING_PATH + script + "/"
    script_f = script_d + script + ".scala"
    parent_f = module_d + PARENT_SRC_PATH
    output_f = script_d + PACKAGE_NAME + ".jar"

    if not compile_script_and_check(compile_cmd, script_d, script_f, parent_f,
                                    output_f):
        return False
    
    # try:
    #     ssh = create_ssh_client(server, user, passwd)
    # except IOError:
    #     msg = " Local host key file was not found!"
    #     logging.exception(msg)
    #     return False
    # except socket.error:
    #     msg = " Failed to connect to %s!" % server
    #     logging.exception(msg)
    #     return False
    # except paramiko.BadHostKeyException:
    #     msg = " Failed to verify host key of %s!" % server
    #     logging.exception(msg)
    #     return False
    # except paramiko.AuthenticationException:
    #     msg = " Erroneous username and/or password while logging in to %s!" % server
    #     logging.exception(msg)
    #     return False
    # except (paramiko.SSHException, OSError):
    #     msg = " Failed to establish SSH session!"
    #     logging.exception(msg)
    #     return False
    
    # remote_d = target_d + REMOTE_STREAMING_PATH + script
    # if not scp_script_output(ssh, output_f, remote_d):
    #     return False

    try:
        p = subprocess.Popen([launch_cmd, PACKAGE_NAME + "/" + script, "yarn-client", server, 
                              "11112", "10000", server, "11113", server, "11114"], cwd=script_d)
        p.wait(timeout=30)
    except OSError:
        msg = " Could not find script launcher %s!" % launch_cmd
        logging.exception(msg)
        return False
    except subprocess.TimeoutExpired:
        pass
    else:
        if p.returncode == 1:
            msg = " Failed to launch streaming script %s! Reason: %s" % (script_f, p.stderr)
            logging.error(msg)
            return False

    # try:
    #     subprocess.check_call(["rm", "-rf", output_f])
    # except subprocess.CalledProcessError:
    #     msg = " Failed to remove output file %s!" % output_file
    #     logging.exception(msg)

    # _, stdout, _ = ssh.exec_command("cd %s; %s %s/%s yarn-client %s 11112 10000 %s 11113 %s 11114" % (remote_d,
    #                                                                                                   REMOTE_LAUNCHER_PATH,
    #                                                                                                   PACKAGE_NAME,
    #                                                                                                   script,
    #                                                                                                   server, server, server))

    #ssh.close()
    
    return True

class AgentException(Exception):
    pass

cluster = ApiResource(server_host="192.168.109.100", username="admin", password="$:*.tiR3Nnw").get_cluster("cluster")
flume = cluster.get_service("flume")
launch_source("hdfs_agent", flume, "datanode1.traffic.pilot", "misalmi", "$:*.tiR3Nnw", "./")
