package flumestream

import org.apache.spark.streaming._
import org.apache.spark.streaming.dstream._
import org.apache.spark.streaming.flume._
import org.apache.spark.streaming.StreamingContext._
import scala.collection.mutable.Map
import scala.collection.mutable.Queue
import java.text.SimpleDateFormat
import java.util.GregorianCalendar
import java.util.Calendar
import java.util.Date
import java.nio.charset.Charset
import org.apache.spark.rdd.RDD

case class Observation(
  timestamp: Calendar,
  speed: Double
)

class SpeedAvg(
  var sum: Double,
  var count: Int
) extends Serializable {
  
  var lastAdd: Calendar = new GregorianCalendar()

  def add(speed: Double, time: Calendar) {
    sum += speed
    count += 1
    lastAdd = time
  }

  def subtract(speed: Double) {
    sum -= speed
    if (sum < 0) {
      sum = 0
    }
    count -= 1
    if (count < 0) {
      count = 0
    }
  }

  def clear() {
    sum = 0
    count = 0
  }

  def getVal():Double = {
    return sum / count
  }
}

case class AreaData(
  refSpeedAvgs: Map[Int, SpeedAvg],  
  var recentSpeedAvg: SpeedAvg,
  observations: Queue[Observation]
)
  
class TaxidataStreamLauncher(
  params: Array[String]
) extends FlumeStreamLauncher(params) {
   
  override val EXTRA_PARAM_NUM = 4
  override val USAGE_STR = """Arguments: <master> <srcHost> <srcPort> <batchInterval> <jars> 
                            |<destHost1> <destPort1> <destHost2> <destPort2>""".stripMargin

  override def runAnalysis(stream: DStream[SparkFlumeEvent], params: Array[String]) {
    val Array(taxiDataHost, taxiDataPort, fluencyDataHost, fluencyDataPort) = params
    val HOUR_IN_MS = 3600000
    val REF_HOURS = Set(3, 7, 10, 13, 21)
    val NODE_N = 3
    val MIN_SPEED = 0.0
    val MAX_SPEED = 300.0
    val MIN_SAMPLES = 3
    val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    
    val formatTime = (timeStr: String) => {
      val time = new GregorianCalendar()
      format.format(time.getTime())
      time.setTime(format.parse(timeStr))
      time
    }
  
    val extractAnalysisParams = (eventStr: String) => {
      val fields = eventStr.split(",")
      val time = formatTime(fields(2))
      val area = fields(3).toInt
      val speed = fields(6).toDouble
      (area, (time, speed))
    }

    val createNewRefSpeeds = () => {
      val refSpeedAvgs = Map[Int, SpeedAvg]()
      REF_HOURS.foreach(hour => refSpeedAvgs.put(hour, new SpeedAvg(0.0, 0)))
      refSpeedAvgs
    }

    val createAreaData = () => {
      val refSpeedAvgs = createNewRefSpeeds()
      val recentSpeedAvg = new SpeedAvg(0.0, 0)
      val observations = Queue[Observation]()
      AreaData(refSpeedAvgs, recentSpeedAvg, observations)
    }
    
    val updateReferenceData = (areaData: AreaData, time: Calendar, 
      speed: Double) => {
      val hour = time.get(Calendar.HOUR_OF_DAY)
      val ms = time.getTimeInMillis
      if (REF_HOURS.contains(hour)) {
        val refSpeedAvg = areaData.refSpeedAvgs(hour)
        if (ms - refSpeedAvg.lastAdd.getTimeInMillis > HOUR_IN_MS) {
          refSpeedAvg.clear
        }
        refSpeedAvg.add(speed, time)
      }
    }

    val updateAreaData = (areaData: AreaData, time: Calendar, 
      speed: Double) => {
      val observations = areaData.observations
      
      if (speed >= MIN_SPEED && speed <= MAX_SPEED) {
        areaData.recentSpeedAvg.add(speed, time)
        observations += Observation(time, speed)
        var oldestObs = observations.head
      
        while (observations.length > 1000 || 
          time.getTimeInMillis - oldestObs.timestamp.getTimeInMillis > HOUR_IN_MS) {
	  areaData.recentSpeedAvg.subtract(oldestObs.speed)
          observations.dequeue()
	  oldestObs = observations.head
        }
        updateReferenceData(areaData, time, speed)
      }
    }

    val analyzeObservation = (values: Seq[(Calendar, Double)], 
      state: Option[AreaData]) => {
      val areaData = state.getOrElse(createAreaData())
      for ((time, speed) <- values) {
        updateAreaData(areaData, time, speed)
      }
      Some(areaData)
    }

    val checkIfUpdated = (values: Seq[AreaData], state: Option[(Boolean, AreaData)]) => {
      val data = values.last
      if (!state.nonEmpty) {
        Some((true, data))
      } else {
        val (_, prev) = state.get
        val lastObs = prev.observations.last
        if (data.observations.last != lastObs) {
	  Some((true, data))
	} else {
	  Some((false, data))
	}
      } 
    }

    val parseResultStr = (area: Int, time: Calendar, speed: Double, speedAvg: Double, 
      fluency: Double) => {
      val timeStr = format.format(time.getTime())
      val speedStr = speed.toString
      var speedAvgStr = speedAvg.toString
      if (speedAvg.isNaN) {
        speedAvgStr = ""
      }
      var fluencyStr = fluency.toString
      if (fluency.isNaN) {
        fluencyStr = ""
      }
      List(area, timeStr, speedStr, speedAvgStr, fluencyStr).mkString(",")
    }

    val calculateResults = (area: Int, areaData: AreaData) => {
      var speedAvg = 0.0 / 0.0
      var refSpeedAvg = 0.0 / 0.0

      if (areaData.recentSpeedAvg.count >= MIN_SAMPLES) {
        speedAvg = areaData.recentSpeedAvg.getVal
      }
      val lastObs = areaData.observations.last
      var refSpeedSum = 0.0
      var refSpeedCount = 0
      for ((h, refSpeedAvg) <- areaData.refSpeedAvgs) {
        refSpeedSum += refSpeedAvg.sum
        refSpeedCount += refSpeedAvg.count
      }
      if (refSpeedCount > MIN_SAMPLES) {
        refSpeedAvg = refSpeedSum / refSpeedCount
      }
      val fluency = speedAvg / refSpeedAvg
      parseResultStr(area, lastObs.timestamp, lastObs.speed, speedAvg, fluency)
    }

    val pipeRDD = (rdd: RDD[String], destHost: String, destPort: String) => {
      // Come up with a smarter way to send data back to Flume.
      try {
        rdd.pipe("nc -v " + destHost + " " + destPort).collect()
      } catch {
        case e: java.lang.Exception => {}
      }
    }

    val strStream = stream.repartition(NODE_N).map({event =>
      new String(event.event.getBody().array(), Charset.forName("UTF-8"))
    })

    strStream.foreachRDD({
      rdd => pipeRDD(rdd, taxiDataHost, taxiDataPort)
    })

    val proc = strStream.map(extractAnalysisParams)
             .updateStateByKey[AreaData](analyzeObservation)
	     .updateStateByKey[(Boolean, AreaData)](checkIfUpdated)
	     .filter({elem =>
	              val (k, v) = elem
		      val (updated, _) = v
		      updated})
             .map({elem =>
                   val (k, v) = elem
		   val (_, data) = v
	           calculateResults(k, data)})
             .foreachRDD({rdd => pipeRDD(rdd, fluencyDataHost, fluencyDataPort)})
  }
}

object TaxidataStream {
  def main(args: Array[String]) {
    new TaxidataStreamLauncher(args).launch()
  }
}