# -*- coding: utf-8 -*-

import os
import re
import sys
import shutil
import logging
import subprocess

sys.path.append("..")
import settings
from shared.constants import *

DRIVER_MEM_ENV = "SPARK_DRIVER_MEMORY"
EXECUTOR_MEM_ENV = "SPARK_EXECUTOR_MEMORY"
EXECUTOR_NUM_ENV = "SPARK_EXECUTOR_INSTANCES"
EXECUTOR_CORES_ENV = "SPARK_EXECUTOR_CORES"

MODULE_PATH = os.path.realpath(__file__).rsplit("/", 1)[0] + "/"
STREAMING_PATH = MODULE_PATH + "../../user/streaming/"
PARENT_SRC = MODULE_PATH + "../streaming/FlumeStreamLauncher.scala"

SCALA_SUFFIX = ".scala"
JAR_SUFFIX = ".jar"

STREAMING_PCKG = "flumestream"

MASTER_REGEXPS = ("yarn-client$", 
                  "local\[\d\]$", 
                  "spark://[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}:[0-9]{1,5}$")

class StreamAnalysisConf(object):
    """
    A simple configuration structure containing arguments for StreamAnalysis instance. To be
    used as an argument for SourceAdmin.add_source_with_analysis

    @param name: The name of the analysis, which must be the name of the file of the analysis
                 source code file without the .scala suffix.
    @type name: str
    @param master: The type of the Spark master. Can be Spark master URL (spark://host:port), 
                   local[x] where x is the number of cores used, or yarn-client.
    @type master: str
    @param recv_host: The IP address of the node which acts as a stream receiver. In the future,
                      should support multiple receivers.
    @type recv_host: str
    @param recv_port: The port the receiver node listens for incoming data.
    @type recv_port: int
    @param batch_ival: The batch interval, at which the analysis should receive data, in
                       milliseconds.
    @type batch_ival: int
    @params jars: Jar file paths which will be distributed with the analysis.
    @type jars: sequence
    @param args: Extra commandline arguments for the analysis script.
    @type args: sequence
    """
    def __init__(self, name, master, recv_host, recv_port, batch_ival, jars, args):
        self.name = name
        self.master = master
        self.recv_host = recv_host
        self.recv_port = recv_port
        self.batch_ival = batch_ival
        self.jars = jars
        self.args = args

class StreamAnalysis(object):
    """
    Defines a single real time Spark Streaming analysis created by the user as a Scala source 
    code file under user/streaming, as well as methods for compiling and running it. 
    Additionally, manages the process, in which the analysis runs after it is started.

    @param sa_conf: Configuration of the StreamAnalysis.
    @type sa_conf: StreamAnalysisConf
    
    @raise StreamAnalysisException: Raised on object instantation error, such as: 
                                    - Analysis with the given name is not found.
                                    - The master is not recognized.
                                    - If batch_ival <= 0.
    """

    _classpath = []
    """
    @cvar _classpath: The default _classpath which is used by each StreamAnalysis object when 
                      compiling or running analysis
    @type _classpath: list
    """

    _jars = []
    """
    @cvar _jars: The default jar files that are used with each analysis
    @type _jars: list
    """

    _env_ready = False
    """
    @cvar _env_ready: Class variable that indicates if the environment variables have been set 
                      for Spark Streaming.
    @type _env_ready: bool
    """
    
    def __init__(self, sa_conf):
        if os.path.isfile(STREAMING_PATH + sa_conf.name):
            msg = "Analysis script %s was not found under %s!" % (sa_conf.name, STREAMING_PATH)
            raise StreamAnalysisException(msg)
        if all(re.match(pattern, sa_conf.master) is None for pattern in MASTER_REGEXPS):
            msg = "Master %s is not allowed for Spark Streaming!" % sa_conf.master
            raise StreamAnalysisException(msg)
        if sa_conf.batch_ival <= 0:
            msg = "Batch interval for stream data must be at least 1!"
            raise StreamAnalysisException(msg)

        if not self._env_ready:
            self.init_env_variables()
        if len(self._classpath) == 0:
            self.init_classpath()
        if len(self._jars) == 0:
            self.init_jars()

        self.name = re.sub("\.scala$", "", sa_conf.name.rstrip("/"))
        self._run_args = [sa_conf.master, sa_conf.recv_host, str(sa_conf.recv_port), 
                          str(sa_conf.batch_ival)]
        self._jar_args = sa_conf.jars
        self._extra_args = sa_conf.args
        self._compile_d = STREAMING_PATH + self.name + "/"
        
        self._proc = None
        self.compiled = False

    @classmethod
    def init_env_variables(cls):
        """
        Initializes the environment variables for spark driver memory, spark executor memory and
        number of executor cores to be allocated. The variable values are read from the system 
        settings.
        """

        os.environ[DRIVER_MEM_ENV] = settings.spark_driver_mem
        os.environ[EXECUTOR_MEM_ENV] = settings.spark_executor_mem
        os.environ[EXECUTOR_NUM_ENV] = str(settings.spark_num_executors)
        os.environ[EXECUTOR_CORES_ENV] = str(settings.spark_executor_cores)
        cls._env_ready = True

    @classmethod
    def init_classpath(cls):
        """
        Initializes the default classpath for compiling and running Spark Streaming analyses. 
        The variable values are read from the system settings.
        """

        cls._classpath.extend((settings.hadoop_conf,
                               MODULE_PATH + "../streaming/*",
                               MODULE_PATH + "../../user/streaming/lib/*",
                               settings.spark_home + "lib/*",
                               settings.hadoop_home + "*",
                               settings.hadoop_home + "lib/*",
                               settings.mapreduce_home + "*",
                               settings.mapreduce_home + "lib/*",
                               settings.yarn_home + "*",
                               settings.yarn_home + "lib/*",
                               settings.hdfs_home + "*",
                               settings.hdfs_home + "lib/*",
                               settings.spark_home + "assembly/lib/*",
                               settings.spark_home + "examples/lib/*",
                               settings.spark_home + "streaming/lib/*",
                               settings.flume_home + "lib/*"))
        
    @classmethod
    def init_jars(cls):
        """
        Initializes the jar files for running Spark Streaming analyses. The jar file paths are 
        read from the system settings.
        """
        
        cls._jars.extend((STREAMING_PCKG + JAR_SUFFIX, settings.streaming_flume_lib, 
                          settings.avro_lib, settings.avro_ipc_lib, settings.flume_sdk_lib))

    def is_running(self):
        """
        Determines if the analysis is currently running.
        
        @return: True if the analysis is running, otherwise False
        @rtype: bool
        """
        
        return self._proc is not None

    def _create_pckg_dir(self):
        """
        Creates a subdirectory under user/streaming named by the analysis, to which the
        compiled analysis is placed when _compile is called.

        @raise StreamAnalysisException: Raised if the directory creation fails.
        """
        
        try:
            os.mkdir(self._compile_d)
        except OSError as e:
            msg = "Failed to create directory %s! Reason: %s" (self._compile_d, e)
            raise StreamingAnalysisException(msg)

    def _compile(self):
        """
        Compiles the Scala source code file of the stream analysis. A new directory named by 
        the analysis is produced under user/streaming containing the compiled analysis
        package.
        
        @raise StreamAnalysisException: Raised if the compilation of the analysis fails.
        """
        
        logging.info("Compiling stream analysis %s..." % self.name)
        classpath = ":".join(self._classpath + [self._compile_d])
        src = STREAMING_PATH + self.name + SCALA_SUFFIX

        try:
            subprocess.check_call(("scalac", "-cp", classpath, "-d", self._compile_d, 
                                   PARENT_SRC, src))
        except subprocess.CalledProcessError as e:
            msg = ("Compile command of stream analysis %s failed!"
                   " Reason: %s" % (self.name, e))
            raise StreamAnalysisException(msg)
        except OSError:
            msg = "Could not find program scalac in the system!"
            raise StreamAnalysisException(msg)

    def _compress_pckg(self):
        """
        Compresses the compiled package of the stream analysis to a jar file.
        
        @raise StreamAnalysisException: Raised if the compression of the package fails.
        """
        
        logging.info("Compressing package %s of analysis %s..." % (STREAMING_PCKG, 
                                                                   self.name))
        output = self._compile_d + STREAMING_PCKG + JAR_SUFFIX

        try:
            subprocess.check_call(("jar", "cf", output, "-C", self._compile_d, 
                                   STREAMING_PCKG))
        except subprocess.CalledProcessError as e:
            msg = ("Failed to compress package %s for stream analysis %s!"
                   " Reason: %s" % (STREAMING_PCKG, self.name, e))
            raise StreamAnalysisException(msg)
        except OSError:
            msg = "Could not find program jar in the system!"
            raise StreamAnalysisException(msg)

    def _remove_uncomp_pckg(self):
        """
        Removes the uncompiled package of the stream analysis if the package exists.
        """
        logging.info("Removing uncompiled package %s of analysis %s..." % (STREAMING_PCKG, 
                                                                           self.name))
        try:
            shutil.rmtree(self._compile_d + STREAMING_PCKG)
        except OSError as e:
            msg = ("Failed to remove uncompiled package %s of analysis %s" 
                   " Reason: %s" % (STREAMING_PCKG, self.name, e))
            logging.exception(msg)

    def compile(self):
        """
        Compiles the scala source code of the stream analysis under 
        user/streaming/<analysis_name> and compresses it to a jar file. Removes also the
        previous compiled analysis if such exists.

        @raise StreamAnalysisException: Raised if the compile process fails. 
        """
        if not os.path.isdir(self._compile_d):
            self._create_pckg_dir()
        if os.path.isdir(self._compile_d + STREAMING_PCKG):
            self._remove_uncomp_pckg()
        self._compile()
        self._compress_pckg()
        self.compiled = True
            
    def run(self):
        """
        Runs the compiled Spark Streaming analysis with arguments received at object 
        initialization.

        @raise StreamAnalysisException: Raised if the analysis cannot be run or the run
                                        command fails.
        """
        if self.is_running():
            msg = "Stream analysis %s is already running!" % self.name
            raise StreamAnalysisException(msg)
        if not self.compiled:
            msg = "Stream analysis %s hasn't been compiled so it cannot be run!" % self.name
            raise StreamAnalysisException(msg)
        logging.info("Running stream analysis %s..." % self.name)

        classpath = ":".join(self._classpath + [self._compile_d + "*"])
        cmd = ["java", "-cp", classpath, STREAMING_PCKG + "/" + self.name]
        cmd.extend(self._run_args)
        cmd.append(" ".join(self._jars + self._jar_args))
        cmd.extend(str(arg) for arg in self._extra_args)

        try:
            proc = subprocess.Popen(cmd, cwd=self._compile_d)
            proc.communicate(timeout=30)
        except OSError as e:
            msg = "Failed to start process of stream analysis %s! Reason: %s" % (self.name, e)
            raise StreamAnalysisException(msg)
        except subprocess.TimeoutExpired:
            self._proc = proc
        else:
            msg = "Failed to start process of stream analysis %s!" % (self.name)
            raise StreamAnalysisException(msg)
        logging.info("Stream analysis %s running!" % self.name)

    def stop(self):
        """
        Stops the running Spark Streaming analysis process.

        @raise StreamAnalysisException: Raised if the process cannot be stopped or the stop
                                        command fails.
        """
        
        if not self.is_running():
            msg = "Stream analysis %s is not running, so it cannot be stopped!" % self.name
            raise StreamAnalysisException(msg)
        logging.info("Stopping stream analysis %s..." % self.name)

        try:
            self._proc.terminate()
        except OSError:
            msg = ("Cannot stop process of stream analysis %!"
                   " Process does not exist." % self.name)
            self._proc = None
            raise StreamAnalysisException(msg)
        try:
            self._proc.wait(timeout=5)
        except subprocess.TimeoutExpired:
            msg = "Process of stream analysis %s ignored stop command!" % self.name
            raise StreamAnalysisException(msg)
        self._proc = None

        logging.info("Stream analysis %s stopped!" % self.name)

class StreamAnalysisException(Exception):
    """
    An exception which is raised when an error associated with a StreamAnalysis object is 
    encountered.
    """
    pass
