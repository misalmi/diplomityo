# -*- coding: utf-8 -*-

import logging
import importlib
import time
import calendar
import sys
import Queue
import os
from datetime import datetime, timedelta

import hdfs
import ba_admin
sys.path.append("..")
from shared import db_connector, utils
from shared.constants import *
sys.path.append(BATCH_PATH)

LAST_HOUR_OF_DAY = 23
FIRST_HOUR_OF_DAY = 0
SECOND_DAY_OF_MONTH = 2
DECEMBER = 12
REFRESH_FREQ = 5

class MetadataUpdater(object):
    def __init__(self, hdfs_util, spark, data_queue, tbl_queue, ba_queue, upd_queue):
        self._hdfs_util = hdfs_util
        self._spark = spark
        self._tables = {}
        self._analyses = {}
        self._data_stores = {}
        self._tbl_queue = tbl_queue
        self._data_queue = data_queue
        self._ba_queue = ba_queue
        self.upd_queue = upd_queue

    def _read_queue(self, q):
        messages = []
        while True:
            try:
                messages.append(q.get_nowait())
            except Queue.Empty:
                break
        return messages

    def _add_tables(self, tbl, now, future):
        try:
            tbl.cache.add_partition(now.hour)
        except db_connector.ImpalaException as e:
            logging.exception("Failed to add partition %02d to table %s!"
                              " Reason: %s" % (now.hour, tbl.name, e))
        try:    
            tbl.cache.add_partition(future.hour)
        except db_connector.ImpalaException as e:
            logging.exception("Failed to add partition %02d to table %s!"
                              " Reason: %s" % (future.hour, tbl.name, e))
        try:    
            tbl.cache.refresh()
        except db_connector.ImpalaException as e:
            logging.exception("Failed to refresh table %s! Reason: %s" % (tbl.name, e))
        self._tables[tbl.name] = tbl
        logging.info("Added table %s to updater!" % tbl.name)

    def _remove_tables(self, name):
        try:
            del self._tables[name]
        except KeyError:
            logging.exception("Failed to remove table %s from updater:"
                              " no such table!" % name)
            return
        logging.info("Removed table %s from updater!" % name)

    def _update_tables(self, now):
        #logging.info("Performing table updates...")
        for cmd, data in self._read_queue(self._tbl_queue):
            if cmd == UPDATE_CMD:
                self._add_tables(data, now, now + timedelta(minutes=60))
            elif cmd == DEL_CMD:
                self._remove_tables(data)
    
    def _add_data_store(self, name, save_raw):
        if not self._hdfs_util.cache_exists(name):
            try:
                self._hdfs_util.create_cache(name)
            except hdfs.HDFSUtilityException as e:
                logging.exception("Failed to create data store %s cache!"
                                  " Reason: %s" % (name, e))
                return
        if save_raw and not self._hdfs_util.cache_exists(name):
            try:
                self._hdfs_util.create_data_archive(name)
            except hdfs.HDFSUtilityException as e:
                logging.exception("Failed to create data store %s archive!"
                                  " Reason: %s" % (name, e))
                return
        self._data_stores[name] = save_raw
        logging.info("Added data store %s to updater!" % name)
 
    def _remove_data_store(self, name):
        try:
            self._hdfs_util.remove_data_cache(name)
        except hdfs.HDFSUtilityException as e:
            logging.exception("Failed to remove data store %s cache!"
                              " Reason: %s" % (name, e))
        if save_raw:
            try:
                self._hdfs_util.remove_data_archive(name)
            except hdfs.HDFSUtilityException as e:
                logging.exception("Failed to remove data store %s archive!"
                                  " Reason: %s" % (name, e))
        try:
            save_raw = self._data_stores[name]
            del self._data_stores[name]
        except KeyError:
            logging.exception("Failed to remove data store %s from updater:"
                              " no such store!" % name)
        else:
            logging.info("Removed data store %s from updater!" % name)

    def _update_data_stores(self): 
        logging.info("Performing data store updates...")
        for cmd, (name, save_raw) in self._read_queue(self._data_queue):
            if cmd == UPDATE_CMD:
                self._add_data_store(name, save_raw)
            elif cmd == DEL_CMD:
                self._remove_data_store(name)
    
    def _add_analysis(self, ba):
        try:
            cls = ba_admin.BatchAnalysisAdmin.check_analysis_script(ba.name, ba.cls)
        except ba_admin.AnalysisInitException as e:
            logging.exception("Failed to add analysis %s to updater!"
                              " Reason: %s" % (ba.name, e))
            return
        self._analyses[ba.name] = (cls, ba.data_tables, ba.result_table, 
                                   ba.launch_lvl)
        logging.info("Added analysis %s to updater!" % ba.name)

    def _remove_analysis(self, name):
        try:
            del self._analyses[name]
        except KeyError:
            logging.exception("Failed to remove analysis %s from updater:"
                              " no such analysis!" % name)
            return
        logging.info("Removed analysis %s from updater!" % name)

    def _update_analyses(self):
        logging.info("Performing analysis updates...")
        for cmd, data in self._read_queue(self._ba_queue):
            if cmd == UPDATE_CMD:
                self._add_analysis(data)
            elif cmd == DEL_CMD:
                self._remove_analysis(name)
                
    def _add_cache_partitions(self, hour):
        logging.info("Adding cache partitions")
        [utils.run_process(tbl.cache.add_partition, (hour,)) 
         for tbl in self._tables.values()]
    
    def _refresh_caches(self):
        #logging.info("Refreshing tables")
        [utils.run_process(tbl.cache.refresh) for tbl in self._tables.values()]        

    def _archive_partition_and_merge(self, tbl, dtime):
        tbl.archive.insert_into(tbl.cache, dtime)
        tbl.archive.insert_overwrite(dtime)
        tbl.cache.drop_partition(dtime.hour)

    def _archive_partition(self, tbl, dtime):
        tbl.archive.insert_into(tbl.cache, dtime)
        tbl.cache.drop_partition(dtime.hour)

    def _archive_partitions(self, dtime):
        if dtime.hour == LAST_HOUR_OF_DAY:
            f = self._archive_partition_and_merge
        else:
            f = self._archive_partition
            
        logging.info("Archiving table partitions")
        processes = [utils.run_process(f, (tbl, dtime,)) 
                     for tbl in self._tables.values()]
        [p.join() for p in processes]

    def _manipulate_raw_data(self, f, dtime):
        for name, save_raw in self._data_stores.iteritems():
            if not save_raw:
                continue
            logging.info("Raw data: %s" % name)
            try:
                f(name, dtime)
            except hdfs.HDFSUtilityException as e:
                msg = ("Operation for raw data set %s failed!"
                       " Reason: %s" % (name, e))
                logging.exception(msg)

    def _archive_raw_data(self, dtime):
        logging.info("Archiving raw data")
        last_day_of_month = calendar.monthrange(dtime.year, dtime.month)[1]
        
        if dtime.hour != LAST_HOUR_OF_DAY:
            f = self._hdfs_util.perform_hourly_merge
        elif dtime.day != last_day_of_month:
            f = self._hdfs_util.perform_daily_merge
        else:
            f = self._hdfs_util.perform_monthly_merge
        self._manipulate_raw_data(f, dtime)

    def _run_analyses(self, dtime):
        logging.info("Running analyses")
        last_day_of_month = calendar.monthrange(dtime.year, dtime.month)[1]

        for name, (cls, data_tbls, rslt_tbl, launch_lvl) in self._analyses.iteritems():
            if launch_lvl <= LEVEL_DAY and dtime.hour != LAST_HOUR_OF_DAY:
                continue
            elif launch_lvl <= LEVEL_MONTH and dtime.day != last_day_of_month:
                continue
            elif launch_lvl == LEVEL_YEAR and dtime.month != DECEMBER:
                continue
            logging.info("Analysis: %s" % name)
            print datetime.now()
            try:
                inst = cls(data_tbls, rslt_tbl, self._hdfs_util, self._spark)
                inst.run_analysis(dtime)
            except Exception as e:
                logging.exception("Error while running analysis %s: %s" % (name, e))

    def _remove_raw_data(self, dtime):
        logging.info("Removing raw data")
        if dtime.hour != FIRST_HOUR_OF_DAY:
            f = self._hdfs_util.perform_hourly_removal
        elif dtime.day != SECOND_DAY_OF_MONTH:
            f = self._hdfs_util.perform_daily_removal
        else:
            f = self._hdfs_util.perform_monthly_removal
        self._manipulate_raw_data(f, dtime)

    def _archive_and_analyze(self, dtime):
        f = open("output", "w")
        f.write(str(datetime.now()) + "\n")
        self._archive_partitions(dtime)
        f.write(str(datetime.now()) + "\n")
        self._archive_raw_data(dtime)
        f.write(str(datetime.now()) + "\n")
        self._run_analyses(dtime)
        f.write(str(datetime.now()) + "\n")
        self._remove_raw_data(dtime)
        f.write(str(datetime.now()))
        f.close()

    def _update(self, now, past):
        self._update_tables(now)
        utils.run_process(self._refresh_caches)
        time.sleep(REFRESH_FREQ)

        now = datetime.now() # Add timedelta to change
        if now.hour != past.hour:
            future = now + timedelta(minutes=60)
            self._update_data_stores()
            self._update_analyses()
            utils.run_process(self._add_cache_partitions, (future.hour,))
            utils.run_process(self._archive_and_analyze, (past,))
            past = now
        return now, past
    
    def run(self):
        cmd = START_CMD
        past = datetime.now() # Add timedelta to change
        now = past

        while True:
            messages = self._read_queue(self.upd_queue)
            if len(messages) > 0:
                cmd = messages[-1]
                
            if cmd == START_CMD:
                now, past = self._update(now, past)
            else:
                past = datetime.now() # Add timedelta to change
                now = past
                time.sleep(5)

class UpdaterException(Exception):
    pass

