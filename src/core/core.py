# -*- coding: utf-8 -*-

import sys
import logging
from multiprocessing import Queue
#from pyspark import SparkContext
from cm_api.api_client import ApiResource, ApiException

sys.path.append("..")
import settings
import flume_agent
import stream_analysis
import hdfs
import storage_admin
import source_admin
import table_admin
import ba_admin
import iface_admin
import metadata_updater
from shared import tables, utils, db_connector, conf_admin
from shared.constants import *

class TableAdmin(object):
    def __init__(self, tbl_admin, ba_admin, iface_admin):
        self._tbl_admin = tbl_admin
        self._ba_admin = ba_admin
        self._iface_admin = iface_admin

    def add_data_table_pair(self, name, cols, part_lvl):
        self._tbl_admin.add_data_table_pair(name, cols, part_lvl)

    def add_result_table(self, name, cols, part_lvl):
        self._tbl_admin.add_result_table(name, cols, part_lvl)
            
    def remove_data_table_pair(self, name):
        tbls = self._ba_admin.list_analysis_data_tables()
        if name in tbls:
            msg = ("Table pair %s is used by one or more batch analyses!"
                   " Remove the analyses first!" % name)
            raise table_admin.TableAdminException(msg)
        tbls, _ = self._iface_admin.list_all_tables_in_use()
        if name in tbls:
            msg = ("Table pair %s is used by one or more interfaces!"
                   " Remove the interfaces first!" % name)
            raise table_admin.TableAdminException(msg)
        self._tbl_admin.remove_data_table_pair(name)

    def remove_result_table(self, name):
        tbls = self._ba_admin.list_analysis_result_tables()
        if name in tbls:
            msg = ("Result table %s is used by one or more batch analyses!"
                   " Remove the analyses first!" % name)
            raise table_admin.TableAdminException(msg)
        _, tbls = self._iface_admin.list_all_tables_in_use()
        if name in tbls:
            msg = ("Result table %s is used by one or more interfaces!"
                   " Remove the interfaces first!" % name)
            raise table_admin.TableAdminException(msg)
        self._tbl_admin.remove_result_table(name)

    def has_data_table_pair(self, name):
        return self._tbl_admin.has_data_table_pair()

    def has_result_table(self, name):
        return self._tbl_admin.has_result_table()
        
    def list_data_table_pairs(self):
        return self._tbl_admin.list_data_table_pairs()
    
    def list_result_tables(self):
        return self._tbl_admin.list_result_tables()
        
    def get_table_pair_configuration(self):
        return self._tbl_admin.get_table_pair_configuration()

    def get_result_table_configuration(self):
        return self._tbl_admin.get_result_table_configuration()

class StorageAdmin(object):
    def __init__(self, stor_admin, src_admin, tbl_admin):
        self._stor_admin = stor_admin
        self._src_admin = src_admin
        self._tbl_admin = tbl_admin

    def remove_storage(self, name):
        if self._tbl_admin.has_data_table_pair(name):
            msg = ("Storage %s is used by a table pair!"
                   "Remove table pair first!" % name)
            raise storage_admin.StorageAdminException(msg)
        if name in self._src_admin.get_all_storages():
            msg = ("Storage %s is used by a source!"
                   "Remove the source first!" % name)
            raise source_admin.StorageAdminException(msg)
        
        self._stor_admin.remove_storage(name)

class SystemCore(object):
    def __init__(self, spark, webservers):
        logging.basicConfig(level=settings.logging_level,
                            format="%(levelname)s:%(filename)s: %(message)s")
        try:
            api = ApiResource(server_host=settings.manager_host, 
                              username=settings.manager_user, 
                              password=settings.manager_passwd)
        except ApiException as e:
            msg = "Failed to connect to Cloudera Manager API! Reason: %s" % e
            raise SystemInitException(msg)
        try:
            cluster = api.get_cluster(settings.cluster_name)
        except ApiException as e:
            msg = "Failed to get cluster resources from Cloudera Manager! Reason %s" % e
            raise SystemInitException(msg)
        try:
            flume = cluster.get_service(settings.flume_name)
        except ApiException as e:
            msg = "Failed to get Flume service from Cloudera Manager! Reason: %s" % e
            raise SystemInitException(msg)
        
        conn = db_connector.DatabaseConnector(settings.impala_host, 
                                              settings.impala_port)

        flume_agent.FlumeAgent.flume = flume
        stream_analysis.StreamAnalysis.init_env_variables()
        stream_analysis.StreamAnalysis.init_classpath()
        
        
        stor_q = Queue()
        tbl_q = Queue()
        ba_q = Queue()
        upd_q = Queue()
        
        stor_admin = storage_admin.StorageAdmin(STORAGE_CONF_F, stor_q)
        self.src_admin = source_admin.SourceAdmin(SOURCE_CONF_F, stor_admin)
        tbl_admin = table_admin.TableAdmin(TABLE_CONF_F, tbl_q, conn, stor_admin)
        self.stor_admin = StorageAdmin(stor_admin, self.src_admin, tbl_admin)
        self.ba_admin = ba_admin.BatchAnalysisAdmin(BATCH_CONF_F, ba_q, tbl_admin)
        self.iface_admin = iface_admin.InterfaceAdmin(webservers,tbl_admin)
        self.tbl_admin = TableAdmin(tbl_admin, self.ba_admin, self.iface_admin)
        
        hdfs_util = hdfs.HDFSUtility(settings.hdfs_path + CACHE, settings.hdfs_path + ARCHIVE)
        self._updater = metadata_updater.MetadataUpdater(hdfs_util, spark, stor_q, tbl_q, ba_q,
                                                         upd_q)
        self._upd_proc = None

    def start_updater(self):
        if self._upd_proc is None:
            self._upd_proc = utils.run_process(self._updater.run)
        else:
            self._updater.upd_q.put(START_CMD)

    def stop_updater(self):
        if self._upd_proc is None:
            msg = "Updater must be started at least once before stopping it!"
            raise metadata_updater.UpdaterException(msg)
        self._updater.upd_q.put(STOP_CMD)

class SystemInitException(Exception):
    pass
                                            
if __name__ == "__main__":    
    # agent = {"name" : "hdfs_agent",
    #          "conf" : "test_agent",
    #          "host" : "datanode1.traffic.pilot",
    #          "source" : {"bind" : "127.0.0.1",
    #                      "port" : "11111"},
    #          "rslt_sinks" : [{"name" : "trafficfluency"}],
    #          "raw_sink" : {"name" : "taxidata"}}
    # streamer = {"name" : "TaxidataStream",
    #             "client" : "yarn-client",
    #             "target" : "datanode1.traffic.pilot",
    #             "port" : "11112",
    #             "batch_interval" : "10000",
    #             "args" : ["datanode1.traffic.pilot", "11113", "datanode1.traffic.pilot", "11114"],
    #             "rslt_ports" : ["11112"]}
    # outputs = [{"name" : "trafficfluency",
    #              "save_raw" : False}]
    # raw_output = {"name" : "taxidata",
    #               "save_raw" : False}
    # api = SystemAPI()
    # src_admin = api.src_admin
    # src_admin.add_streaming_source("taxidata", api.flume, outputs, agent, streamer, raw_output)
    # src_admin.run_sources()
    pass

