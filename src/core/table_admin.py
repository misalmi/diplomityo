# -*- coding: utf-8 -*-

import sys
import re
import hdfs
import logging
import impala.error
from collections import namedtuple

sys.path.append("..")
from shared import conf_admin, tables, utils, db_connector
from shared.constants import *

PARTITION_LEVEL = "part_lvl"
LOCATION_STR = "Location"
TBL_TYPE_STR = "Table Type"
EXTERNAL_STR = "EXTERNAL_TABLE"
TYPE_SUFFIX = "_TYPE"
INT_TYPE = "INT_TYPE"
STRING_TYPE = "STRING_TYPE"

class TableAdmin(conf_admin.ConfAdmin):
    def __init__(self, conf_f, queue, conn, stor_admin):
        super(TableAdmin, self).__init__(conf_f)
        self._queue = queue
        self._conn = conn
        self._stor_admin = stor_admin
        self._data_tbls = []
        self._rslt_tbls = []
        self._parse_tables()
        [self._queue.put((UPDATE_CMD, tbl)) for tbl in self._data_tbls]
        
    @staticmethod
    def parse_columns(cols):
        try:
            return [tables.Column(col[NAME], col[TYPE]) for col in cols]
        except KeyError as e:
            msg = COL_PARSE_FAILED % (col, e)
            raise ColumnConfKeyError(msg)
        except tables.SQLCharException as e:
            raise conf_admin.ConfException(str(e))

    @staticmethod
    def compose_columns(cols):
        return [{NAME : cname, TYPE : ctype} for cname, ctype in cols]

    @staticmethod
    def check_table_partition(name, part_lvl, part_type, tbl_parts, part_name):
        part_col = tbl_parts[part_lvl - 1]
        col_name, col_type = part_col
        if col_name != part_name or col_type != part_type:
            msg = "Invalid partition in database table %s" % name
            raise DatabaseTableException(msg)

    @staticmethod
    def check_table_partitions(name, part_lvl, tbl_parts):
        TableAdmin.check_table_partition(name, LEVEL_YEAR, INT_TYPE,
                                         tbl_parts, YEAR_PART)
        if part_lvl >= LEVEL_MONTH:
            TableAdmin.check_table_partition(name, LEVEL_MONTH, INT_TYPE,
                                             tbl_parts, MONTH_PART)
        if part_lvl >= LEVEL_DAY:
            TableAdmin.check_table_partition(name, LEVEL_DAY, INT_TYPE,
                                             tbl_parts, DAY_PART)
        if part_lvl == LEVEL_HOUR:
            TableAdmin.check_table_partition(name, LEVEL_HOUR, INT_TYPE,
                                             tbl_parts, HOUR_PART)

    @staticmethod
    def check_table_pair_partitions(name, part_lvl, cache_parts, archive_parts):
        TableAdmin.check_table_partition(name + CACHE, LEVEL_CACHE, STRING_TYPE,
                                         cache_parts, HOUR_PART)
        TableAdmin.check_table_partitions(name + ARCHIVE, part_lvl,
                                          archive_parts)

    @staticmethod
    def check_table_pair_columns(name, conf_cols, part_lvl, cache_cols, 
                                 archive_cols):
        conf_col_len = len(conf_cols)
        msg = "Columns of table pair %s mismatch with tables in database!" % name

        if (len(cache_cols) != conf_col_len + LEVEL_CACHE or 
            len(archive_cols) != conf_col_len + part_lvl):
            raise DatabaseTableException(msg)
        
        cache_parts = cache_cols[:LEVEL_CACHE]
        archive_parts = archive_cols[:part_lvl]
        TableAdmin.check_table_pair_partitions(name, part_lvl, 
                                               cache_parts, archive_parts)

        cache_cols = cache_cols[LEVEL_CACHE:]
        archive_cols = archive_cols[part_lvl:]
        if cache_cols != archive_cols:
            raise DatabaseTableException(msg)
        for i, col in enumerate(conf_cols):
            cname, ctype = cache_cols[i]
            ctype = re.sub("%s$" % TYPE_SUFFIX, "", ctype).lower()
            if col.name != cname or col.type != ctype:
                raise DatabaseTableException(msg)

    @staticmethod
    def check_result_table_columns(name, conf_cols, part_lvl, db_cols):
        msg = "Columns of result table %s mismatch with table in database!" % name

        if len(db_cols) != len(conf_cols) + part_lvl:
            raise DatabaseTableException(msg)
            
        db_parts = db_cols[:part_lvl]
        TableAdmin.check_table_partitions(name, part_lvl, db_parts)

        db_cols = db_cols[part_lvl:]
        for i, col in enumerate(conf_cols):
            cname, ctype = db_cols[i]
            ctype = re.sub("%s$" % TYPE_SUFFIX, "", ctype).lower()
            if col.name != cname or col.type != ctype:
                raise DatabaseTableException(msg)

    @staticmethod
    def check_table_pair_cache_location(name, conn):
        cache_name = name + CACHE
        try:
            desc = conn.describe_formatted(cache_name)
        except db_connector.ImpalaException as e:
            msg = ("Could not read cache table %s from database!" 
                   " Reason: %s" % (cache_name, e))
            raise DatabaseTableException(msg)
        
        path = HDFS_INCOMING_PATH + name
        path_found = (dname.strip(": ") == LOCATION_STR and dtype == path
                      for dname, dtype, _ in desc)
        type_found = (dname.strip(": ") == TBL_TYPE_STR and dtype == EXTERNAL_STR
                      for dname, dtype, _ in desc)
        if not any(path_found) or any(type_found):
            msg = "Cache %s is not an ext. table pointing to %s!" % (cache_name, 
                                                                     path)
            raise DatabaseTableException(msg)

    @staticmethod
    def get_table_pair_schema(name, conn):
        try:
            cache_cols = conn.get_table_schema(name + CACHE)
            archive_cols = conn.get_table_schema(name + ARCHIVE)
        except db_connector.ImpalaException:
            msg = "Could not read data table pair %s from database!" % name
            raise DatabaseTableException(msg)
        return cache_cols, archive_cols

    @staticmethod
    def get_result_table_schema(name, conn):
        try:
            return conn.get_table_schema(name)
        except db_connector.ImpalaException:
            msg = "Could not find result table %s from database!" % name
            raise DatabaseTableException(msg)

    @staticmethod
    def check_database_table_pair(name, cols, part_lvl, conn):
        TableAdmin.check_table_pair_cache_location(name, conn)
        cache_cols, archive_cols = TableAdmin.get_table_pair_schema(name, conn)
        
        TableAdmin.check_table_pair_columns(name, cols, part_lvl, 
                                            cache_cols, archive_cols)

    @staticmethod
    def check_database_result_table(name, cols, part_lvl, conn):
        db_cols = TableAdmin.get_result_table_schema(name, conn)
        TableAdmin.check_result_table_columns(name, cols, part_lvl, db_cols)

    @staticmethod
    def parse_table_conf(conf):
        name = conf[NAME]
        cols = conf[COLUMNS]
        part_lvl = conf[PARTITION_LEVEL]
        try:
            cols = TableAdmin.parse_columns(cols)
        except (ColumnConfKeyError, tables.SQLTypeException) as e:
            raise conf_admin.ConfException(str(e))
        return name, cols, part_lvl

    def _check_table_pair_name(self, name):
        if self.has_data_table_pair(name):
            msg = "Table pair object %s already exists!" % name
            raise TableAdminException(msg)
        if not self._stor_admin.has_storage(name):
            msg = "No storage corresponds to table name %s" % name
            raise TableAdminException(msg)

    def _check_result_table_name(self, name):
        if self.has_result_table(name):
            msg = "Result table object %s already exists!" % name
            raise TableAdminException(msg)

    def _check_table_pair_conf(self, name, cols, part_lvl):
        try:
            self._check_table_pair_name(name)
        except TableAdminException as e:
            raise conf_admin.ConfException(str(e))

        TableAdmin.check_database_table_pair(name, cols, part_lvl, self._conn)

    def _check_result_table_conf(self, name, cols, part_lvl):
        try:
            self._check_result_table_name(name)
        except TableAdminException as e:
            raise conf_admin.ConfException(str(e))

        TableAdmin.check_database_result_table(name, cols, part_lvl, self._conn)

    def _parse_data_table_pairs(self, confs):
        logging.info("Parsing data table pairs...")
        tbl_pairs = set()

        for conf in confs:
            try:
                name, cols, part_lvl = TableAdmin.parse_table_conf(conf)
            except KeyError as e:
                msg = DTBL_PARSE_FAILED % (conf, e)
                logging.exception(msg)
                continue
            except conf_admin.ConfException:
                msg = "Failed to parse data table pair from %s" % conf
                logging.exception(msg)
                continue

            try:
                self._check_table_pair_conf(name, cols, part_lvl)
            except conf_admin.ConfException:
                msg = "Failed to parse data table pair %s" % name
                logging.exception(msg)
                continue
            except DatabaseTableException:
                msg = ("Table pair %s corresponding to conf" 
                       " not found in database!" % name)
                logging.exception(msg)
                continue

            try:
                tbl = tables.DataTablePair(name, cols, part_lvl, self._conn)
            except (tables.SQLCharException, tables.PartitionLevelException) as e:
                logging.exception(str(e))

            self._data_tbls.append(tbl)
            logging.info("Data table pair %s parsed!" % name)

    def _parse_result_tables(self, confs):
        logging.info("Parsing result tables...")

        for conf in confs:
            try:
                name, cols, part_lvl = TableAdmin.parse_table_conf(conf)
            except KeyError as e:
                msg = RTBL_PARSE_FAILED % (conf, e)
                logging.exception(msg)
                continue
            except conf_admin.ConfException:
                msg = "Failed to parse result table from %s" % conf
                logging.exception(msg)
                continue

            try:
                self._check_result_table_conf(name, cols, part_lvl)
            except conf_admin.ConfException:
                msg = "Failed to parse result table %s" % name
                logging.exception(msg)
                continue
            except DatabaseTableException:
                msg = ("Result table %s corresponding to conf"
                       " not found in database!" % name)
                logging.exception(msg)
                continue
    
            try:
                tbl = tables.ResultTable(name, cols, part_lvl, self._conn)
            except (tables.SQLCharException, tables.PartitionLevelException) as e:
                logging.exception(str(e))
                continue

            self._rslt_tbls.append(tbl)
            logging.info("Result table %s parsed!" % name) 

    def _parse_tables(self):
        dtbl_confs = self._read_from_conf(DATA_TABLES)
        rtbl_confs = self._read_from_conf(RESULT_TABLES) 
        
        self._parse_data_table_pairs(dtbl_confs)
        self._parse_result_tables(rtbl_confs)

    def _create_data_table_pair(self, tbl_pair):
        try:
            cache_name = tbl_pair.cache.name
            archive_name = tbl_pair.archive.name
            msg = "Table %s already exists!"
            if self._conn.has_table(cache_name):
                raise DatabaseTableException(msg % cache_name)
            elif self._conn.has_table(archive_name):
                raise DatabaseTableException(msg % archive_name)
        except db_connector.ImpalaException as e:
            msg = "Unable to read tables from database! Reason: %s" % e
            raise DatabaseTableException(msg)
        try:
            tbl_pair.create()
        except db_connector.ImpalaException as e:
            msg = "Failed to create data table pair! Reason: %s" % e
            raise DatabaseTableException(msg)

    def _create_result_table(self, tbl):
        try:
            name = tbl.name
            msg = "Table %s already exists!"
            if self._conn.has_table(name):
                raise DatabaseTableException(msg % name)
        except db_connector.ImpalaException as e:
            msg = "Unable to read tables from database! Reason: %s" % e
            raise DatabaseTableException(msg)
        try:
            tbl.create()
        except db_connector.ImpalaException as e:
            msg = "Failed to create data table pair! Reason: %s" % e
            raise DatabaseTableException(msg)
        
    def get_data_table_pair(self, name):
        for tbl in self._data_tbls:
            if tbl.name == name:
                return tbl
        msg = "Data table pair by name %s was not found" % name
        raise TableAdminException(msg)

    def get_result_table(self, name):
        for tbl in self._rslt_tbls:
            if tbl.name == name:
                return tbl
        msg = "Result table by name %s was not found" % name
        raise TableAdminException(msg)

    def has_data_table_pair(self, name):
        try:
            self.get_data_table_pair(name)
            return True
        except TableAdminException:
            return False

    def has_result_table(self, name):
        try:
            self.get_result_table(name)
            return True
        except TableAdminException:
            return False

    def add_data_table_pair(self, name, cols, part_lvl):
        self._check_table_pair_name(name)
        col_conf = TableAdmin.compose_columns(cols)
        tbl_cols = TableAdmin.parse_columns(col_conf)
        tbl = tables.DataTablePair(name, tbl_cols, part_lvl, self._conn)

        tbl_conf = {NAME : name, 
                    COLUMNS : col_conf, 
                    PARTITION_LEVEL : part_lvl}
        self._append_to_conf(DATA_TABLES, tbl_conf)
        logging.info("Table pair %s added to configuration!" % name)
        
        self._create_data_table_pair(tbl)
        logging.info("Table pair %s created to database!" % name)

        self._queue.put((UPDATE_CMD, tbl))
        self._data_tbls.append(tbl)
        logging.info("Table pair %s added to admin!" % name)     
                           
    def add_result_table(self, name, cols, part_lvl):
        self._check_result_table_name(name)
        col_conf = TableAdmin.compose_columns(cols)
        tbl_cols = TableAdmin.parse_columns(col_conf)
        
        tbl = tables.ResultTable(name, tbl_cols, part_lvl, self._conn)
        tbl_conf = {NAME : name, 
                    COLUMNS : col_conf, 
                    PARTITION_LEVEL : part_lvl}
        self._append_to_conf(RESULT_TABLES, tbl_conf)
        logging.info("Result table %s added to configuration!" % name)
        
        self._create_result_table(tbl)
        logging.info("Result table %s created to database!" % name)
        
        self._queue.put((UPDATE_CMD, tbl))
        self._rslt_tbls.append(tbl)
        logging.info("Result table %s added to admin!" % name)     

    def remove_data_table_pair(self, name):
        try:
            tbl = self.get_data_table_pair(name)
        except TableAdminException:
            msg = "Table pair %s does not exist in admin!" % name
            logging.exception(msg)
        else:
            self._data_tbls.remove(tbl)
            logging.info("Table pair %s removed from admin!" % name)  
            self._queue.put((DEL_CMD, name))
        
        try:
            self._remove_from_conf(DATA_TABLES, NAME, name)
            logging.info("Table pair %s removed from configuration!" % name)
        except conf_admin.ConfException:
            msg = "Failed to remove table pair %s from configuration!" % name
            logging.exception(msg)
        
        cache_name = name + CACHE
        try:
            self._conn.drop_table(cache_name)
            logging.info("Cache table %s removed from database!" % cache_name)
        except db_connector.ImpalaException as e:
            msg = ("Failed to remove cache %s from database"
                   " Reason %s" % (cache_name, e))
            logging.exception(msg)

        archive_name = name + ARCHIVE
        try:
            self._conn.drop_table(name + ARCHIVE)
            logging.info("Archive table %s removed from database!" % archive_name)
        except db_connector.ImpalaException as e:
            msg = ("Failed to remove archive %s from database"
                   " Reason %s" % (archive_name, e))
            logging.exception(msg)

    def remove_result_table(self, name):
        try:
            tbl = self.get_result_table(name)
        except TableAdminException:
            msg = "Result table %s does not exist in admin!" % name
            logging.exception(msg)
        else:
            self._rslt_tbls.remove(tbl)
            logging.info("Result table %s removed from admin!" % name)
        
        try:
            self._remove_from_conf(RESULT_TABLES, NAME, name)
            logging.info("Result table %s removed from configuration!" % name)
        except conf_admin.ConfException:
            msg = "Failed to remove result_table %s from configuration!" % name
            logging.exception(msg)
        
        try:
            self._conn.drop_table(name)
            logging.info("Result table %s removed from configuration!" % name)
        except db_connector.ImpalaException as e:
            msg = ("Failed to remove table %s from database"
                   " Reason %s" % (name, e))
            logging.exception(msg)

    def list_data_table_pairs(self):
        return [tbl.name for tbl in self._data_tbls]
    
    def list_result_tables(self):
        return [tbl.name for tbl in self._rslt_tbls]

    def get_table_pair_configuration(self):
        return self._read_from_conf(DATA_TABLES)

    def get_result_table_configuration(self):
        return self._read_from_conf(RESULT_TABLES)

class DatabaseTableException(Exception):
    pass

class ColumnConfKeyError(KeyError):
    pass

class TableAdminException(Exception):
    pass
