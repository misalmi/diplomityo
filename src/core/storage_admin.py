# -*- coding: utf-8 -*-

import sys
import logging

sys.path.append("..")
from shared import conf_admin2
from shared.constants import *

ARCHIVE_RAW = "archive_raw"

class StorageConf(object):
    def __init__(self, name, archive_raw):
        self.name = name
        self.archive_raw = archive_raw

class StorageAdmin(conf_admin2.ConfAdmin):
    def __init__(self, conf_f, queue):
        super(StorageAdmin, self).__init__(conf_f)
        self._queue = queue
        self._storages = {}
        self._parse_storages()
        [self._queue.put((UPDATE_CMD, (stor.name, stor.archive_raw)))
         for stor in self._storages.values()]
        
    def _parse_storages(self):
        logging.info("Parsing data storages...")
        for name, archive_raw in self._read_from_conf(STORAGES).iteritems():
            try:
                self._check_name(name)
            except StorageAdminException as e:
                logging.exception(str(e))
                continue
            self._storages[name] = StorageConf(name, archive_raw)
            logging.info("Data storage %s parsed!" % name)

    def _check_name(self, name):
        if self.has_storage(name):
            msg = "Storage %s already exists!" % name
            raise StorageAdminException(msg)
        if not name.isalpha() or not name.islower():
            msg = "Storage name must be alphabetic and lowercase!"
            raise StorageAdminException(msg)

    def has_storage(self, name):
        return name in self._storages

    def add_storage(self, storage):
        name = storage.name
        archive_raw = storage.archive_raw
        self._check_name(name)
        self._update_conf(STORAGES, name, archive_raw)
        
        self._storages[name] = storage
        self._queue.put((UPDATE_CMD, (name, archive_raw)))

    def remove_storage(self, name):
        if self.has_storage(name):
            del self._storages[name]

        self._queue.put((DEL_CMD, name))

        try:    
            self._remove_from_conf(STORAGES, name)
        except conf_admin2.ConfException as e:
            logging.exception(str(e))
        else:
            logging.info("Storage %s removed from configuration!" % name)

class StorageAdminException(Exception):
    pass
