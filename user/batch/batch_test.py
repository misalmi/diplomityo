# -*- coding: utf-8 -*-

import math
import operator
from datetime import datetime

from batch_analysis import AnalysisScript

TRAFFICFLUENCY = "trafficfluency"
WEATHERDATA = "weatherdata"
FLUENCY = "fluency"
AREA = "area"
TIME = "time"
MEASUREDTIME = "measurementtime"
STATION = "stationid"
ROADCOND = "roadsurfaceconditions"

class CorrelationScript(AnalysisScript):
    def launch_task(self, dtime):
        fluency_ind = self.data_tables[TRAFFICFLUENCY].get_column_indexes()
        weather_ind = self.data_tables[WEATHERDATA].get_column_indexes()
        fluency_path = self.get_month_data_path(TRAFFICFLUENCY, dtime)
        weather_path = self.get_month_data_path(WEATHERDATA, dtime)

        area_station_d = dict((str(loc_id), loc_id + 1) for loc_id in range(1000, 10000))

        def fluency_mapper(line):
            line = line.split(",")
            
            try:
                area = line[fluency_ind[AREA]]
                t = line[fluency_ind[TIME]]
                fluency = line[fluency_ind[FLUENCY]]
            except IndexError:
                return None, None
            
            try:
                station = area_station_d[area]
            except KeyError:
                return None, None

            try:
                t = datetime.strptime(t, "%Y-%m-%d %H:%M:%S")
                year, month, day, hour = t.year, t.month, t.day, t.hour
            except ValueError:
                return None, None

            try:
                fluency = float(fluency)
            except ValueError:
                return None, None
            if fluency < 0 or fluency > 100:
                return None, None

            return (station, year, month, day, hour), (1, fluency)

        def fluency_reducer(fluency1, fluency2):
            return tuple(map(operator.add, fluency1, fluency2))

        def roadcond_mapper(line):
            line = line.split(",")

            try:
                station = line[weather_ind[STATION]]
                t = line[weather_ind[MEASUREDTIME]]
                roadcond = line[weather_ind[ROADCOND]]
            except IndexError:
                return None, None
                
            try:
                t = datetime.strptime(t, "%Y-%m-%d %H:%M:%S")
                year, month, day, hour = t.year, t.month, t.day, t.hour
            except ValueError:
                return None, None

            try:
                station = int(station)
                roadcond = float(roadcond)
            except ValueError:
                return None, None
            if station not in area_station_d.values():
                return None, None
            if roadcond < 0 or roadcond > 100:
                return None, None
            
            return (station, year, month, day, hour), (1, roadcond)

        def roadcond_reducer(roadcond1, roadcond2):
            return tuple(map(operator.add, roadcond1, roadcond2))
            
        def datapair_mapper(datapair):
            fluency_it, roadcond_it = datapair[1]
            if len(fluency_it) == 0 or len(roadcond_it) == 0:
                return None, None
            
            fluency_avg, roadcond_avg = sum(fluency_it), sum(roadcond_it)
            multiplication = fluency_avg * roadcond_avg
            fluency_power = fluency_avg ** 2
            roadcond_power = roadcond_avg ** 2
            val = fluency_avg, roadcond_avg, multiplication, fluency_power, roadcond_power

            return val

        def datapair_reducer(vals1, vals2):
            return tuple(map(operator.add, vals1, vals2))
            
        fluencies = (self.spark.textFile(fluency_path)
                     .map(fluency_mapper)
                     .filter(lambda sample: sample != (None, None)))
        fluency_avgs = (fluencies.reduceByKey(fluency_reducer)
                        .filter(lambda flu: flu[1][0] != 0)
                        .map(lambda flu: (flu[0], flu[1][1] / flu[1][0])))

        roadconds = (self.spark.textFile(weather_path)
                     .map(roadcond_mapper)
                     .filter(lambda sample: sample != (None, None)))
        roadcond_avgs = (roadconds.reduceByKey(fluency_reducer)
                         .filter(lambda rc: rc[1][0] != 0)
                         .map(lambda rc: (rc[0], rc[1][1] / rc[1][0])))

        datapairs = (fluency_avgs.cogroup(roadcond_avgs)
                     .map(datapair_mapper)
                     .filter(lambda sample: sample != (None, None)))

        sum_f, sum_r, sum_fr, sum_f2, sum_r2 = datapairs.reduce(datapair_reducer)
        n = datapairs.count()
        
        result = ((n * sum_fr - sum_f * sum_r) / 
                  (math.sqrt(n * sum_f2 - sum_f ** 2) * math.sqrt(n * sum_r2 - sum_r ** 2)))

        return [(dtime.month, result)]
