
package com.gofore.sujuvuus.schemas;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.gofore.sujuvuus.schemas package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.gofore.sujuvuus.schemas
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RoadWeatherResponse }
     * 
     */
    public RoadWeatherResponse createRoadWeatherResponse() {
        return new RoadWeatherResponse();
    }

    /**
     * Create an instance of {@link ObstimeType }
     * 
     */
    public ObstimeType createObstimeType() {
        return new ObstimeType();
    }

    /**
     * Create an instance of {@link RoadWeatherResponse.Roadweatherdata }
     * 
     */
    public RoadWeatherResponse.Roadweatherdata createRoadWeatherResponseRoadweatherdata() {
        return new RoadWeatherResponse.Roadweatherdata();
    }

    /**
     * Create an instance of {@link RoadWeather }
     * 
     */
    public RoadWeather createRoadWeather() {
        return new RoadWeather();
    }

    /**
     * Create an instance of {@link RoadWeatherType }
     * 
     */
    public RoadWeatherType createRoadWeatherType() {
        return new RoadWeatherType();
    }

}
