# -*- coding: utf-8 -*-

import sys
import os
import logging
import importlib
from collections import namedtuple

sys.path.append("..")
import data_source
import flume_agent
import stream_analysis
import storage_admin
from shared import conf_admin2
from shared.constants import *

SOURCE = "source"
SOURCES = "sources"
AGENTS = "agents"
ANALYSES = "analyses"
CONF = "conf"
MASTER = "master"
BATCH_IVAL = "batch_interval"
JARS = "jars"
ARGS = "args"

BATCH_IVAL_DEF = 100
BATCH_SIZE_DEF = 100
ROLL_SIZE_DEF = 1000
ROLL_COUNT_DEF = 0
ROLL_IVAL_DEF = 0
IDLE_TIMEOUT_DEF = 0

class SourceAdmin(conf_admin2.ConfAdmin):
    """
    Manages all DataSource objects in the system by providing methods for manipulating them.
    Parses the sources from a configuration file and validates each attempt to add or remove
    them.

    @param conf_f: The name of the configuration file from which the sources are read.
    @type conf_f: str
    @param stor_adm: The storage admin instance of the system.
    @type stor_adm: StorageAdmin

    @raise ConfException: Raised if the configuration file cannot be read or it is wholly 
                          corrupted.
    """
    def __init__(self, conf_f, stor_adm):
        super(SourceAdmin, self).__init__(conf_f)
        self._srcs = {}
        self._stor_adm = stor_adm
        self._parse_sources()

    @staticmethod
    def create_storage_conf(name, archive_raw):
        """
        A static method for creating StorageConf objects.
        
        @param name: The name of the HDFS storage.
        @type name: str
        @param archive_raw: Determines if the raw data written to the storage should be 
                            archived.
        @type archive_raw: bool
        
        @return: The created storage configuration object.
        @rtype: StorageConf
        """
        
        return storage_admin.StorageConf(name, archive_raw)

    @staticmethod
    def create_agent_conf(name, conf, host):
        """
        A static method for creating AgentConf objects.
        
        @param name: The name of the Flume agent.
        @type name: str
        @param conf: The name of the configuration used by the agent, that is, the
                     configuration file name without file suffix. Configuration files are 
                     located under user/flume. 
        @type conf: str
        @param host: The address of the host where the Flume agent should be started by the
                     Cloudera Manager.
        @type host: str
        
        @return: The created agent configuration object.
        @rtype: AgentConf
        """

        return flume_agent.AgentConf(name, conf, host)

    @staticmethod
    def create_agent_source_conf(name, bind, port):
        """
        A static method for creating AgentSourceConf objects.
        
        @param name: The name of the Flume agent source.
        @type name: str
        @param conf: The address to which the agent source should bind.
        @type conf: str
        @param port: The port to which the agent source should listen.
        @type port: int
        
        @return: The created agent source configuration object.
        @rtype: AgentSourceConf
        """

        return flume_agent.AgentSourceConf(name, bind, port)

    @staticmethod
    def create_hdfs_sink_conf(storage, batch_size=BATCH_SIZE_DEF, roll_size=ROLL_SIZE_DEF,
                              roll_count=ROLL_COUNT_DEF, roll_ival=ROLL_IVAL_DEF, 
                              idle_timeout=IDLE_TIMEOUT_DEF):
        """
        A static method for creating HDFSSinkConf objects.
        
        @param storage: Configuration of the HDFS storage to which the sink should write its 
                        data.
        @type storage: StorageConf
        @param batch_size: The size of a single batch of data (in samples) that the sink 
                           should use when reading data from its channel.
        @type batch_size: int
        @param roll_size: The size of the data (in bytes) the sink should write to a single
                          file before rolling it and creating a new one. Should be set to zero
                          if not used.
        @type roll_size: int
        @param roll_count: The size of the data (in samples) the sink should write to a single
                          file before rolling it and creating a new one. Should be set to zero
                          if not used.
        @type roll_size: int
        @param roll_ival: Number of seconds the sink should wait befor rolling a file and
                          creating a new one. Should be set to zero if not used.
        @type roll_ival: int
        @param idle_timeout: Timeout after the sink should roll an inactive file and create a
                             new file. Should be set to zero if not used.
        @type idle_timeout: int
       
        @return: The created HDFS sink configuration object.
        @rtype: HDFSSinkConf
        """

        return flume_agent.HDFSSinkConf(storage, batch_size, roll_size, roll_count, roll_ival,
                                        idle_timeout)

    @staticmethod
    def create_analysis_conf(name, master, recv_host, recv_port, batch_ival=BATCH_IVAL_DEF,
                             jars=[], args=[]):
        """
        A static method for creating StreamAnalysisConf objects.

        @param name: The name of the analysis, which must be the name of the file of the analysis
                 source code file without the .scala suffix.
        @type name: str
        @param master: The type of the Spark master. Can be Spark master URL (spark://host:port), 
                       local[x] where x is the number of cores used, or yarn-client.
        @type master: str
        @param recv_host: The IP address of the node which acts as a stream receiver. In the future,
                          should support multiple receivers.
        @type recv_host: str
        @param recv_port: The port the receiver node listens for incoming data.
        @type recv_port: int
        @param batch_ival: The batch interval, at which the analysis should receive data, in
                           milliseconds.
        @type batch_ival: int
        @params jars: Jar file paths which will be distributed with the analysis.
        @type jars: sequence
        @param args: Extra commandline arguments for the analysis script.
        @type args: sequence
        
        @return: The created stream analysis configuration object.
        @rtype: StreamAnalysisConf
        """

        return stream_analysis.StreamAnalysisConf(name, master, recv_host, recv_port, 
                                                  batch_ival, jars, args)

    @classmethod
    def _create_agent_from_conf(cls, name, conf):
        """
        Creates a new FlumeAgent with the given name from the specified configuration which is
        assumed to be a dictionary.

        @param name: The name of the new agent
        @type name: str
        @param conf: The configuration of the agent. Required keys: host, conf.
        @type conf: dict

        @return: The created agent.
        @rtype: FlumeAgent

        @raise ConfException: Raised if the configuration cannot be read or it is missing keys.
        @raise AgentException: Raised if the agent initialisation fails.
        """
        
        try:
            conf = dict(conf)
        except (ValueError, TypeError) as e:
            msg = "Agent %s has corrupted conf: %s" % (name, conf)
            raise conf_admin2.ConfException(msg)

        try:
            conf_f = conf[CONF]
            host = conf[HOST]
        except KeyError as e:
            msg = cls._create_missing_key_msg("agent", conf, e)
            raise conf_admin2.ConfException(msg)
        return flume_agent.FlumeAgent(cls.create_agent_conf(name, conf_f, host))

    @classmethod
    def _create_analysis_from_conf(cls, name, conf):
        """
        Creates a new StreamAnalysis with the given name from the specified configuration, 
        which is assumed to be a dictionary

        @param name: The name of the new stream analysis
        @type name: str
        @param conf: The configuration of the analysis. Required keys: master, host, port.
                     Optional keys: batch_ival, args.
        @type conf: dict

        @return: The created stream analysis
        @rtype: StreamAnalysis

        @raise ConfException Raised if the configuration cannot be read or it is missing keys.
        @raise StreamAnalysisException: Raised if the analysis initialisation fails.
        """

        try:
            conf = dict(conf)
        except (ValueError, TypeError) as e:
            msg = "Analysis %s has corrupted conf: %s" % (name, conf)
            raise conf_admin2.ConfException(msg)
        
        try:
            master = conf[MASTER]
            recv_host = conf[HOST]
            recv_port = conf[PORT]
        except KeyError as e:
            msg = self._create_missing_key_msg("analysis", conf, e)
            raise conf_admin2.ConfException(msg)
        batch_ival = conf.get(BATCH_IVAL) or BATCH_IVAL_DEF
        jars = conf.get(JARS) or []
        args = conf.get(ARGS) or []
        an_conf = cls.create_analysis_conf(name, master, recv_host, recv_port, batch_ival, 
                                           jars, args)
        return stream_analysis.StreamAnalysis(an_conf)
        
    def _check_source_name(self, name):
        """
        Checks if the admin already has a DataSource with the specified name and raises an
        exception if such is the case.

        @param name: The name of source
        @type name: str
        
        @raise SourceAdminException: Raised if the source name is already in use or the name
                                     is not alphabetic and lowercase.
        """

        if self.has_source(name):
            msg = " Source with name %s already exists!" % name
            raise SourceAdminException(msg)
        if not name.isalpha() or not name.islower():
            msg = "Source name must be alphabetic and lowercase!"
            raise SourceAdminException(msg)

    def _create_source_from_conf(self, name, storages, agents, analyses):
        """
        Creates a new DataSource from given configuration arguments. Checks the configuration
        validity first.

        @param name: The name of the source.
        @type name: str
        @param storages: The storage configuration of the source.
        @type storages: dict
        @param agents: The agent configuration of the source.
        @type agents: dict
        @param analyses: The analysis configuration of the source
        @type analyses: dict

        @return: The created source.
        @rtype: DataSource
        
        @raise SourceAdminException: Raised if the name of the source is already taken.
        @raise ConfException: Raised if the configuration of storages, agents or analyses is
                              corrupted or contains invalid values.
        """

        self._check_source_name(name)
        try:
            storages = list(storages)
        except (TypeError, ValueError) as e:
            msg = "Storages corrupted in conf file %s: %s" % (self._conf_f, e)
            raise conf_admin2.ConfException(msg)
        if not all(self._stor_adm.has_storage(stor) for stor in storages):
            msg = "Source %s has invalid storages in its conf!" % name
            raise conf_admin2.ConfException(msg)
        
        try:
            agents = dict(agents)
        except (TypeError, ValueError) as e:
            msg = "Agents corrupted in conf file %s: %s" % (self._conf_f, e)
            raise conf_admin2.ConfException(msg)
        
        try:
            analyses = dict(analyses)
        except (TypeError, ValueError) as e:
            msg = "Analyses corrupted in conf file %s: %s" % (self._conf_f, e)
            raise conf_admin2.ConfException(msg)
        
        try:
            agents = [self._create_agent_from_conf(ag_name, conf) 
                      for ag_name, conf in agents.iteritems()]
        except flume_agent.AgentException as e:
            msg = "Failed to create agents from source %s conf! Reason: %s" % (name, e)
            raise conf_admin2.ConfException(msg)

        try:
            analyses = [self._create_analysis_from_conf(an_name, conf)
                        for an_name, conf in analyses.iteritems()]
        except stream_analysis.StreamAnalysisException as e:
            msg = "Failed to create analyses from source %s conf! Reason: %s" % (name, e)
            raise conf_admin2.ConfException(msg)
        
        return data_source.DataSource(name, storages, agents, analyses)

    def _parse_sources(self):
        """
        Parses the sources from a json file. The sources whose configuration contains errors
        are dismissed.

        @raise ConfException: Raised if the configuration file cannot be read or it is wholly 
                              corrupted. 
        """

        logging.info("Parsing data sources...")
        for name, src_conf in self._read_from_conf(SOURCES).iteritems():
            try:
                storages = src_conf[STORAGES]
                agents = src_conf[AGENTS]
                analyses = src_conf[ANALYSES]
            except KeyError as e:
                msg = self._create_missing_key_msg("source", src_conf, e)
                logging.exception(msg)
                continue
            
            try:
                src = self._create_source_from_conf(name, storages, agents, analyses)
            except SourceAdminException as e:
                logging.exception(str(e))
                continue
            except conf_admin2.ConfException as e:
                msg = self._create_parse_failure_msg("source " + name, e)
                logging.exception(msg)
                continue
            except data_source.SourceException as e:
                msg = "Failed to create source %s! Reason: %s" % (name, e)
                logging.exception(msg)
                continue

            self._srcs[name] = src
            logging.info("Data source %s parsed!" % name)
        
    def _add_source_to_conf(self, name, storages, agents, analyses=[]):
        """
        Adds a DataSource to the source configuration file as a JSON structure.
        
        @param name: The name of the DataSource.
        @type name: str
        @param storages: Names of the storages associated with the source.
        @type name: sequence of strings
        @param agents: Configurations of the Flume agents associated with the source.
        @type agents: sequence of AgentConfs
        @param analyses: Configurations of the Spark Streaming analyses associated
                         with the source.
        @type analyses: sequence of StreamAnalysisConfs
        
        @raise ConfException: Raised if an error occurs while writing the configuration.
        """
        ag_conf = dict((ag.name, {CONF : ag.conf, 
                                  HOST : ag.host}) 
                       for ag in agents)
        an_conf = dict((an.name, {MASTER : an.master,
                                  HOST : an.recv_host,
                                  PORT : an.recv_port,
                                  BATCH_IVAL : an.batch_ival,
                                  JARS : an.jars,
                                  ARGS : an.args})
                       for an in analyses)

        src_conf = {STORAGES : storages,
                    AGENTS : ag_conf,
                    ANALYSES : an_conf}
        self._update_conf(SOURCES, name, src_conf) 
        logging.info("Data source %s created to configuration!" % name)

    def get_source(self, name):
        """
        Returns the DataSource from the admin by name.

        @param name: The name of the source.
        @type name: str
        
        @return: The requested source object.
        @rtype: DataSource

        @raise SourceAdminException: Raised if the requested source is not found.
        """

        try:
            return self._srcs[name]
        except KeyError:
            msg = "Source %s does not exist!" % name
            raise SourceAdminException(msg)

    def list_sources(self):
        """
        Lists the names of all the DataSources in the admin.

        @return: The names of the admin's sources.
        @rtype: list of strings
        """
        
        return self._srcs.keys()

    def list_initialized_sources(self):
        """
        Lists the names of all the initialized DataSources in the admin.

        @return: The names of the admin's initialized sources.
        @rtype: list of strings
        """

        return [name for name, src in self._srcs.iteritems() if src.is_initialized()]

    def list_running_sources(self):
        """
        Lists the names of all the running DataSources in the admin.

        @return: The names of the admin's running sources.
        @rtype: list of strings
        """
        
        return [name for name, src in self._srcs.iteritems() if src.is_running()]

    def list_partly_running_sources(self):
        """
        Lists the names of all the partly running DataSources in the admin.

        @return: The names of the admin's partly running sources.
        @rtype: list of strings
        """
        
        return [name for name, src in self._srcs.iteritems() if src.is_partly_running()]

    def list_stopped_sources(self):
        """
        Lists the names of all the stopped DataSources in the admin

        @return: The names of the admin's stopped sources.
        @rtype: list of strings
        """
        
        return [name for name, src in self._srcs.iteritems() if not src.is_partly_running()]
    
    def get_sources(self):
        """
        Returns all the DataSources from the admin.
        
        @return: The sources of the admin.
        @rtype: list of DataSources
        """

        return self._srcs.values()

    def get_initialized_sources(self):
        """
        Returns all the initialized DataSources from the admin.
        
        @return: The initialized sources of the admin.
        @rtype: list of DataSources
        """
        
        return [src for src in self._srcs.itervalues() if src.is_initialized()]
    
    def get_running_sources(self):
        """
        Returns all the running DataSources from the admin.
        
        @return: The running sources of the admin.
        @rtype: list of DataSources
        """

        return [src for src in self._srcs.itervalues() if src.is_running()]

    def get_partly_running_sources(self):
        """
        Returns all the partly running DataSources from the admin.
        
        @return: The partly running sources of the admin.
        @rtype: list of DataSources
        """

        return [src for src in self._srcs.itervalues() if src.is_partly_running()]

    def get_stopped_sources(self):
        """
        Returns all the stopped DataSources from the admin.
        
        @return: The stopped sources of the admin.
        @rtype: list of DataSources
        """

        return [src for src in self._srcs.itervalues() if not src.is_partly_running()]

    def get_all_storages(self):
        """
        Returns all the storage names which are attached to the admin's DataSources.
        
        @return: The names of the storages
        @rtype: list of strings
        """

        return [stor for src in self._srcs.itervalues() for stor in src.storages]

    def has_source(self, name):
        """
        Determines if the admin has a DataSource with the given name.

        @return: True if a source with the name exists.
        @rtype: bool
        """
        
        return name in self._srcs

    def run_source(self, name):
        """
        Runs a source by name. No exceptions are raised if source fails to run.
        
        @raise SourceAdminException: Raised if no source with the given name is found.
        """

        self.get_source(name).run()

    def stop_source(self, name):
        """
        Stops a source by name. No exceptions are raised if source fails to stop

        @raise SourceAdminException: Raised if no source with the given name is found.
        """

        self.get_source(name).stop()

    def all_sources_initialized(self):
        """
        Determines if all the sources of the admin are initialized.

        @return: True if all the sources are initialized, otherwise False.
        @rtype: bool

        @raise FlumeException: Raised if fails to connect to Flume service to check an agent 
                               status.
        """
        
        return all(src.is_initialized() for src in self._srcs.itervalues())

    def some_sources_initialized(self):
        """
        Determines if some sources of the admin are initialized.

        @return: True if some of the sources are initialized, otherwise False.
        @rtype: bool

        @raise FlumeException: Raised if fails to connect to Flume service to check an agent 
                               status.
        """

        return any(src.is_initialized() for src in self._srcs.itervalues())

    def all_sources_running(self):
        """
        Determines if all the sources of the admin are running.

        @return: True if all the sources are running, otherwise False.
        @rtype: bool

        @raise FlumeException: Raised if an exception occurs when reading the state of an agent 
                               role.
        """

        return all(src.is_running() for src in self._srcs.itervalues())

    def some_sources_running(self):
        """
        Determines if some sources of the admin are running.

        @return: True if some sources are running, otherwise False.
        @rtype: bool

        @raise FlumeException: Raised if an exception occurs when reading the state of an agent 
                               role.
        """

        return any(src.is_running() for src in self._srcs.itervalues())

    def some_sources_partly_running(self):
        """
        Determines if some sources of the admin are partly running.

        @return: True if some sources are partly running, otherwise False.
        @rtype: bool

        @raise FlumeException: Raised if an exception occurs when reading the state of an agent 
                               role.
        """
        
        return any(src.is_partly_running() for src in self._srcs.itervalues())

    def run_sources(self):
        """
        Runs all the sources of the admin. Exceptions are not raised if source fails to run.
        """

        [src.run() for src in self._srcs.itervalues()]

    def stop_sources(self):
        """
        Stops all the sources of the admin. Exceptions are not raised if source fails to 
        stop.
        """
        
        [src.stop() for src in self._srcs.itervalues()]
    
    def add_simple_source(self, name, agent, raw_src, hdfs_sink):
        """
        Adds a simple data source with a single Flume agent with one source and one sink to 
        admin.

        @param name: The name of the DataSource to be added.
        @type name: str
        @param agent: The configuration of the Flume agent.
        @type agent: AgentConf
        @param raw_src: The netcat source of the Flume agent ingesting raw data.
        @type raw_src: AgentSourceConf
        @param hdfs_sink: The HDFS sink of the Flume agent writing data to the HDFS.
        @type hdfs_sink: AgentSinkConf

        @return: The source that was created and added.
        @rtype: DataSource

        @raise SourceAdminException: Raised if the source with given name already exists.
        @raise AgentException: Raised if the write operation of the agent configuration file 
                                fails.
        @raise StorageAdminException: Raised if the creation of the configured sink storage
                                      fails.
        @raise ConfException: Raised if an error occurs while writing storage or source
                              configuration.
        @raise SourceException: Raised if the creation of the DataSource fails.
        """
        
        self._check_source_name(name)
        stor = hdfs_sink.storage
        fa = flume_agent.FlumeAgent(agent)
        fa.create_simple_conf(raw_src, hdfs_sink)
        
        self._add_source_to_conf(name, [stor.name], [fa])
        self._stor_adm.add_storage(storage)
        src = data_source.DataSource(name, [stor.name], [fa])
        self._srcs[name] = src
        return src

    def add_source_with_analysis(self, name, agent, analysis, raw_src, hdfs_sinks, rslt_ports,
                                 raw_hdfs_sink=None):
        """
        Adds a simple data source with a single Flume agent and a single Spark Streaming
        analysis. The agent directs the data stream to analysis which is supposed to pipe it
        back to the agent. The agent should have ports listening to each analysis result stream
        and sinks to write them into HDFS.
        
        @param name: The name of the DataSource to be added.
        @type name: str
        @param agent: The configuration of the Flume agent.
        @type agent: AgentConf
        @param analysis: The configuration of the streaming analysis.
        @type analysis: StreamAnalysisConf
        @param raw_src: The netcat source of the Flume agent ingesting raw data.
        @type raw_src: AgentSourceConf
        @param hdfs_sinks: The HDFS sinks of the Flume agent writing data to the HDFS.
        @type hdfs_sinks: sequence of AgentSinkConfs
        @param rslt_ports: The numbers of the agent source ports listening to analysis result
                           streams.
        @type rslt_ports: sequence of ints
        @param raw_hdfs_sink: The HDFS sink of the Flume agent which can be used to directly 
                              write raw data to HDFS without passing it through the analysis.
        @type raw_hdfs_sink: AgentSinkConf

        @return: The source that was created and added.
        @rtype: DataSource

        @raise SourceAdminException: Raised if the source with given name already exists.
        @raise AgentException: Raised if the write operation of the agent configuration file 
                               fails.
        @raise StreamAnalysisException: Raised if the creation of the StreamAnalysis fails.
        @raise StorageAdminException: Raised if the creation of the configured sink storage
                                      fails.
        @raise ConfException: Raised if an error occurs while writing storage or source
                              configuration.
        @raise SourceException: Raised if the creation of the DataSource fails.
        """

        self._check_source_name(name)
        avro_sink = flume_agent.AvroSinkConf(agent.host, an.recv_port)
        fa = flume_agent.FlumeAgent(agent)
        sa = stream_analysis.StreamAnalysis(analysis)
        fa.create_complex_conf(raw_src, avro_sink, rslt_ports, hdfs_sinks, raw_hdfs_sink)
        
        storages = [sink.storage for sink in hdfs_sinks]
        storages.extend(raw_hdfs_sink.storage)
        stor_names = [stor.name for stor in storages]

        self._add_source_to_conf(name, stor_names, [fa], [sa])
        [self._stor_adm.add_storage(stor) for stor in storages]
        src = data_source.DataSource(name, stor_names, [fa], [sa])
        self._srcs[name] = src
        return src

    def add_custom_source(self, name, storages, agents, analyses=[]):
        """
        Adds a custom DataSource to the admin. The Flume agent configuration files must already 
        have been created by the user.

        @param name: The name of the DataSource to be added.
        @type name: str
        @param storages: Configurations of the data storages to be used by the source.
        @type storages: sequence of StorageConfs
        @param agents: Configurations of the Flume agents to be used by the source. 
        @type agents: sequence of AgentConfs
        @param analyses: Configurations of the Spark Streaming analyses to be used by the 
                         source.
        @type analyses: sequence of StreamAnalysisConfs
        
        @return The source that was created and added.
        @rtype: DataSource
        
        @raise SourceAdminException: Raised if the source with given name already exists.
        @raise StreamAnalysisException: Raised if the creation of a StreamAnalysis fails.
        @raise StorageAdminException: Raised if the creation of a storage fails.
        @raise ConfException: Raised if an error occurs while writing storage or source
                              configuration.
        @raise SourceException: Raised if the creation of the DataSource fails.
        """
        
        self._check_source_name(name)
        f_agents = [flume_agent.FlumeAgent(ag) for ag in agents]
        s_analyses = [stream_analysis.StreamAnalysis(an) for an in analyses]
        stor_names = [stor.name for stor in storages]

        self._add_source_to_conf(name, stor_names, agents, analyses)
        [self._stor_adm.add_storage(stor) for stor in storages]
        src = data_source.DataSource(name, stor_names, f_agents, s_analyses)
        self._srcs[name] = src
        return src

    def remove_source(self, name):
        """
        Removes the source with the given name from the admin and configuration file. Deletes
        also the roles of the Flume agents associated with the source. Attempts to clean
        the configuration even if the source is not found by admin, and does not raise errors
        if the cleanup fails.

        @param name: The name of the source to be removed.
        @type name: str
        
        @raise SourceAdminException: Raised if the source is found by admin but is still
                                     running.
        """

        if self.has_source(name):
            src = self.get_source(name)
            if src.is_running() or src.is_partly_running():
                msg = "Source %s must be stopped before it can be removed!" % name
                raise SourceAdminException(msg)

            del self._srcs[name] 
            logging.info("Source %s removed from admin!" % name)

            for ag in src.list_agents():
                try:
                    src.delete_agent_role(ag)
                    logging.info("Agent %s role deleted!" % ag)
                except (flume_agent.AgentException, flume_agent.FlumeException) as e:
                    logging.exception(str(e))
            
        try:    
            self._remove_from_conf(SOURCES, name)
        except conf_admin2.ConfException as e:
            logging.exception(str(e))
        else:
            logging.info("Source %s removed from configuration!" % name)

class SourceAdminException(Exception):
    """
    An exception which is raised when an error associated with a SourceAdmin is encountered.
    """
    pass
