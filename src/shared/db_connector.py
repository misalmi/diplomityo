# -*- coding: utf-8 -*-

import logging
import subprocess
import impala.dbapi
import impala.error
#import pyhs2
import thrift

from constants import *
import tables

AUTH_PLAIN = "PLAIN"

class SimpleConnector(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port

    def _connect_to_impala(self):
        try:
            conn = impala.dbapi.connect(self.host, self.port)
            cursor = conn.cursor()
        except thrift.transport.TTransport.TTransportException as e:
            msg = "Failed to connect to Impala! Reason %s" % e
            raise ImpalaException(msg)
        return conn, cursor

    def _close_impala_connection(self, conn, cursor):
        cursor.close()
        conn.close()

    def _execute_in_impala(self, cursor, stmt, params=None):
        if params is None:
            stmt_frmttd = stmt
        else:
            stmt_frmttd = impala.dbapi._bind_parameters(stmt, params)

        #logging.info("Executing Impala statment %s;" % stmt_frmttd)
        try:
            cursor.execute(stmt, params)
        except impala.error.RPCError as e:
            msg = "Failed to execute Impala statment %s! Reason: %s" % (stmt_frmttd, e)
            raise ImpalaException(msg)
        #logging.info("Statement %s; executed succesfully" % stmt_frmttd)

    def execute_and_fetch(self, stmt, params=None):
        conn, cursor = self._connect_to_impala()
        try:
            self._execute_in_impala(cursor, stmt, params)
            return cursor.fetchall()
        finally:
            self._close_impala_connection(conn, cursor)

    def get_table_schema(self, name):
        conn, cursor = self._connect_to_impala()
        try:
            return cursor.get_table_schema(name)
        finally:
            self._close_impala_connection(conn, cursor)

    def describe_formatted(self, name):
        stmt = "DESCRIBE FORMATTED %s" % tables.check_sql_string(name)
        return self.execute_and_fetch(stmt)

class DatabaseConnector(SimpleConnector):
    # def __init__(self, impala_host, impala_port, hive_host, 
    #              hive_port, hive_user, hive_passwd):
    #     super(DatabaseConnector, self).__init__(impala_host, impala_port)
    #     self.hive_host = hive_host
    #     self.hive_port = hive_port
    #     self.hive_user = hive_user
    #     self.hive_passwd = hive_passwd

    # def _execute_in_hive(self, cursor, stmt, params=None):
    #     if params is None:
    #         stmt_frmttd = stmt
    #     else:
    #         stmt_frmttd = impala.dbapi._bind_parameters(stmt, params)

    #     logging.info("Executing Hive statment %s;" % stmt_frmttd)
    #     try:
    #         cursor.execute(stmt_frmttd)
    #     except pyhs2.error.Pyhs2Exception as e:
    #         msg = "Failed to execute Hive statement %s! Reason %s" % (stmt_frmttd, e)
    #         raise HiveException(msg)
    #     logging.info("Statement %s; executed succesfully" % stmt_frmttd)

    # def execute_in_hive(self, stmt, params=None):
    #     logging.info("Executing hive statment %s;" % stmt)
    #     try:
    #         with pyhs2.connect(host=self.hive_host, 
    #                            port=self.hive_port, 
    #                            authMechanism=AUTH_PLAIN, 
    #                            user=self.hive_user, 
    #                            password=self.hive_passwd) as conn:
    #             with conn.cursor() as cursor:
    #                 self._execute_in_hive(cursor, stmt, params)
    #     except thrift.transport.TTransport.TTransportException as e:
    #         msg = "Failed to connect to Hive! Reason %s" % e
    #         raise HiveException(msg)
    
    def execute(self, stmt, params=None):
        conn, cursor = self._connect_to_impala()
        try:
            self._execute_in_impala(cursor, stmt, params)
        finally:
            self._close_impala_connection(conn, cursor)

    def get_table_schema(self, name):
        conn, cursor = self._connect_to_impala()
        try:
            return cursor.get_table_schema(name)
        except impala.error.RPCError as e:
            msg = "Failed to read table %s from Impala! Reason: %s" % (name, e)
            raise ImpalaException(msg)
        finally:
            self._close_impala_connection(conn, cursor)

    def get_tables(self):
        conn, cursor = self._connect_to_impala()
        try:
            cursor.get_tables()
            return cursor.fetchall()
        except impala.error.RPCError as e:
            msg = "Failed to read tables from Impala! Reason: %s" % e
            raise ImpalaException(msg)
        finally:
            self._close_impala_connection(conn, cursor)

    def has_table(self, name):
        return any(tbl_name == name for _, _, tbl_name, _, _ in self.get_tables())
        
    def refresh_table(self, name):
        stmt = "REFRESH %s" % tables.check_sql_string(name)
        return self.execute(stmt)

    def invalidate_table(self, name):
        stmt = "INVALIDATE METADATA %s" % tables.check_sql_string(name)
        return self.execute(stmt)
    
    def drop_table(self, name):
        stmt = "DROP TABLE %s" % tables.check_sql_string(name)
        return self.execute(stmt)

class HiveException(Exception):
    pass

class ImpalaException(Exception):
    pass
