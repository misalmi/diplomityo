# -*- coding: utf-8 -*-

import socket
import random
import time
from datetime import datetime

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(("127.0.0.1", 11111))
n = 1
try:
    while True:
        timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
        velocity = random.random() * 100 + 20
        area = random.randrange(9000, 9035)
        s.send("1,1,%s,%s,2528.7943,6500.559,%s,0,0,0\n" % (timestamp, area, velocity))
        if n % 20000 == 0:
            print datetime.now()
            n = 1
        else:
            n += 1
        time.sleep(0.0005)
except (KeyboardInterrupt, socket.error):
    s.close()
