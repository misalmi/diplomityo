# -*- coding: utf-8 -*-

import core
import table_admin

class ClusterApi(object):
    def __init__(self, spark_client, webservers=[]):

        self._sc = core.SystemCore(spark_client, webservers)

    def get_source_admin(self):
        return self._sc.src_admin

    def get_batch_analysis_admin(self):
        return self._sc.ba_admin

    def get_interface_admin(self):
        return self._sc.iface_admin

    def get_table_admin(self):
        return self._sc.tbl_admin

    def get_storage_admin(self):
        return self._sc.stor_admin

    def start_updater(self):
        self._sc.start_updater()

    def stop_updater(self):
        self._sc.stop_updater()

class ApiInitException(Exception):
    pass

if __name__ == "__main__":
    ca = ClusterApi((("192.168.109.101", 11113, "misalmi", "$:*.tiR3Nnw", "salakala"),))
