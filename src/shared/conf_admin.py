# -*- coding: utf-8 -*-

import os
import logging
import json

CONF_NOT_FOUND = "Conf file %s was not found!"

class ConfAdmin(object):
    def __init__(self, conf_f):
        if os.path.isfile(conf_f):
            self._conf_f = conf_f
        else:
            msg = CONF_NOT_FOUND % conf_f
            raise ConfException(msg)

    def _read_conf(self):
        try:
            with open(self._conf_f) as f:
                data = json.load(f)
        except IOError:
            msg = CONF_NOT_FOUND % self._conf_f
            raise ConfException(msg)
        except ValueError as e:
            msg = "Corrupted JSON data in conf file %s: %s" % (self._conf_f, e)
            raise ConfException(msg)
        return data

    def _read_from_conf(self, key):
        data = self._read_conf()
        try:
            return list(data[key])
        except KeyError as e:
            msg = "Conf %s has no key %s!" % (self._conf_f, key)
            raise ConfException(msg)
        except (TypeError, ValueError) as e:
            msg = "Corrupted data in conf file %s under key %s: %s" % (self._conf_f, key, e)

    def _write_conf(self, data):
        try:
            with open(self._conf_f, 'w') as f:
                json.dump(data, f)
        except IOError:
            msg = CONF_NOT_FOUND % self._conf_f
            raise ConfException(msg)

    def _append_to_conf(self, key, item):
        data = self._read_conf()
        try:
            data[key].append(item)
        except (KeyError, AttributeError) as e:
            msg = "Failed to append item %s under key %s to conf %s!" % (item, key, self._conf_f)
            raise ConfException(msg)
        self._write_conf(data) 

    def _remove_from_conf(self, key, cmp_attr, item_val):
        data = self._read_conf()
        try:
            items = data[key]
            for item in items:
                if item[cmp_attr] == item_val:
                    items.remove(item)
                    break
            else:
                msg = "No item under %s in %s has attr %s with value %s!" % (key, self._conf_f, 
                                                                             cmp_attr, item_val)
                raise ConfException(msg)
        except (KeyError, AttributeError, ValueError) as e:
            msg = "Failed to remove item under key %s from conf %s!" % (cmp_attr, key, self._conf_f)
            raise ConfException(msg)
        self._write_conf(data) 

class ConfException(Exception):
    pass
