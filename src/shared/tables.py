# -*- coding: utf-8 -*-

import re
import logging
from string import Template
import impala.dbapi

import utils
import settings
import db_connector
from constants import *

def check_sql_string(sql_str):
    if any(char in sql_str for char in INVALID_SQL_CHARACTERS):
        msg = "String %s contains invalid characters for SQL!" % sql_str
        raise SQLCharException(msg)
    return sql_str

def check_sql_strings(sql_strs):
    return [check_sql_strings(sql_str) for sql_str in sql_strs]

class Column(object):
    def __init__(self, name, sql_type):
        self.name = check_sql_string(name)
        self.type = self._check_sql_type(sql_type)

    def _check_sql_type(self, sql_type):
        if sql_type not in VALID_SQL_DATATYPES:
            msg = "SQL data type %s is not supported" % sql_type
            raise SQLTypeException(msg)
        return sql_type

class Table(object):
    def __init__(self, name, columns, connector):
        self.name = check_sql_string(name)
        self.columns = columns
        self._conn = connector
        self.part_lvl = LEVEL_CACHE

    def check_partition_level(self):
        if self.part_lvl < LEVEL_YEAR or self.part_lvl > LEVEL_HOUR:
            msg = ("Table %s configured with" 
                   " bad partition level: %s!" % (self.name, self.part_lvl))
            raise PartitionLevelException(msg)
    
    def execute(self, stmt):
        return self._conn.execute(stmt)

    def refresh(self):
        return self._conn.refresh_table(self.name)

    def invalidate(self):
        return self._conn.invalidate_table(self.name)

    def create_select_stmt(self, columns=None, cond=None):
        if columns is None:
            stmt = "SELECT %s FROM %s" % (self.build_column_name_string(), self.name)
        else:
            col_str = ", ".join(utils.escape_sql_strings(columns))
            stmt = "SELECT %s FROM %s" % (col_str, tbl)

        if cond is not None:
            stmt += " WHERE " + cond
        return stmt

    def select(self, columns=None, cond=None, params=None):
        select_stmt = self.create_select_stmt(columns, cond)
        return self._conn.execute_and_fetch(select_stmt, params)
    
    def build_partition_string(self):
        if self.part_lvl >= LEVEL_YEAR:
            part_str = "%s INT" % YEAR_PART
        if self.part_lvl >= LEVEL_MONTH:
            part_str += ", %s INT" % MONTH_PART
        if self.part_lvl >= LEVEL_DAY:
            part_str += ", %s INT" % DAY_PART
        if self.part_lvl >= LEVEL_HOUR:
            part_str += ", %s INT" % HOUR_PART
        return part_str

    def build_partition_value_string(self, dtime, sep=","):
        if self.part_lvl >= LEVEL_YEAR:
            part_str = "%s=%d" % (YEAR_PART, dtime.year)
        if self.part_lvl >= LEVEL_MONTH:
            part_str += "%s %s=%d" % (sep, MONTH_PART, dtime.month)
        if self.part_lvl >= LEVEL_DAY:
            part_str += "%s %s=%d" % (sep, DAY_PART, dtime.day)
        if self.part_lvl >= LEVEL_HOUR:
            part_str += "%s %s=%d" % (sep, HOUR_PART, dtime.hour)
        return part_str

    def build_column_string(self):
        return ", ".join([col.name + " " + col.type for col in self.columns])
    
    def build_column_name_string(self):
        return ", ".join([col.name for col in self.columns])

    def get_column_index(self, col_name):
        try:
            return next(i for i, col in enumerate(self.columns) 
                        if col.name == col_name)
        except StopIteration:
            msg = "No column named %s in table %s" % (col_name, self.name)
            raise ColumnNotFoundException(msg)

    def get_column_indexes(self):
        return dict((col.name, i) for i, col in enumerate(self.columns))

    def compare_columns(self, columns):
        return all(self.columns[i].name == col.name and 
                   self.columns[i].type == col.type 
                   for i, col in enumerate(columns))

class CacheTable(Table):
    def create(self):
        d = dict(name=self.name,
                 columns=self.build_column_string(),
                 path_id=PATH)
        stmt = Template("CREATE EXTERNAL TABLE $name($columns)"
                        " PARTITIONED BY (hour STRING)" 
                        " ROW FORMAT DELIMITED"
                        " FIELDS TERMINATED BY ','"
                        " LINES TERMINATED BY '\n'"
                        " LOCATION %($path_id)s").substitute(d)
        location = HDFS_INCOMING_PATH + re.sub(CACHE + '$', '', self.name)
        param = {PATH : location}
        self._conn.execute(stmt, param)
        self.invalidate()
                 
    def add_partition(self, hour):
        d = dict(name=self.name,
                 part="'%02d'" % hour)
        stmt = Template("ALTER TABLE $name"
                        " ADD PARTITION(hour=$part)").substitute(d)
        self._conn.execute(stmt)

    def drop_partition(self, hour):
        d = dict(name=self.name,
                 part="'%02d'" % hour)
        stmt = Template("ALTER TABLE $name"
                        " DROP IF EXISTS"
                        " PARTITION(hour=$part)").substitute(d)
        self._conn.execute(stmt)
        self.refresh()

class ArchiveTable(Table):
    def __init__(self, name, columns, part_lvl, connector):
        super(ArchiveTable, self).__init__(name, columns, connector)
        self.part_lvl = part_lvl
        self.check_partition_level()

    def create(self):
        d = dict(name=self.name,
                 columns=self.build_column_string(),
                 parts=self.build_partition_string())
        stmt = Template("CREATE TABLE $name($columns)"
                        " PARTITIONED BY ($parts)" 
                        " STORED AS PARQUETFILE").substitute(d)
        return self.execute(stmt)

    def insert_into(self, cache, dtime):
        d = dict(name=self.name,
                 parts=self.build_partition_value_string(dtime),
                 columns=self.build_column_name_string(),
                 cache=cache.name,
                 cache_part="'%02d'" % dtime.hour)
        stmt = Template("INSERT INTO TABLE $name"
                        " PARTITION($parts)"
                        " SELECT $columns FROM $cache"
                        " WHERE hour=$cache_part").substitute(d)
        self.execute(stmt)

    def insert_overwrite(self, dtime):
        #TODO: Better way to merge Parquet files if necessary.
        
        d = dict(name=self.name,
                 parts=self.build_partition_value_string(dtime),
                 columns=self.build_column_name_string(),
                 part_cond=self.build_partition_value_string(dtime, sep=" AND"))
        stmt = Template("INSERT OVERWRITE TABLE $name"
                        " PARTITION($parts)"
                        " SELECT $columns FROM $name"
                        " WHERE $part_cond").substitute(d)
        self.execute(stmt)

class DataTablePair:
    def __init__(self, name, columns, part_lvl, connector):
        self.name = name
        self.cache = CacheTable(name + CACHE, columns, connector)
        self.archive = ArchiveTable(name + ARCHIVE, columns, part_lvl, connector)
        
    def execute(self, stmt, params):
        return self.cache._conn.execute_and_fetch(stmt, params)

    def create(self):
        self.cache.create() 
        self.archive.create()

    def create_select_stmt(self, union_id="t", union_cols=None, select_cols=None, cond=None):
        cache_stmt = self.cache.create_select_stmt(union_cols)
        archive_stmt = self.archive.create_select_stmt(union_cols)
        union_clause = "SELECT DISTINCT * FROM (%s UNION ALL %s) t" % (cache_stmt, archive_stmt)
        from_clause = "(%s) %s" % (union_clause, union_id)
       
        if cond is not None:
            from_clause += " WHERE " + cond
        
        if select_cols is None:
            select_stmt = "SELECT * FROM %s" % from_clause
        else:
            col_str = ", ".join(utils.escape_sql_strings(select_cols))
            select_stmt = "SELECT %s FROM %s" % (col_str, from_clause)
        return select_stmt

    def select(self, union_id="t", union_cols=None, select_cols=None, cond=None, params=None):
        select_stmt = self.create_select_stmt(union_id, union_cols, select_cols, cond)
        return self.cache._conn.execute_and_fetch(select_stmt, params)

    def get_column_index(self, col_name):
        return self.cache.get_column_index(col_name)
    
    def get_column_indexes(self):
        return self.cache.get_column_indexes()

    def compare_columns(self, columns):
        return self.cache.compare_columns(columns)

class ResultTable(Table):
    def __init__(self, name, columns, part_lvl, connector):
        super(ResultTable, self).__init__(name, columns, connector)
        self.part_lvl = part_lvl
        self.check_partition_level()

    def create(self):
        d = dict(name=self.name,
                 columns=self.build_column_string(),
                 parts=self.build_partition_string())
        stmt = Template("CREATE TABLE $name($columns)"
                        " PARTITIONED BY ($parts)"
                        " STORED AS PARQUETFILE").substitute(d)
        return self.execute(stmt)

    def convert_result(self, result):
        rows = []
        for res in result:
            row = []
            for i, val in enumerate(res):
                ctype = self.columns[i].type
                if ctype == "string":
                    row.append("'%s'" % check_sql_string(val))
                elif ctype == "float" or ctype == "double":
                    row.append("%.7f" % val)
                elif ctype == "timestamp":
                    row.append("'%s'" % utils.format_timestamp(val))
                else:
                    row.append("%s" % val)
            rows.append(",".join(row))
        return rows
    
    def insert_into(self, result, dtime):
        values = "), (".join([row for row in result])

        d = dict(name=self.name,
                 parts=self.build_partition_value_string(dtime),
                 values=values,
                 columns=self.build_column_name_string(),
                 part_cond=self.build_partition_value_string(dtime, sep=" AND"))
        into_stmt = Template("INSERT INTO TABLE $name"
                             " PARTITION($parts)" 
                             " VALUES($values)").substitute(d)
        overwrite_stmt = Template("INSERT OVERWRITE TABLE $name" 
                                  " PARTITION($parts)"
                                  " SELECT $columns FROM $name"
                                  " WHERE $part_cond").substitute(d)
        self.execute(into_stmt)
        self.execute(overwrite_stmt)

class ColumnNotFoundException(Exception):
    pass

class PartitionLevelException(Exception):
    pass

class SQLTypeException(Exception):
    pass

class SQLCharException(Exception):
    pass

