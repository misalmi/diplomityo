# -*- coding: utf-8 -*-

import hmac
import scp
import sys
import Pyro4
import paramiko
import logging
from Crypto.Hash import SHA256
sys.path.append("..")
from shared import tables

SSH_PORT = 22

def create_ssh_client(server, user, passwd):
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(server, SSH_PORT, user, passwd)
    return client

def scp_file(ssh, fname, remote_d):
    try:
        scp_client = scp.SCPClient(ssh.get_transport())
        scp_client.put(fname, remote_d)
    except (scp.SCPException, OSError):
        msg = " Remote copy of file %s to remote directory %s failed!" % (fname, remote_d)
        logging.exception(msg)
    except IOError:
        msg = " File %s was not found!" % fname
        logging.exception(msg)

Pyro4.config.HMAC_KEY = SHA256.new("salakala").hexdigest()
ns = Pyro4.locateNS(host="datanode1.traffic.pilot", port=11113)
url = ns.lookup("interface.server")
print ns.list()
proxy = Pyro4.Proxy(url)
ssh = create_ssh_client("datanode1.traffic.pilot", "misalmi", "$:*.tiR3Nnw")
path = proxy.get_interface_path()
print path
#scp_file(ssh, "../../user/interfaces/traffic.py", path)
ssh.close()
tbl = {"name" : "totalvelocity", "columns" : [{"name":"area", "type":"int"}, {"name":"time", "type":"timestamp"}, {"name":"obs", "type":"int"}, {"name":"v_t", "type":"double"}]}
proxy.update("traffic", "TrafficInterface", [], [tbl])
print proxy.get_filenames()
print proxy.get_interface_confs()
proxy.delete("traffic")
print proxy.get_filenames()
print proxy.get_interface_confs()
