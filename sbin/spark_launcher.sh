#!/bin/sh

source /etc/spark/conf/spark-env.sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CLASSPATH=/etc/hadoop/conf
CLASSPATH=$CLASSPATH:$DIR/../src/streaming/*
CLASSPATH=$CLASSPATH:$SCALA_LIBRARY_PATH/*
CLASSPATH=$CLASSPATH:$HADOOP_HOME/*:$HADOOP_HOME/lib/*
CLASSPATH=$CLASSPATH:$HADOOP_HOME/../hadoop-mapreduce/*
CLASSPATH=$CLASSPATH:$HADOOP_HOME/../hadoop-mapreduce/lib/*
CLASSPATH=$CLASSPATH:$HADOOP_HOME/../hadoop-yarn/*
CLASSPATH=$CLASSPATH:$HADOOP_HOME/../hadoop-yarn/lib/*
CLASSPATH=$CLASSPATH:$HADOOP_HOME/../hadoop-hdfs/*
CLASSPATH=$CLASSPATH:$HADOOP_HOME/../hadoop-hdfs/lib/*
CLASSPATH=$CLASSPATH:$SPARK_HOME/assembly/lib/*
CLASSPATH=$CLASSPATH:$SPARK_HOME/examples/lib/*
CLASSPATH=$CLASSPATH:$SPARK_HOME/../flume-ng/lib/*
CLASSPATH=$CLASSPATH:$SPARK_HOME/streaming/lib/*

export SPARK_DRIVER_MEMORY=512m
export SPARK_EXECUTOR_MEMORY=512m
export SPARK_EXECUTOR_CORES=2

java -cp $CLASSPATH $@