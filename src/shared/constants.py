# -*- coding: utf-8 -*-

import settings

# json and miscellaneous string constants
CACHE = "cache"
ARCHIVE = "archive"
DATA_TABLES = "data_tables"
RESULT_TABLES = "result_tables"
IFACES = "interfaces"
STORAGES = "storages"
NAME = "name"
TYPE = "type"
CLASS = "cls"
COLUMNS = "columns"
HOST = "host"
PORT = "port"
PATH = "path"
PY_SUFFIX = ".py"
PYC_SUFFIX = ".pyc"

# queue commands
UPDATE_CMD = "update"
DEL_CMD = "delete"
START_CMD = "start"
STOP_CMD = "stop"

# path constants
CONF_PATH = "../../conf/"
TMP_PATH = "../../tmp/"
USER_PATH = "../../user/"
FLUME_PATH = USER_PATH + "flume/"
STREAMING_PATH = USER_PATH + "streaming/"
BATCH_PATH = USER_PATH + "batch/"
IFACE_PATH = USER_PATH + "interfaces/"
HDFS_INCOMING_PATH = settings.hdfs_path + CACHE + "/"
HOUR_DIR = "/hour=%H"

# configuration files
TABLE_CONF_F = CONF_PATH + "tables.json"
BATCH_CONF_F = CONF_PATH + "batch_analyses.json"
SOURCE_CONF_F = CONF_PATH + "sources.json"
STORAGE_CONF_F = CONF_PATH + "storages.json"

# sql constants
VALID_SQL_DATATYPES = ("bigint", "boolean", "double", "float", "int", 
                       "real", "smallint", "string", "timestamp", "tinyint")
VALID_SQL_FUNCTIONS = ("AVG", "COUNT", "NVD", "MAX", "MIN", "SUM")
INVALID_SQL_CHARACTERS = ("\\", "\n", "\r", "'", "\"")

# cli constants
ECHO = "echo"
HIVE = "hive"
IMPALA = "impala-shell"

# connection constants
LOCALHOST = "127.0.0.1"

# partition and launch levels
LEVEL_CACHE = 1
LEVEL_YEAR = 1
LEVEL_MONTH = 2
LEVEL_DAY = 3
LEVEL_HOUR = 4

# partition names
HOUR_PART = "hour"
DAY_PART = "day"
MONTH_PART = "month"
YEAR_PART = "year"

# partition interval constant
PARTITION_ADD_M = 59

# allowed module name limit
MOD_MAX_N = 1000

# parse errors
def conf_parse_failed(msg):
    return "Failed to parse " + msg + " conf %s! Missing_key: %s"

DTBL_PARSE_FAILED = conf_parse_failed("data table pair")
RTBL_PARSE_FAILED = conf_parse_failed("result table")
COL_PARSE_FAILED = conf_parse_failed("column")
ANALYSIS_PARSE_FAILED = conf_parse_failed("batch analysis")
IFACE_PARSE_FAILED = conf_parse_failed("interface")

# init errors
METHOD_NOT_IMPLEMENTED = "Class %s does not implement required method %s!"
