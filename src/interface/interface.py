# -*- coding: utf-8 -*-

import sys
import impala.error
from flask.ext.restful import reqparse

sys.path.append("..")
from shared import tables, utils, db_connector
from shared.constants import *

class QueryInterface(object):
    def __init__(self, data_tbls=set(), rslt_tbls=set()):
        self._data_tbls = data_tbls
        self._rslt_tbls = rslt_tbls
        self._parser = reqparse.RequestParser()
        self._args = None

    @staticmethod
    def create_between_cond(col, key, val1, val2, tbl_id=""):
        key1, key2 = key + "1", key + "2"
        if tbl_id:
            tbl_id = tbl_id + "."

        cond = "(%s%s BETWEEN %%(%s)s AND %%(%s)s)" % (tbl_id, col, key1, key2)
        params = {key1 : val1, key2 : val2}
        return cond, params

    @staticmethod
    def create_in_cond(col, key, vals, tbl_id=""):
        if len(vals) == 0:
            return "", {}
        if tbl_id:
            tbl_id = tbl_id + "."
 
        params = dict(("%s%d" % (key, i + 1), val) for i, val in enumerate(vals))
        pholders = ",".join("%%(%s)s" % k for k in params.iterkeys())
        cond = "(%s%s IN (%s))" % (tbl_id, col, pholders)

        return cond, params

    @staticmethod
    def create_cmp_cond(col, key, val, operator, tbl_id=""):
        if tbl_id:
            tbl_id = tbl_id + "."
 
        cond = "(%s%s %s %%(%s)s)" % (tbl_id, col, operator, key)
        param = {key : val}
        return cond, param

    def create_union_select_stmt(self, tbl_name, union_id="t", union_cols=None, select_cols=None,
                                 cond=None):
        for tbl in self._data_tbls:
            if tbl.name == tbl_name:
                try:
                    return tbl.create_select_stmt(union_id, union_cols, select_cols, cond)
                except impala.error.RPCError as e:
                    raise QueryFailedException(str(e))
        else:
            msg = "Data table pair %s does not exist!" % tbl_name
            raise QueryFailedException(msg)

    def select(self, tbl_name, stmt, params=None):
        for tbl in self._data_tbls:
            if tbl.name == tbl_name:
                try:
                    return tbl.execute(stmt, params)
                except impala.error.RPCError as e:
                    raise QueryFailedException(str(e))
        else:
            msg = "Data table pair %s does not exist!" % tbl_name
            raise QueryFailedException(msg)

    def select_from_data(self, tbl_name, union_id="t", union_cols=None, select_cols=None, 
                         cond=None, params=None):
        for tbl in self._data_tbls:
            if tbl.name == tbl_name:
                try:
                    return tbl.select(union_id, union_cols, select_cols, cond, params)
                except impala.error.RPCError as e:
                    raise QueryFailedException(str(e))
        else:
            msg = "Data table pair %s does not exist!" % tbl_name
            raise QueryFailedException(msg)

    def select_from_results(self, tbl_name, columns=None, cond=None, params=None):
        for tbl in self._rslt_tbls:
            if tbl.name == tbl_name:
                try:
                    return tbl.select(columns, cond, params)
                except impala.error.RPCError as e:
                    raise QueryFailedException(str(e))
        else:
            msg = "Result table %s does not exist!" % tbl_name
            raise QueryFailedException(msg)

    def parse_args(self):
        raise NotImplementedError("Subclasess should implement this!")

    def query(self):
        raise NotImplementedError("Subclasess should implement this!")

class QueryFailedException(Exception):
    pass

class ParseException(Exception):
    pass
