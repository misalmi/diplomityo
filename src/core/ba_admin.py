# -*- coding: utf-8 -*-

import os
import sys
import logging
import importlib
import inspect
from collections import namedtuple

sys.path.append("..")
from shared.constants import *
from shared import conf_admin
import table_admin
import batch_analysis
sys.path.append(BATCH_PATH)

ANALYSES = "analyses"
RESULT_TABLE = "result_table"
LAUNCH_LEVEL = "launch_lvl"
LAUNCH_METHOD = "launch_task"
BatchAnalysis = namedtuple("BatchAnalysis", (NAME, 
                                             CLASS,
                                             DATA_TABLES, 
                                             RESULT_TABLE,
                                             LAUNCH_LEVEL))

class BatchAnalysisAdmin(conf_admin.ConfAdmin):
    def __init__(self, conf_f, queue, tbl_admin):
        super(BatchAnalysisAdmin, self).__init__(conf_f)
        self._queue = queue
        self._tbl_admin = tbl_admin
        self._mod_names = set()
        self._ba_attached = []
        self._ba_unattached = []
        self._parse_analyses()
        [self._queue.put((UPDATE_CMD, ba)) for ba in self._ba_attached]

    @staticmethod
    def check_analysis_script(name, cls_name):
        fname = BATCH_PATH + name.lstrip("/") + PY_SUFFIX

        if not os.path.exists(fname):
            msg = "Analysis script module %s does not exist!" % fname
            raise AnalysisInitException(msg)
        try:
            if sys.modules.has_key(name):
                m = reload(sys.modules[name])
            else:
                m = importlib.import_module(name) 
        except ImportError as e:
            msg = "ImportError occurred while importing analysis script %s: %s!" % (fname, e)
            raise AnalysisInitException(msg) 
        except Exception as e:
            msg = "Exception occured while importing analysis script %s: %s" % (fname, e)
            raise AnalysisInitException(msg)
    
        try:
            c = getattr(m, cls_name)
        except AttributeError as e:
            msg = "Failed to find class %s from analysis script %s" % (fname, cls_name)
            raise AnalysisInitException(msg)
        except Exception as e:
            msg = "Exception occured while accessing class %s" % cls_name
            raise AnalysisInitException(msg)

        if not inspect.isclass(c):
            msg = "Object %s is not a class!" % cls_name
            raise AnalysisInitException(msg)
        if not hasattr(c, LAUNCH_METHOD):
            msg = METHOD_NOT_IMPLEMENTED % (cls_name, LAUNCH_METHOD)
            raise AnalysisInitException(msg)
        if not issubclass(c, batch_analysis.AnalysisScript):
            msg = "Class %s is not an instance of class AnalysisScript!" % cls_name
            raise AnalysisInitException(msg)
        return c

    def _check_analysis_script(self, name, cls_name):
        if len(self._mod_names) > settings.module_limit:
            msg = "Module namespace exhausted! Program restart required to load more modules."
            raise AnalysisInitException(msg)
        BatchAnalysisAdmin.check_analysis_script(name, cls_name)

    def _build_data_table_dict(self, data_tbls):
        if type(data_tbls) != list:
            msg = "%s should be a list of names of data table pairs!" % data_tbls
            raise BatchAnalysisAdminException(msg)
        
        return dict((name, self._tbl_admin.get_data_table_pair(name)) 
                    for name in data_tbls)

    def _check_analysis_name(self, name):
        if self.has_analysis(name):
            msg = "Analysis %s already exists!" % name
            raise BatchAnalysisAdminException(msg)

    def _check_result_table(self, name):
        if (any(ba.result_table.name == name
                for ba in self._ba_attached) or
            any(ba.result_table == name
                for ba in self._ba_unattached)):
            msg = "Result table %s is already bound to another analysis!" % name
            raise BatchAnalysisAdminException(msg)

    def _check_launch_level(self, name, lvl):
        if lvl < LEVEL_YEAR or lvl > LEVEL_HOUR:
            msg = "Analysis %s configured with bad partition level: %s!" % (name, lvl)
            raise BatchAnalysisAdminException(msg)

    def _create_attached_analysis(self, name, cls_name, data_tbl_d, rslt_tbl, launch_lvl):
        rslt_tbl = self._tbl_admin.get_result_table(rslt_tbl)
        return BatchAnalysis(name, cls_name, data_tbl_d, rslt_tbl, launch_lvl)
        
    def _parse_analyses(self):
        logging.info("Parsing batch analysis configurations...")

        for conf in self._read_from_conf(ANALYSES):
            try:
                name = conf[NAME]
                cls_name = conf[CLASS]
                data_tbls = conf[DATA_TABLES]
                rslt_tbl = conf[RESULT_TABLE]
                launch_lvl = conf[LAUNCH_LEVEL]
            except KeyError as e:
                msg = ANALYSIS_PARSE_FAILED % (conf, e)
                logging.exception(msg)
                continue
            
            try:
                self._check_analysis_name(name)
                self._check_result_table(name)
                self._check_launch_level(name, launch_lvl)
            except BatchAnalysisAdminException as e:
                logging.exception(str(e))
                continue
            try:
                data_tbl_d = self._build_data_table_dict(data_tbls)
            except (BatchAnalysisAdminException, 
                    table_admin.TableAdminException) as e:
                msg = "Failed to read data table pairs of analysis %s!" % name
                logging.exception(msg)
                continue

            try:
                self._check_analysis_script(name, cls_name)
            except AnalysisInitException as e:
                logging.exception(str(e))
                continue

            if self._tbl_admin.has_result_table(rslt_tbl):
                ba = self._create_attached_analysis(name, cls_name, data_tbl_d, 
                                                    rslt_tbl, launch_lvl)
                self._ba_attached.append(ba)
            else:
                ba = BatchAnalysis(name, cls_name, data_tbl_d, rslt_tbl, launch_lvl)
                self._ba_unattached.append(ba)
            logging.info("Analysis %s configuration parsed!" % name)

    def _get_unattached_analysis(self, name):
        for ba in self._ba_unattached:
            if ba.name == name:
                return ba
        else:
            msg = "Unattached analysis %s not found!" % name
            raise BatchAnalysisAdminException(msg)

    def _get_attached_analysis(self, name):
        for ba in self._ba_attached:
            if ba.name == name:
                return ba
        else:
            msg = "Attached analysis %s not found!" % name
            raise BatchAnalysisAdminException(msg)
    
    def _add_attached_analysis(self, ba):
        rslt_tbl = self._tbl_admin.get_result_table(ba.result_table)
        ba = BatchAnalysis(ba.name, ba.cls, ba.data_tables, rslt_tbl, ba.launch_lvl)
        self._ba_attached.append(ba)
        self._queue.put((UPDATE_CMD, ba))

    def has_analysis(self, name):
        try:
            self._get_unattached_analysis(name)
        except BatchAnalysisAdminException:
            try:
                self._get_attached_analysis(name)
            except BatchAnalysisAdminException:
                return False
        return True

    def add_analysis(self, name, cls_name, data_tbls, rslt_tbl, launch_lvl):  
        self._check_analysis_name(name)
        self._check_result_table(name)
        self._check_launch_level(name, launch_lvl)
        self._check_analysis_script(name, cls_name)
        data_tbl_d = self._build_data_table_dict(data_tbls)
        
        ba_conf = {NAME : name, 
                   CLASS : cls_name,
                   DATA_TABLES : data_tbls,
                   RESULT_TABLE : rslt_tbl,
                   LAUNCH_LEVEL : launch_lvl}
        self._append_to_conf(ANALYSES, ba_conf)
        logging.info("Data analysis %s added to configuration!" % name)
        if self._tbl_admin.has_result_table(rslt_tbl):
            ba = self._create_attached_analysis(name, cls_name, data_tbl_d, 
                                                rslt_tbl, launch_lvl)
            self._queue.put((UPDATE_CMD, ba))
            self._ba_attached.append(ba)
        else:
            ba = BatchAnalysis(name, cls_name, data_tbl_d, rslt_tbl, launch_lvl)
            self._ba_unattached.append(ba)
        logging.info("Data analysis %s added to admin!" % name)

    def add_analysis_with_table(self, name, cls_name, data_tbl_names, rslt_tbl_name, 
                                rslt_tbl_cols, rslt_tbl_part_lvl, launch_lvl):
        if self._tbl_admin.has_result_table(name):
            msg = "Result table %s already exists!" % rslt_tbl_name
            raise table_admin.TableAdminException(msg)
        self.add_analysis(name, cls_name, data_tbl_names, rslt_tbl_name, launch_lvl)
        self._tbl_admin.add_result_table(rslt_tbl_name, rslt_tbl_cols, 
                                         rslt_tbl_part_lvl)
        self.attach_analysis_to_table(name)

    def attach_analysis_to_table(self, name):
        ba = self._get_unattached_analysis(name)
        self._ba_unattached.remove(ba)
        self._add_attached_analysis(ba)
        msg = "Data analysis %s attached to result table %s!" % (name, 
                                                                 ba.result_table)
        logging.info(msg)

    def remove_analysis(self, name):
        msg = "Data analysis %s removed from admin!" % name
        try:
            ba = self._get_unattached_analysis(name)
        except BatchAnalysisAdminException:
            try:
                ba = self._get_attached_analysis(name)
                self._ba_attached.remove(ba)
                self._queue.put((DEL_CMD, name))
                logging.info(msg)
            except BatchAnalysisAdminException:
                pass
        else:
            self._ba_unattached.remove(ba)
            logging.info(msg)

        try:
            self._remove_from_conf(ANALYSES, NAME, name)
            logging.info("Data analysis %s removed from configuration!" % name)
        except conf_admin.ConfException as e:
            logging.exception(str(e))

    def list_analyses(self):
        return self.list_attached_analyses() + self.list_unattached_analyses()

    def list_attached_analyses(self):
        return [ba.name for ba in self._ba_attached]

    def list_unattached_analyses(self):
        return [ba.name for ba in self._ba_unattached]
    
    def list_analysis_data_tables(self):
        data_tbls = set()
        [data_tbls.update(ba.data_tables.keys()) for ba in self._ba_attached]
        [data_tbls.update(ba.data_tables.keys()) for ba in self._ba_unattached]
        return list(data_tbls)

    def list_analysis_result_tables(self):
        return [ba.result_table.name for ba in self._ba_attached]

    def get_analysis_configuration(self):
        return self._read_conf(ANALYSES)

class BatchAnalysisAdminException(Exception):
    pass

class AnalysisInitException(Exception):
    pass
