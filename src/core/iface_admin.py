# -*- coding: utf-8 -*-

import os
import sys
import logging
import importlib
import inspect
import itertools
from collections import namedtuple

import paramiko
import scp
import Pyro4
from Crypto.Hash import SHA256

sys.path.append("..")
from shared import conf_admin, utils
from shared.constants import *
import table_admin
import settings

SSH_PORT = 22

Pyro4.util.all_exceptions["conf_admin.ConfException"] = conf_admin.ConfException
Pyro4.util.all_exceptions["shared.utils.InterfaceInitException"] = utils.InterfaceInitException

class ServerData(object):
    def __init__(self, host, port, user, keyfile, hmac_key, tbl_admin):
        self._host = host
        self._port = port
        self._hmac_key = hmac_key
        self._tbl_admin = tbl_admin
        self._proxy = None
        self.user = user
        self.keyfile = keyfile

    def _activate_hmac_key(self):
        Pyro4.config.HMAC_KEY = SHA256.new(self._hmac_key).hexdigest()

    def _get_proxy(self):
        if self._proxy is None:
            msg = "No proxy to %s:%s" % (self._host, self._port)
            raise RemoteConnectionException(msg)
        return self._proxy

    def create_proxy(self):
        self._activate_hmac_key()
        try:
            ns = Pyro4.locateNS(host=self._host, port=self._port)
            url = ns.lookup(settings.iface_server)
            self._proxy = Pyro4.Proxy(url)
        except Pyro4.errors.PyroError:
            msg = "Failed to create proxy to %s:%s" % (self._host, self._port)
            raise RemoteConnectionException(msg)

    def _check_data_tables(self, data_tbls):
        for tbl in data_tbls:
            if not self._tbl_admin.has_data_table_pair(tbl):
                msg = "Data table pair %s does not exist!" % tbl
                raise InterfaceConfException(msg)
                
    def _check_result_tables(self, rslt_tbls):
        for tbl_name in rslt_tbls:
            if not self._tbl_admin.has_result_table(tbl_name):
                msg = "Result table %s does not exist!" % tbl
                raise InterfaceConfException(msg)

    def _read_tables(self, confs):
        data_tbls = set()
        rslt_tbls = set()

        try:
            for conf in confs:
                data_tbls.update(tbl[NAME] for tbl in conf[DATA_TABLES])
                rslt_tbls.update(tbl[NAME] for tbl in conf[RESULT_TABLES])
        except TypeError as e:
             msg = ("Failed to read interface conf at %s:%s."
                    " Parameter of invalid type: %s" % (self._host, self._port, e))
             raise InterfaceConfException(msg)
        except KeyError as e:
            msg = ("Failed to read interface conf at %s:%s."
                   " Missing key: %s" % (self._host, self._port, e))
            raise InterfaceConfException(msg)

        self._check_data_tables(data_tbls)
        self._check_result_tables(rslt_tbls)
        return list(data_tbls), list(rslt_tbls)

    def read_confs(self):
        proxy = self._get_proxy() 
        self._activate_hmac_key()
        try:
            return proxy.get_interface_confs()
        except Pyro4.errors.PyroError:
            msg = "Failed to connect to server at %s:%s" % (self._host, self._port)
            raise RemoteConnectionException(msg)
        except conf_admin.ConfException as e:
            msg = ("Failed to read interface confs from server at %s:%s"
                   " Reason: %s" % (self._host, self._port, e))
            raise RemoteServerException(msg)

        if type(ifaces) != list:
            msg = ("Invalid interface configuration at server %s:%s!"
                   " Conf object must be of type list." % (self._host, self._port))
            raise InterfaceConfException(msg)

    def read_tables(self):
        confs = self.read_confs()
        return self._read_tables(confs)

    def get_interface(self, name):
        ifaces = self.read_conf()
        for iface in ifaces:
            if iface[NAME] == name:
                return iface
        msg = "Interface %s not found from server at %s:%s"
        raise StopIteration(msg)

    def get_interface_path(self):
        proxy = self._get_proxy()
        self._activate_hmac_key()
        
        try:
            return proxy.get_interface_path()
        except Pyro4.errors.PyroError as e:
            msg = "Failed to connect to server at %s:%s! Reason: %s" % (self._host, self._port, 
                                                                        e)
            raise RemoteConnectionException(msg)
        except Exception as e:
            msg = ("Failed to retrieve remote target directory"
                   " from %s:%s" % (self._host, self._port))
            raise RemoteServerException(msg)

    def update_remote_interface(self, name, cls, data_tbls, rslt_tbls):
        proxy = self._get_proxy()
        self._activate_hmac_key()
        try:
            proxy.update(name, cls, data_tbls, rslt_tbls)
        except Pyro4.errors.PyroError as e:
            msg = "Failed to connect to server at %s:%s! Reason: %s" % (self._host, self._port,
                                                                        e)
            raise RemoteConnectionException(msg)
        except (conf_admin.ConfException, utils.InterfaceInitException) as e:
            msg = ("Initialization of interface %s at %s:%s failed!"
                   " Reason: %s" % (name, self._host, self._port, e))
            raise RemoteServerException(msg)
        except Exception as e:
            print "".join(Pyro4.util.getPyroTraceback())
            raise e

    def remove_remote_interface(self, name):
        proxy = self._get_proxy()
        self._activate_hmac_key()
        try:
            proxy.remove(name)
        except Pyro4.errors.PyroError:
            msg = "Failed to connect to server at %s:%s" % (self._host, self._port)
            raise RemoteConnectionException(msg)
        except conf_admin.ConfException as e:
            msg = ("Removal of interface %s at %s:%s failed!" 
                   " Reason: %s" % (name, self._host, self._port, e))
            raise RemoteServerException(msg)
         
class InterfaceAdmin(object):
    def __init__(self, servers, tbl_admin):
        self._servers = {}
        self._tbl_admin = tbl_admin
        for host, port, user, keyfile, hmac_key in servers:
            server = ServerData(host, port, user, keyfile, hmac_key, tbl_admin)
            self._servers[(host, port)] = server
            logging.info("Web server %s:%s added to admin!" % (host, port))
            try:
                server.create_proxy()
                logging.info("Web server %s:%s proxy created!" % (host, port))
            except RemoteConnectionException as e:
                logging.exception(str(e))
                continue

    @staticmethod
    def _create_ssh_client(server, user, keyfile):
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(server, SSH_PORT, username=user, key_filename=keyfile)
        return client

    @staticmethod
    def _scp_file(ssh, fname, remote_d):
        scp_client = scp.SCPClient(ssh.get_transport())
        scp_client.put(fname, remote_d)

    def _get_interface(self, name, host, port):
        try:
            ifaces = self._get_server(host, port).get_interface(name)
        except StopIteration:
            msg = "Interface %s at %s:%s does not exist!" % (name, host, port)
            raise InterfaceAdminException(msg)

    def _get_server(self, host, port):
        try:
            return self._servers[(host, port)]
        except KeyError:
            msg = "Server %s:%s does not exist!" % (host, port)
            raise InterfaceAdminException(msg)

    def _read_table_confs(self, data_tbls, rslt_tbls):
        dtbl_confs = []
        rtbl_confs = []
        
        try:
            for conf in self._tbl_admin.get_table_pair_configuration():
                if conf[NAME] in data_tbls:
                    dtbl_confs.append(conf)
        
            for conf in self._tbl_admin.get_result_table_configuration():
                if conf[NAME] in rslt_tbls:
                    rtbl_confs.append(conf)
        except (KeyError, TypeError) as e:
            msg = ("Failed to read table configuration file contents!"
                   " Reason: %s" % e)
            raise conf_admin.ConfException(msg)
        return dtbl_confs, rtbl_confs

    def _send_interface_script(self, name, host, port):
        fname = name + PY_SUFFIX
        server = self._get_server(host, port)
        
        path = server.get_interface_path()

        try:
            ssh = InterfaceAdmin._create_ssh_client(host, server.user, server.keyfile)
        except paramiko.SSHException as e:
            msg = ("Failed to initialize SSH connection for remote copy of file %s!" 
                   " Reason: %s" (fname, e))
            raise RemoteConnectionException(msg)
        
        try:
            InterfaceAdmin._scp_file(ssh, IFACE_PATH + fname, path)
        except (scp.SCPException, OSError):
            msg = "Remote copy of file %s to remote directory %s failed!" % (fname, path)
            raise RemoteConnectionException(msg)
        except paramiko.SSHException as e:
            msg = ("SSH connection failed during remote copy of file %s!"
                   " Reason: %s" (fname, e))
            raise RemoteConnectionException(msg)
        finally:
            ssh.close()
        logging.info("File %s copied to remote server %s:%s" % (fname, host, port))
        
    def has_server(self, host, port):
        return (host, port) in self._servers

    def has_interface(self, name, host, port):
        try:
            self._get_interface(name, host, port)
        except InterfaceAdminExpection(msg):
            return False
        return True

    def add_server(self, host, port, user, keyfile, hmac_key, ifaces=set()):
        self._servers[(host, port)] = ServerData(host, port, user, keyfile, hmac_key, 
                                                 self._tbl_admin)
        logging.info("Added server at %s:%s to admin connections!" % (host, port))
        self.reconnect_to_server(host, port)
        
    def reconnect_to_server(self, host, port):
        server = self._get_server(host, port)
        server.create_proxy()
        logging.info("Server %s:%s proxy created!" % (host, port))

    def drop_server(self, host, port):
        server = self._get_server(host, port)
        try:
            if len(server.read_confs()) != 0:
                msg = "Cannot drop server %s:%s since it hosts active interfaces!" % (host, port)
                raise InterfaceAdminException(msg)
        except RemoteConnectionException:
            pass
        del self._servers[(host, port)]
        logging.info("Dropped server at %s:%s from admin connections!" % (host, port))
        
    def update_interface(self, name, host, port, cls, data_tbls=[], rslt_tbls=[]):
        dtbl_confs, rtbl_confs = self._read_table_confs(data_tbls, rslt_tbls)
        self._send_interface_script(name, host, port)
        self._get_server(host, port).update_remote_interface(name, cls, dtbl_confs, rtbl_confs)
        logging.info("Interface %s hosted at %s:%s added to server!" % (name, host, port))

    def remove_interface(self, name, host, port):
        self._get_server(host, port).remove_remote_interface(name)
        logging.info("Interface hosted at %s:%s removed from server!" % (host, port))

    def get_interface_confs(self, host, port):
        return self._get_server(host, port).read_confs()

    def list_servers(self):
        return self._servers.keys()

    def list_tables_in_use(self, host, port):
        return self._get_server(host, port).read_tables()

    def list_all_tables_in_use(self):
        data_tbls = set()
        rslt_tbls = set()
        for s in self._servers.itervalues():
             dt, rt = s.read_tables()
             data_tbls.update(dt)
             rslt_tbls.update(rt)
        return list(data_tbls), list(rslt_tbls)

class InterfaceAdminException(Exception):
    pass

class RemoteConnectionException(Exception):
    pass

class RemoteServerException(Exception):
    pass

class InterfaceConfException(conf_admin.ConfException):
    pass
