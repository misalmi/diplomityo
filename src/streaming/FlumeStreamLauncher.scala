package flumestream

import org.apache.spark.storage.StorageLevel
//import org.apache.spark.SparkConf
import org.apache.spark.streaming._
import org.apache.spark.streaming.flume._
import org.apache.spark.streaming.dstream._

class FlumeStreamLauncher(
  val params: Array[String]
){
  val DEFAULT_PARAM_NUM = 5
  val EXTRA_PARAM_NUM = 0
  val DEFAULT_PORT = 10000
  val DEFAULT_BATCH_INTERVAL = 10000
  val USAGE_STR = "Arguments: <master> <host> <port> <batchInterval>"
  
  def toInt(s: String):Option[Int] = {
    try {
      Some(s.toInt)
    } catch {
      case e:NumberFormatException => None
    }
  }

  def runAnalysis(stream: DStream[SparkFlumeEvent], params: Array[String]) {
    throw new NotImplementedError("Subclasses should implement this method!")
  }

  def launch() {
    if (params.length != DEFAULT_PARAM_NUM + EXTRA_PARAM_NUM) {
      System.err.println(USAGE_STR)
      return
    }
    val Array(master, host, port, batchIntervalStr, jarStr) = params.slice(0, DEFAULT_PARAM_NUM)
    val batchInterval = toInt(batchIntervalStr).getOrElse(DEFAULT_BATCH_INTERVAL)
    val jars = jarStr.split(" ").toSeq
      
    val ssc = new StreamingContext(master, "FlumeStreamLauncher", Milliseconds(batchInterval),
      System.getenv("SPARK_HOME"), jars)
    ssc.checkpoint(".")
    
    val stream = FlumeUtils.createStream(ssc, host, toInt(port).getOrElse(0), 
      StorageLevel.MEMORY_ONLY_SER_2)
/*    val stream2 = FlumeUtils.createStream(ssc, "ip-172-31-0-102.us-west-2.compute.internal", 11114, 
      StorageLevel.MEMORY_ONLY_SER_2)
    val stream3 = FlumeUtils.createStream(ssc, "ip-172-31-0-103.us-west-2.compute.internal", 11114, 
      StorageLevel.MEMORY_ONLY_SER_2)*/
           
    //val streams = ssc.union(Seq(stream, stream2, stream3))
    runAnalysis(stream, params.slice(DEFAULT_PARAM_NUM, DEFAULT_PARAM_NUM + EXTRA_PARAM_NUM))
    
    ssc.start()
    ssc.awaitTermination()
  }
}
