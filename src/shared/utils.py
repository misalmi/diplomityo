# -*- coding: utf-8 -*-

import os
import subprocess
from datetime import datetime, timedelta
from multiprocessing import Process
from collections import namedtuple

from constants import *

# TODO: Encapsulate most these functions to approriate classes as static methods.
# Find a more reliable way to escape SQL strings.

def run_process(target, args=()):
    p = Process(target=target, args=args)
    p.start()
    return p

def check_sql_func(func_str):
    if func_str not in VALID_FUNCTIONS:
        raise InvalidSQLAggregateFunction("SQL function %s is not supported" % func_str)
    return func_str

def create_between_string(col, val1, val2):
    return col + " BETWEEN %s AND %s" % (val1, val2)

def format_timestamp(dtime):
    return datetime.strftime(dtime, "%G-%m-%d %H:%M:%S")

def format_time_suffix(dtime):
    return datetime.strftime(dtime, "-%y-%m-%d-%H")

def extract_module_name(fname):
    return os.path.splitext(if_name)[0]

class InvalidSQLAggregateFunction(Exception):
    pass

class InterfaceInitException(Exception):
    pass
