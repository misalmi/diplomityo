
package com.gofore.sujuvuus.schemas;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoadWeatherType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoadWeatherType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="stationid" type="{http://www.gofore.com/sujuvuus/schemas}StationIdType"/>
 *         &lt;element name="measurementtime" type="{http://www.gofore.com/sujuvuus/schemas}ObstimeType"/>
 *         &lt;element name="airtemperature1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="airtemperature1change" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="roadsurfacetemperature1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="roadsurfacetemperature1change" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="roadsurfacetemperature2" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="roadsurfacetemperature2change" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="groundtemperature1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="groundtemperature2" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="dewpoint" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="freezingpoint1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="freezingpoint2" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="averagewindspeed" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="maxwindspeed" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="winddirection" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="humidity" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="precipitation" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="precipitationintensity" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="precipitationsum" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="precipitationtype" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="visibility" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="roadsurfaceconditions1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="roadsurfaceconditions2" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="warning1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="warning2" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="conductivity1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="conductivity2" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="surfacesignal1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="surfacesignal2" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="icefrequency1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="icefrequency2" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="roaddewpointdifference" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="surfacemoisture1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="surfacemoisture2" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="saltamount1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="saltamount2" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="saltconcentration1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="saltconcentration2" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="safetytemperature1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="safetytemperature2" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="visibilitymeters" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="sunup" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="bright" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="fiberresponsesmall1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="fiberresponsesmall2" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="fiberresponsebig1" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="fiberresponsebig2" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="airtemperature3" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="roadsurfacetemperature3" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="humidity3" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="roadsurfacestate3" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="warning3" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="friction3" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="wateramount3" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="snowamount3" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="iceamount3" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *         &lt;element name="friction3number" type="{http://www.gofore.com/sujuvuus/schemas}SensorValueType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoadWeatherType", propOrder = {
    "stationid",
    "measurementtime",
    "airtemperature1",
    "airtemperature1Change",
    "roadsurfacetemperature1",
    "roadsurfacetemperature1Change",
    "roadsurfacetemperature2",
    "roadsurfacetemperature2Change",
    "groundtemperature1",
    "groundtemperature2",
    "dewpoint",
    "freezingpoint1",
    "freezingpoint2",
    "averagewindspeed",
    "maxwindspeed",
    "winddirection",
    "humidity",
    "precipitation",
    "precipitationintensity",
    "precipitationsum",
    "precipitationtype",
    "visibility",
    "roadsurfaceconditions1",
    "roadsurfaceconditions2",
    "warning1",
    "warning2",
    "conductivity1",
    "conductivity2",
    "surfacesignal1",
    "surfacesignal2",
    "icefrequency1",
    "icefrequency2",
    "roaddewpointdifference",
    "surfacemoisture1",
    "surfacemoisture2",
    "saltamount1",
    "saltamount2",
    "saltconcentration1",
    "saltconcentration2",
    "safetytemperature1",
    "safetytemperature2",
    "visibilitymeters",
    "sunup",
    "bright",
    "fiberresponsesmall1",
    "fiberresponsesmall2",
    "fiberresponsebig1",
    "fiberresponsebig2",
    "airtemperature3",
    "roadsurfacetemperature3",
    "humidity3",
    "roadsurfacestate3",
    "warning3",
    "friction3",
    "wateramount3",
    "snowamount3",
    "iceamount3",
    "friction3Number"
})
public class RoadWeatherType {

    @XmlElement(required = true)
    protected BigInteger stationid;
    @XmlElement(required = true)
    protected ObstimeType measurementtime;
    protected BigDecimal airtemperature1;
    @XmlElement(name = "airtemperature1change")
    protected BigDecimal airtemperature1Change;
    protected BigDecimal roadsurfacetemperature1;
    @XmlElement(name = "roadsurfacetemperature1change")
    protected BigDecimal roadsurfacetemperature1Change;
    protected BigDecimal roadsurfacetemperature2;
    @XmlElement(name = "roadsurfacetemperature2change")
    protected BigDecimal roadsurfacetemperature2Change;
    protected BigDecimal groundtemperature1;
    protected BigDecimal groundtemperature2;
    protected BigDecimal dewpoint;
    protected BigDecimal freezingpoint1;
    protected BigDecimal freezingpoint2;
    protected BigDecimal averagewindspeed;
    protected BigDecimal maxwindspeed;
    protected BigDecimal winddirection;
    protected BigDecimal humidity;
    protected BigDecimal precipitation;
    protected BigDecimal precipitationintensity;
    protected BigDecimal precipitationsum;
    protected BigDecimal precipitationtype;
    protected BigDecimal visibility;
    protected BigDecimal roadsurfaceconditions1;
    protected BigDecimal roadsurfaceconditions2;
    protected BigDecimal warning1;
    protected BigDecimal warning2;
    protected BigDecimal conductivity1;
    protected BigDecimal conductivity2;
    protected BigDecimal surfacesignal1;
    protected BigDecimal surfacesignal2;
    protected BigDecimal icefrequency1;
    protected BigDecimal icefrequency2;
    protected BigDecimal roaddewpointdifference;
    protected BigDecimal surfacemoisture1;
    protected BigDecimal surfacemoisture2;
    protected BigDecimal saltamount1;
    protected BigDecimal saltamount2;
    protected BigDecimal saltconcentration1;
    protected BigDecimal saltconcentration2;
    protected BigDecimal safetytemperature1;
    protected BigDecimal safetytemperature2;
    protected BigDecimal visibilitymeters;
    protected BigDecimal sunup;
    protected BigDecimal bright;
    protected BigDecimal fiberresponsesmall1;
    protected BigDecimal fiberresponsesmall2;
    protected BigDecimal fiberresponsebig1;
    protected BigDecimal fiberresponsebig2;
    protected BigDecimal airtemperature3;
    protected BigDecimal roadsurfacetemperature3;
    protected BigDecimal humidity3;
    protected BigDecimal roadsurfacestate3;
    protected BigDecimal warning3;
    protected BigDecimal friction3;
    protected BigDecimal wateramount3;
    protected BigDecimal snowamount3;
    protected BigDecimal iceamount3;
    @XmlElement(name = "friction3number")
    protected BigDecimal friction3Number;

    /**
     * Gets the value of the stationid property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStationid() {
        return stationid;
    }

    /**
     * Sets the value of the stationid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStationid(BigInteger value) {
        this.stationid = value;
    }

    /**
     * Gets the value of the measurementtime property.
     * 
     * @return
     *     possible object is
     *     {@link ObstimeType }
     *     
     */
    public ObstimeType getMeasurementtime() {
        return measurementtime;
    }

    /**
     * Sets the value of the measurementtime property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObstimeType }
     *     
     */
    public void setMeasurementtime(ObstimeType value) {
        this.measurementtime = value;
    }

    /**
     * Gets the value of the airtemperature1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAirtemperature1() {
        return airtemperature1;
    }

    /**
     * Sets the value of the airtemperature1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAirtemperature1(BigDecimal value) {
        this.airtemperature1 = value;
    }

    /**
     * Gets the value of the airtemperature1Change property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAirtemperature1Change() {
        return airtemperature1Change;
    }

    /**
     * Sets the value of the airtemperature1Change property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAirtemperature1Change(BigDecimal value) {
        this.airtemperature1Change = value;
    }

    /**
     * Gets the value of the roadsurfacetemperature1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRoadsurfacetemperature1() {
        return roadsurfacetemperature1;
    }

    /**
     * Sets the value of the roadsurfacetemperature1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRoadsurfacetemperature1(BigDecimal value) {
        this.roadsurfacetemperature1 = value;
    }

    /**
     * Gets the value of the roadsurfacetemperature1Change property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRoadsurfacetemperature1Change() {
        return roadsurfacetemperature1Change;
    }

    /**
     * Sets the value of the roadsurfacetemperature1Change property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRoadsurfacetemperature1Change(BigDecimal value) {
        this.roadsurfacetemperature1Change = value;
    }

    /**
     * Gets the value of the roadsurfacetemperature2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRoadsurfacetemperature2() {
        return roadsurfacetemperature2;
    }

    /**
     * Sets the value of the roadsurfacetemperature2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRoadsurfacetemperature2(BigDecimal value) {
        this.roadsurfacetemperature2 = value;
    }

    /**
     * Gets the value of the roadsurfacetemperature2Change property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRoadsurfacetemperature2Change() {
        return roadsurfacetemperature2Change;
    }

    /**
     * Sets the value of the roadsurfacetemperature2Change property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRoadsurfacetemperature2Change(BigDecimal value) {
        this.roadsurfacetemperature2Change = value;
    }

    /**
     * Gets the value of the groundtemperature1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGroundtemperature1() {
        return groundtemperature1;
    }

    /**
     * Sets the value of the groundtemperature1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGroundtemperature1(BigDecimal value) {
        this.groundtemperature1 = value;
    }

    /**
     * Gets the value of the groundtemperature2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGroundtemperature2() {
        return groundtemperature2;
    }

    /**
     * Sets the value of the groundtemperature2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGroundtemperature2(BigDecimal value) {
        this.groundtemperature2 = value;
    }

    /**
     * Gets the value of the dewpoint property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDewpoint() {
        return dewpoint;
    }

    /**
     * Sets the value of the dewpoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDewpoint(BigDecimal value) {
        this.dewpoint = value;
    }

    /**
     * Gets the value of the freezingpoint1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFreezingpoint1() {
        return freezingpoint1;
    }

    /**
     * Sets the value of the freezingpoint1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFreezingpoint1(BigDecimal value) {
        this.freezingpoint1 = value;
    }

    /**
     * Gets the value of the freezingpoint2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFreezingpoint2() {
        return freezingpoint2;
    }

    /**
     * Sets the value of the freezingpoint2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFreezingpoint2(BigDecimal value) {
        this.freezingpoint2 = value;
    }

    /**
     * Gets the value of the averagewindspeed property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAveragewindspeed() {
        return averagewindspeed;
    }

    /**
     * Sets the value of the averagewindspeed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAveragewindspeed(BigDecimal value) {
        this.averagewindspeed = value;
    }

    /**
     * Gets the value of the maxwindspeed property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxwindspeed() {
        return maxwindspeed;
    }

    /**
     * Sets the value of the maxwindspeed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxwindspeed(BigDecimal value) {
        this.maxwindspeed = value;
    }

    /**
     * Gets the value of the winddirection property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWinddirection() {
        return winddirection;
    }

    /**
     * Sets the value of the winddirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWinddirection(BigDecimal value) {
        this.winddirection = value;
    }

    /**
     * Gets the value of the humidity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHumidity() {
        return humidity;
    }

    /**
     * Sets the value of the humidity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHumidity(BigDecimal value) {
        this.humidity = value;
    }

    /**
     * Gets the value of the precipitation property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrecipitation() {
        return precipitation;
    }

    /**
     * Sets the value of the precipitation property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrecipitation(BigDecimal value) {
        this.precipitation = value;
    }

    /**
     * Gets the value of the precipitationintensity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrecipitationintensity() {
        return precipitationintensity;
    }

    /**
     * Sets the value of the precipitationintensity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrecipitationintensity(BigDecimal value) {
        this.precipitationintensity = value;
    }

    /**
     * Gets the value of the precipitationsum property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrecipitationsum() {
        return precipitationsum;
    }

    /**
     * Sets the value of the precipitationsum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrecipitationsum(BigDecimal value) {
        this.precipitationsum = value;
    }

    /**
     * Gets the value of the precipitationtype property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrecipitationtype() {
        return precipitationtype;
    }

    /**
     * Sets the value of the precipitationtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrecipitationtype(BigDecimal value) {
        this.precipitationtype = value;
    }

    /**
     * Gets the value of the visibility property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVisibility() {
        return visibility;
    }

    /**
     * Sets the value of the visibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVisibility(BigDecimal value) {
        this.visibility = value;
    }

    /**
     * Gets the value of the roadsurfaceconditions1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRoadsurfaceconditions1() {
        return roadsurfaceconditions1;
    }

    /**
     * Sets the value of the roadsurfaceconditions1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRoadsurfaceconditions1(BigDecimal value) {
        this.roadsurfaceconditions1 = value;
    }

    /**
     * Gets the value of the roadsurfaceconditions2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRoadsurfaceconditions2() {
        return roadsurfaceconditions2;
    }

    /**
     * Sets the value of the roadsurfaceconditions2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRoadsurfaceconditions2(BigDecimal value) {
        this.roadsurfaceconditions2 = value;
    }

    /**
     * Gets the value of the warning1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWarning1() {
        return warning1;
    }

    /**
     * Sets the value of the warning1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWarning1(BigDecimal value) {
        this.warning1 = value;
    }

    /**
     * Gets the value of the warning2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWarning2() {
        return warning2;
    }

    /**
     * Sets the value of the warning2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWarning2(BigDecimal value) {
        this.warning2 = value;
    }

    /**
     * Gets the value of the conductivity1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getConductivity1() {
        return conductivity1;
    }

    /**
     * Sets the value of the conductivity1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setConductivity1(BigDecimal value) {
        this.conductivity1 = value;
    }

    /**
     * Gets the value of the conductivity2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getConductivity2() {
        return conductivity2;
    }

    /**
     * Sets the value of the conductivity2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setConductivity2(BigDecimal value) {
        this.conductivity2 = value;
    }

    /**
     * Gets the value of the surfacesignal1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSurfacesignal1() {
        return surfacesignal1;
    }

    /**
     * Sets the value of the surfacesignal1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSurfacesignal1(BigDecimal value) {
        this.surfacesignal1 = value;
    }

    /**
     * Gets the value of the surfacesignal2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSurfacesignal2() {
        return surfacesignal2;
    }

    /**
     * Sets the value of the surfacesignal2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSurfacesignal2(BigDecimal value) {
        this.surfacesignal2 = value;
    }

    /**
     * Gets the value of the icefrequency1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIcefrequency1() {
        return icefrequency1;
    }

    /**
     * Sets the value of the icefrequency1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIcefrequency1(BigDecimal value) {
        this.icefrequency1 = value;
    }

    /**
     * Gets the value of the icefrequency2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIcefrequency2() {
        return icefrequency2;
    }

    /**
     * Sets the value of the icefrequency2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIcefrequency2(BigDecimal value) {
        this.icefrequency2 = value;
    }

    /**
     * Gets the value of the roaddewpointdifference property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRoaddewpointdifference() {
        return roaddewpointdifference;
    }

    /**
     * Sets the value of the roaddewpointdifference property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRoaddewpointdifference(BigDecimal value) {
        this.roaddewpointdifference = value;
    }

    /**
     * Gets the value of the surfacemoisture1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSurfacemoisture1() {
        return surfacemoisture1;
    }

    /**
     * Sets the value of the surfacemoisture1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSurfacemoisture1(BigDecimal value) {
        this.surfacemoisture1 = value;
    }

    /**
     * Gets the value of the surfacemoisture2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSurfacemoisture2() {
        return surfacemoisture2;
    }

    /**
     * Sets the value of the surfacemoisture2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSurfacemoisture2(BigDecimal value) {
        this.surfacemoisture2 = value;
    }

    /**
     * Gets the value of the saltamount1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSaltamount1() {
        return saltamount1;
    }

    /**
     * Sets the value of the saltamount1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSaltamount1(BigDecimal value) {
        this.saltamount1 = value;
    }

    /**
     * Gets the value of the saltamount2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSaltamount2() {
        return saltamount2;
    }

    /**
     * Sets the value of the saltamount2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSaltamount2(BigDecimal value) {
        this.saltamount2 = value;
    }

    /**
     * Gets the value of the saltconcentration1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSaltconcentration1() {
        return saltconcentration1;
    }

    /**
     * Sets the value of the saltconcentration1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSaltconcentration1(BigDecimal value) {
        this.saltconcentration1 = value;
    }

    /**
     * Gets the value of the saltconcentration2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSaltconcentration2() {
        return saltconcentration2;
    }

    /**
     * Sets the value of the saltconcentration2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSaltconcentration2(BigDecimal value) {
        this.saltconcentration2 = value;
    }

    /**
     * Gets the value of the safetytemperature1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSafetytemperature1() {
        return safetytemperature1;
    }

    /**
     * Sets the value of the safetytemperature1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSafetytemperature1(BigDecimal value) {
        this.safetytemperature1 = value;
    }

    /**
     * Gets the value of the safetytemperature2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSafetytemperature2() {
        return safetytemperature2;
    }

    /**
     * Sets the value of the safetytemperature2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSafetytemperature2(BigDecimal value) {
        this.safetytemperature2 = value;
    }

    /**
     * Gets the value of the visibilitymeters property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVisibilitymeters() {
        return visibilitymeters;
    }

    /**
     * Sets the value of the visibilitymeters property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVisibilitymeters(BigDecimal value) {
        this.visibilitymeters = value;
    }

    /**
     * Gets the value of the sunup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSunup() {
        return sunup;
    }

    /**
     * Sets the value of the sunup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSunup(BigDecimal value) {
        this.sunup = value;
    }

    /**
     * Gets the value of the bright property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBright() {
        return bright;
    }

    /**
     * Sets the value of the bright property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBright(BigDecimal value) {
        this.bright = value;
    }

    /**
     * Gets the value of the fiberresponsesmall1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFiberresponsesmall1() {
        return fiberresponsesmall1;
    }

    /**
     * Sets the value of the fiberresponsesmall1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFiberresponsesmall1(BigDecimal value) {
        this.fiberresponsesmall1 = value;
    }

    /**
     * Gets the value of the fiberresponsesmall2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFiberresponsesmall2() {
        return fiberresponsesmall2;
    }

    /**
     * Sets the value of the fiberresponsesmall2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFiberresponsesmall2(BigDecimal value) {
        this.fiberresponsesmall2 = value;
    }

    /**
     * Gets the value of the fiberresponsebig1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFiberresponsebig1() {
        return fiberresponsebig1;
    }

    /**
     * Sets the value of the fiberresponsebig1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFiberresponsebig1(BigDecimal value) {
        this.fiberresponsebig1 = value;
    }

    /**
     * Gets the value of the fiberresponsebig2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFiberresponsebig2() {
        return fiberresponsebig2;
    }

    /**
     * Sets the value of the fiberresponsebig2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFiberresponsebig2(BigDecimal value) {
        this.fiberresponsebig2 = value;
    }

    /**
     * Gets the value of the airtemperature3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAirtemperature3() {
        return airtemperature3;
    }

    /**
     * Sets the value of the airtemperature3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAirtemperature3(BigDecimal value) {
        this.airtemperature3 = value;
    }

    /**
     * Gets the value of the roadsurfacetemperature3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRoadsurfacetemperature3() {
        return roadsurfacetemperature3;
    }

    /**
     * Sets the value of the roadsurfacetemperature3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRoadsurfacetemperature3(BigDecimal value) {
        this.roadsurfacetemperature3 = value;
    }

    /**
     * Gets the value of the humidity3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHumidity3() {
        return humidity3;
    }

    /**
     * Sets the value of the humidity3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHumidity3(BigDecimal value) {
        this.humidity3 = value;
    }

    /**
     * Gets the value of the roadsurfacestate3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRoadsurfacestate3() {
        return roadsurfacestate3;
    }

    /**
     * Sets the value of the roadsurfacestate3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRoadsurfacestate3(BigDecimal value) {
        this.roadsurfacestate3 = value;
    }

    /**
     * Gets the value of the warning3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWarning3() {
        return warning3;
    }

    /**
     * Sets the value of the warning3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWarning3(BigDecimal value) {
        this.warning3 = value;
    }

    /**
     * Gets the value of the friction3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFriction3() {
        return friction3;
    }

    /**
     * Sets the value of the friction3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFriction3(BigDecimal value) {
        this.friction3 = value;
    }

    /**
     * Gets the value of the wateramount3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWateramount3() {
        return wateramount3;
    }

    /**
     * Sets the value of the wateramount3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWateramount3(BigDecimal value) {
        this.wateramount3 = value;
    }

    /**
     * Gets the value of the snowamount3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSnowamount3() {
        return snowamount3;
    }

    /**
     * Sets the value of the snowamount3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSnowamount3(BigDecimal value) {
        this.snowamount3 = value;
    }

    /**
     * Gets the value of the iceamount3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIceamount3() {
        return iceamount3;
    }

    /**
     * Sets the value of the iceamount3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIceamount3(BigDecimal value) {
        this.iceamount3 = value;
    }

    /**
     * Gets the value of the friction3Number property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFriction3Number() {
        return friction3Number;
    }

    /**
     * Sets the value of the friction3Number property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFriction3Number(BigDecimal value) {
        this.friction3Number = value;
    }

}
