# -*- coding: utf-8 -*-

import sys
import time
from datetime import datetime

sys.path.append("..")
from shared import utils, tables
from interface import QueryInterface, ParseException

FLUENCY = "trafficfluency"
AREA = "area"
AREAS = AREA + "s"
TIME = "time"
TIME1 = "t1"
TIME2 = "t2"

class TrafficInterface(QueryInterface):
    @staticmethod
    def parse_areas(areas):
        areas = areas.split(",")
        valid_areas = []
        for area in areas:
            msg = "Invalid area %s!" % area
            try:
                area = int(area)
            except ValueError:
                raise ParseException(msg)
            if area < 0 or area > 10000:
                raise ParseException(msg)
            valid_areas.append(area)
        return valid_areas

    @staticmethod
    def parse_timestamp(t):
        try:
            return datetime.fromtimestamp(float(t))
        except ValueError:
            msg = "Invalid timestamp value %s" % t
            raise ParseException(msg)

    def query_recent_fluency(self, areas):
        cond, params = QueryInterface.create_in_cond(AREA, AREA, areas)
        cond2, params2 = QueryInterface.create_in_cond(AREA, AREA + "2", areas)
        params.update(params2)

        query = self.create_union_select_stmt(FLUENCY, cond=cond)
        query2 = self.create_union_select_stmt(FLUENCY, cond=cond2)
        stmt = ("SELECT t1.area, t1.time, t1.speed, t1.speed_avg, t1.fluency FROM " 
                "(%s) t1 JOIN (%s) t2 ON t1.area = t2.area AND t1.time >= t2.time " 
                "GROUP BY t1.area, t1.time, t1.speed, t1.speed_avg, t1.fluency "
                "HAVING COUNT (*) <= 10 " 
                "ORDER BY area, time") % (query, query2)
        results = self.select(FLUENCY, stmt, params)
        return results

    def query_fluency_at(self, areas, t):
        a_cond, params = QueryInterface.create_in_cond(AREA, AREA, areas)
        t_cond, t_param = QueryInterface.create_cmp_cond(TIME, TIME, 
                                                         utils.format_timestamp(t), 
                                                         "=")
        params.update(t_param)

        cond = "%s AND %s" % (a_cond, t_cond)        
        return self.select_from_data(FLUENCY, cond=cond, params=params)

    def query_avg_fluency_between(self, areas, t1, t2):
        a_cond, params = QueryInterface.create_in_cond(AREA, AREA, areas)
        t_cond, t_params = QueryInterface.create_between_cond(TIME, TIME, 
                                                              utils.format_timestamp(t1), 
                                                              utils.format_timestamp(t2))
        params.update(t_params)
        
        cond = "%s AND %s GROUP BY area" % (a_cond, t_cond)
        return self.select_from_data(FLUENCY, cond=cond, select_cols=(AREA, "AVG(fluency)"), 
                                     params=params)

    def query_fluency(self, args):
        areas = args[AREAS]
        t1 = args[TIME1]
        t2 = args[TIME2]
        
        if t1 is None:
            result = self.query_recent_fluency(areas)
            result= [(area, time.mktime(t.timetuple()), speed, speed_avg, fluency) 
                      for area, t, speed, speed_avg, fluency in result]
        elif t2 is None:
            result = self.query_fluency_at(areas, t1)
            result = [(area, time.mktime(t.timetuple()), speed, speed_avg, fluency) 
                      for area, t, speed, speed_avg, fluency in result]
        else:
            result = self.query_avg_fluency_between(areas, t1, t2)
            result = [(area, fluency) for area, fluency in result]

        return result

    def parse_args(self):
        self._parser.add_argument(AREAS, type=TrafficInterface.parse_areas, required=True)
        self._parser.add_argument(TIME1, type=TrafficInterface.parse_timestamp)
        self._parser.add_argument(TIME2, type=TrafficInterface.parse_timestamp)
        self._args = self._parser.parse_args()

    def query(self):
        if self._args is None:
            msg = "Arguments have not been parsed!"
            raise QueryFailedException(msg)
        return self.query_fluency(self._args)
