/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package weathertest;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;
import com.gofore.sujuvuus.schemas.*;
import com.gofore.sujuvuus.schemas.RoadWeatherResponse.Roadweatherdata;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author gilkate
 */
public class WeatherTest {
    
    private static final int SENSOR_NUM = 563;
    private static final String FLUME_HOST = "54.149.6.212";
    private static final int FLUME_PORT = 11110;
    
    public static void main(String[] args) {
        Socket flumeSocket = null;
        PrintWriter out = null;
        
        Holder<ObstimeType> timeStamp = new Holder<>();
        Holder<XMLGregorianCalendar> lastStaticDataUpdate = new Holder<>();
        Holder<Roadweatherdata> roadWeatherData = new Holder<>();
        
        Logger logger = LoggerFactory.getLogger(WeatherTest.class);
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        StatusPrinter.print(lc);
        
        HashMap<Integer, String> lastData = new HashMap<>();
        for (int i = 0; i < SENSOR_NUM; i++) {
            lastData.put(i, "");
        }
        
        try {
            flumeSocket = new Socket(FLUME_HOST, FLUME_PORT);
        } catch (IOException ex) {
            logger.error("Failed to create connection!");
            System.exit(1);
        }
        
        try {
            out = new PrintWriter(flumeSocket.getOutputStream(), true);
        } catch (IOException ex) {
            logger.error("Failed to create output stream!");
            System.exit(1);
        }
        
        try {
            
            while (true) {
                roadWeather(timeStamp,lastStaticDataUpdate,roadWeatherData);
                sendData(roadWeatherData,lastData, out);
                Thread.sleep(10000);
            }
        } catch (InterruptedException ex) {
           System.out.println("While thread error");
           //ex.printStackTrace();
        }
        try {
            flumeSocket.close();
        } catch (IOException ex) {
            logger.error("Failed to close socket!");
            System.exit(1);
        }
    }
    
    private static void roadWeather(Holder<ObstimeType> timestamp, 
            Holder<XMLGregorianCalendar> laststaticdataupdate, 
            Holder<Roadweatherdata> roadweatherdata) {
        RoadWeatherService service = new RoadWeatherService();
        RoadWeatherPort port = service.getRoadWeatherPort();
        // from : http://stackoverflow.com/questions/1928590/netbeans-basic-http-auth-jax-ws
        Map<String, Object> reqContext = ((BindingProvider)port).getRequestContext();
        reqContext.put(BindingProvider.USERNAME_PROPERTY, "jukkarie");
        reqContext.put(BindingProvider.PASSWORD_PROPERTY, "jukka1043");
        port.roadWeather(timestamp, laststaticdataupdate, roadweatherdata);
    }
    
    private static void sendData(Holder<Roadweatherdata> roadWeatherData, 
            HashMap<Integer, String> lastData, PrintWriter out) {
        int i = 0;
        List<RoadWeatherType> weatherData = roadWeatherData.value.getRoadweather();
        String values;
        
        for (RoadWeatherType weatherType : weatherData) {
            values = "";
            try {
                Method[] methods = weatherType.getClass().getMethods();
                for (Method method : methods) {
                    String methodName = method.getName();
                    if (methodName.startsWith("get")) {
                        if (methodName.equals("getMeasurementtime")) {
                            String meas_date = weatherType.getMeasurementtime().getLocaltime().toString();
                            values += meas_date.substring(0, 10) + " " + meas_date.substring(11,19) + ",";
                        } else if (!methodName.equals("getClass")) {
                            if (method.invoke(weatherType) != null) {
                                values += (method.invoke(weatherType)).toString() + ",";
                            } else {
                                values += ",";
                            }
                        }
                    }
                }
            } catch (InvocationTargetException | IllegalAccessException ex) {
                System.out.println("Error while reading data");
            }
            values = values.substring(0, values.length()-1);
            if (!lastData.get(i).equals(values)) {
                lastData.put(i, values);
                out.println(values);
            }
            i++;
        }
    }
}
