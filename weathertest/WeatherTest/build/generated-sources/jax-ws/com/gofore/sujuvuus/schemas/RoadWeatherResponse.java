
package com.gofore.sujuvuus.schemas;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="timestamp" type="{http://www.gofore.com/sujuvuus/schemas}ObstimeType"/>
 *         &lt;element name="laststaticdataupdate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="roadweatherdata">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="roadweather" type="{http://www.gofore.com/sujuvuus/schemas}RoadWeatherType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "timestamp",
    "laststaticdataupdate",
    "roadweatherdata"
})
@XmlRootElement(name = "RoadWeatherResponse")
public class RoadWeatherResponse {

    @XmlElement(required = true)
    protected ObstimeType timestamp;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar laststaticdataupdate;
    @XmlElement(required = true)
    protected RoadWeatherResponse.Roadweatherdata roadweatherdata;

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link ObstimeType }
     *     
     */
    public ObstimeType getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObstimeType }
     *     
     */
    public void setTimestamp(ObstimeType value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the laststaticdataupdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLaststaticdataupdate() {
        return laststaticdataupdate;
    }

    /**
     * Sets the value of the laststaticdataupdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLaststaticdataupdate(XMLGregorianCalendar value) {
        this.laststaticdataupdate = value;
    }

    /**
     * Gets the value of the roadweatherdata property.
     * 
     * @return
     *     possible object is
     *     {@link RoadWeatherResponse.Roadweatherdata }
     *     
     */
    public RoadWeatherResponse.Roadweatherdata getRoadweatherdata() {
        return roadweatherdata;
    }

    /**
     * Sets the value of the roadweatherdata property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoadWeatherResponse.Roadweatherdata }
     *     
     */
    public void setRoadweatherdata(RoadWeatherResponse.Roadweatherdata value) {
        this.roadweatherdata = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="roadweather" type="{http://www.gofore.com/sujuvuus/schemas}RoadWeatherType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "roadweather"
    })
    public static class Roadweatherdata {

        protected List<RoadWeatherType> roadweather;

        /**
         * Gets the value of the roadweather property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the roadweather property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRoadweather().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RoadWeatherType }
         * 
         * 
         */
        public List<RoadWeatherType> getRoadweather() {
            if (roadweather == null) {
                roadweather = new ArrayList<RoadWeatherType>();
            }
            return this.roadweather;
        }

    }

}
